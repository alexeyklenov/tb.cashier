﻿using System.Runtime.Serialization;
using TB.Services.Infrastructure.Interface;

namespace TB.Cashier.Server.Services.Interfaces.Faults
{
    [DataContract]
    public class ShiftFault : FaultBase
    {
        public ShiftFault(string message) : base(message)
        {
        }
    }
}
