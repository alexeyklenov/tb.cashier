﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces.Faults;

namespace TB.Cashier.Server.Services.Interfaces
{
    [ServiceContract]
    public interface IChequeService
    {
         [OperationContract]
    [FaultContract(typeof(ShiftFault))]
        ShiftDto GetCurrentShift();

        [OperationContract]
         ChequeDto GetActiveCheque();

        [OperationContract]
        void AddMaterialToChequeByCode(long chequeId, string materialCode);

        [OperationContract]
        void AddFreeLineToCheque(long chequeId);

        [OperationContract]
        void UpdateChequeLine(ChequeLineDto chequeLine);

        [OperationContract]
        void ClearChequeById(long id);

        [OperationContract]
        bool CloseCurrentShift();

        [OperationContract]
        void DeleteChequeLineById(long id);

        [OperationContract]
        void CloseChequeById(long id);

        [OperationContract]
        ChequeDto GetChequeById(long id);

        [OperationContract]
        int CheckCodeIsMaterial(string code);

        [OperationContract]
        List<ChequeDto> GetPostponedByCurrentShift();

        [OperationContract]
        void PostponeChequeById(long id);

        [OperationContract]
        List<ChequeDto> GetAllChequeByCurrentShift();

        [OperationContract]
        void DeleteChequeById(long id);

        [OperationContract]
        List<ChequeDto> GetReadyForReturn(long shiftId);

        [OperationContract]
        void ActivateCheque(long id);

        [OperationContract]
        List<ShiftDto> GetAllShifts(DateTime? startPeriod, DateTime? endPeriod);

        [OperationContract]
        void CopyChequeLineById(long lineId, long toChequeId);

        [OperationContract]
        void CopyChequeLines(long fromChequeId, long toChequeId);

        [OperationContract(Name = "CopyChequeLinesByList")]
        void CopyChequeLines(long fromChequeId, long toChequeId, List<long> chequeLinesIds);

        [OperationContract]
        ChequeDto CreateReturnCheque(long sourceChequeId);

        [OperationContract]
        decimal CurrentShiftCashSum();

        [OperationContract]
        decimal CashSumInBox();

        [OperationContract]
        ShiftDto OpenShift();

        [OperationContract]
        void CloseCheque(ChequeDto cheque);

        [OperationContract]
        void MakeIncome(decimal summ);

        [OperationContract]
        void MakeOutcome(decimal summ);

    }
}
