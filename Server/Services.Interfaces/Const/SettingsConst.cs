﻿namespace TB.Cashier.Server.Services.Interfaces.Const
{
    public static class SettingsConst
    {
        public const long BaseSettings = 1;

        public const long CashBoxDepartment = 11;

        public const long ShopName = 12;

        public const long OrgName = 13;

        public const long Warehouse = 14;
    }
}
