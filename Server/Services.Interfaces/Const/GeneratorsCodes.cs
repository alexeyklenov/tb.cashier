﻿namespace TB.Cashier.Server.Services.Interfaces.Const
{
    public static class GeneratorsCodes
    {
        public const long UniversalDocumentGenerator = 1;
        public const long ChequeGenerator = 2;
    }
}
