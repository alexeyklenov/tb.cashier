﻿namespace TB.Cashier.Server.Services.Interfaces.Const
{
    public static class DocumentTypeCodes
    {
        public const long Shift = 1;
        public const long Cheque = 2;
        public const long Discount = 3;
        
        /// <summary>
        /// Внесение наличных
        /// </summary>
        public const long MakeMoney = 4;
       
        /// <summary>
        /// Выдача наличных
        /// </summary>
        public const long IssuanceMoney = 5;

        /// <summary>
        /// Возврат
        /// </summary>
        public const long Return = 6;

        /// <summary>
        /// Сообщение
        /// </summary>
        public const long Message = 7;
    }
}
