﻿namespace TB.Cashier.Server.Services.Interfaces.Const
{
    public static class DescValCodes
    {
        /// <summary>
        /// Наличные
        /// </summary>
        public const long Cash = 1;

        /// <summary>
        /// Безнал
        /// </summary>
        public const long Card = 2;

        /// <summary>
        /// Кредит
        /// </summary>
        public const long Credit = 3;
    }
}
