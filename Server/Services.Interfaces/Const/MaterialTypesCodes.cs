﻿namespace TB.Cashier.Server.Services.Interfaces.Const
{
    public static class MaterialTypesCodes
    {
        public const long Catalog = 1;
        public const long Offer = 2;
        public const long Folder = 3;
    }
}
