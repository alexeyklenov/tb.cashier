﻿namespace TB.Cashier.Server.Services.Interfaces.Const
{
    public static class DescTypeCodes
    {
        /// <summary>
        /// Количество
        /// </summary>
        public const long Quantity = 1;

        /// <summary>
        /// Цена
        /// </summary>
        public const long Price = 2;

        /// <summary>
        /// Скидка
        /// </summary>
        public const long Discount = 3;

        /// <summary>
        /// Вид оплаты
        /// </summary>
        public const long PaymentType = 4;

        /// <summary>
        /// Контрагент
        /// </summary>
        public const long Partner = 5;

        /// <summary>
        /// Дата смены
        /// </summary>
        public const long ShiftDate = 6;

        /// <summary>
        /// Сумма клиента
        /// </summary>
        public const long SumByClient = 7;

        /// <summary>
        /// Склад
        /// </summary>
        public const long Warehouse = 8;

        /// <summary>
        /// Сумма наличности в кассе за смену
        /// </summary>
        public const long CashSumByShift = 9;

        /// <summary>
        /// Индикатор, что смена уже выгружалась
        /// </summary>
        public const long ShiftIsUnloaded = 10;

        /// <summary>
        /// Сумма по чеку
        /// </summary>
        public const long Sum = 11;

        /// <summary>
        /// Характеристика
        /// </summary>
        public const long Characteristic = 12;

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public const long  MessageText = 13;

        /// <summary>
        /// Ставка НДС
        /// </summary>
        public const long Nds = 14;
        
        /// <summary>
        /// Справочник касс
        /// </summary>
        public const long CashBoxDepartments = 15;

        /// <summary>
        /// Справочник магазинов
        /// </summary>
        public const long Shops = 16;

        /// <summary>
        /// Справочник складов
        /// </summary>
        public const long Warehouses = 17;

        /// <summary>
        /// Справочник организаций
        /// </summary>
        public const long Organizations = 18;

        /// <summary>
        /// Справочник производителей
        /// </summary>
        public const long Vendors = 19;
    }
}
