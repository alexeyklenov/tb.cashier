﻿using System.Collections.Generic;
using System.ServiceModel;
using TB.Cashier.Server.Services.Dto;

namespace TB.Cashier.Server.Services.Interfaces
{
     [ServiceContract]
    public interface IMaterialService
     {
         [OperationContract]
         List<MaterialDto> SearchMaterialsByName(string name);

         [OperationContract]
         List<MaterialDto> GetAllMaterialsByFolders();
     }
}
