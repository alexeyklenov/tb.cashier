﻿using System;
using System.Configuration;
using System.IO;
using log4net;
using log4net.Config;
using TB.Services.Infrastructure.Server;

namespace TB.Cashier.Server.ConsoleHost
{
    class Program
    {
        private static ILog _log;
        static void Main()
       {
            XmlConfigurator.ConfigureAndWatch(new FileInfo(ConfigurationManager.AppSettings["LogConfigPath"]));

            _log = LogManager.GetLogger("Main");

            _log.Info("Запуск сервера");
            WatchServiceHoster.Start(string.Empty);

            Console.WriteLine("Нажмите любую клавишу для завершения");
            Console.ReadKey();


        }
    }
}
