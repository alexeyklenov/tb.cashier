﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;
using TB.BOL.Descriptions;
using TB.BOL.Documents;
using TB.BOL.Interfaces;
using TB.BOL.Materials;
using TB.Cashier.Server.Services.Interfaces.Const;
using TB.DAL.DAO.Descriptions;
using TB.DAL.DAO.Documents;
using TB.DAL.Interface;
using TB.Services.Common;
using TB.Services.Common.Dto.Administration;
using TB.Services.Infrastructure.Attributes.Agent;

namespace TB.Cashier.Server.Agents._1c
{
    [KnownType(typeof(LoadAgent))]
    [Agent]
    public class LoadAgent
    {
        private Configuration _config;
        private List<SettingDto> _settings;
        private List<SettingDto> Settings {
            get { return _settings ?? (_settings = new SettingService().GetAllSettings()); }
        }

        private IDao<Material> _mDao = DaoContainer.Dao<Material>();
        private IDao<MaterialType> _mTDao = DaoContainer.Dao<MaterialType>();
        private DescriptionTypeDao _dTDao = DaoContainer.GetDao<DescriptionTypeDao>();

        [AgentJob(20)]
        public void LoadAll()
        {
            _config =
                ConfigurationManager.OpenExeConfiguration(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

            if (!Directory.Exists(_config.AppSettings.Settings["AgentLoadFolder"].Value)) 
                return;

            var shift = DaoContainer.GetDao<DocumentDao>()
                .Linq()
                .FirstOrDefault(
                    d => d.DocumentType.Code == DocumentTypeCodes.Shift && d.DocumentStatus == DocumentStatus.Created);

            if (shift == null ||
                !shift.DerivedDocuments.Any(
                    d => (d.DocumentType.Code == DocumentTypeCodes.Cheque && d.DocumentStatus == DocumentStatus.Closed)))
            {

                LoadNomenclature();
                LoadOffers();
            }

        }

        private void LoadNomenclature()
        {

            var currCashBox = Settings.Find(x => x.Code == SettingsConst.CashBoxDepartment);
            var currWarehouse = Settings.Find(x => x.Code == SettingsConst.Warehouse);
            var offerType = _mTDao.Linq().First(mt => mt.Code == MaterialTypesCodes.Offer);
            var vendors = _dTDao.Linq().First(dt => dt.Code == DescTypeCodes.Vendors);

            //TODO исправить анализ на содержание в имени nom только в имени файла, а не всем пути
            var files =
                Directory.GetFiles(_config.AppSettings.Settings["AgentLoadFolder"].Value)
                    .Where(f => f.Contains("nom"))
                    .Where(f => f.Contains(currCashBox == null ? "" : currCashBox.ReferenceValue))
                    .ToList();

            foreach (var file in files)
            {
                var doc = new XmlDocument();
                doc.Load(file);
                var xmlNodeList = doc.SelectNodes("Root");
                if (xmlNodeList == null)
                    continue;
                foreach (XmlNode node in xmlNodeList)
                {
                    foreach (XmlNode nomenChild in node.ChildNodes)
                    {
                        if (nomenChild.Name == "nomen")
                        {
                            foreach (XmlNode nomen in nomenChild)
                            {
                                if (nomen.Attributes == null)
                                    continue;
                                var no = nomen.Attributes["КодНоменклатуры"].Value;
                                var name = nomen.Attributes["Номенклатура"].Value;
                                var nds = nomen.Attributes["СтавкаНДС"].Value;
                                var vendorCode = nomen.Attributes["Производитель"].Value;

                                decimal ndsRate;
                                decimal.TryParse(nds, out ndsRate);

                                var existingMaterial = _mDao.Linq().FirstOrDefault(m => m.No == no);

                                if (existingMaterial == null)
                                {
                                    existingMaterial = new Material
                                    {
                                        MaterialType = _mTDao.Linq().First(x => x.Code == MaterialTypesCodes.Catalog),
                                        No = no,
                                        Name = name
                                    };
                                    _mDao.Save(existingMaterial);
                                }
                                existingMaterial.Name = name;
                                existingMaterial.SetDescription(DescTypeCodes.Nds, ndsRate);

                                var vendor = vendors.DescriptionValues.FirstOrDefault(dv => dv.OuterCode == vendorCode);
                                if (vendor != null)
                                    existingMaterial.SetDescription(DescTypeCodes.Vendors, vendor.Id);

                                foreach (XmlNode storage in nomen)
                                {
                                    if (storage.Attributes == null)
                                        continue;

                                    var offerName = storage.Attributes["Характеристика"].Value;
                                    var code = storage.Attributes["Штрихкод"].Value;

                                    var offers = existingMaterial.AllChildMaterials.Where(
                                        m =>
                                            m.Descriptions.Any(
                                                d =>
                                                    d.DescriptionType.Code == DescTypeCodes.Characteristic &&
                                                    d.StringValue == offerName)).ToList();

                                    //удаляем дубли от старой конфигурации
                                    if (offers.Count > 1)
                                    {
                                        foreach (
                                            var offer in
                                                offers.Where(
                                                    offer => offer.Documents == null || offer.Documents.Count == 0))
                                        {
                                            offer.RemoveParentRelations();
                                            _mDao.Delete(offer);
                                        }
                                    }

                                    var existingOffer =
                                        existingMaterial.AllChildMaterials.FirstOrDefault(
                                            m => m.Descriptions.Any(
                                                d =>
                                                    (d.DescriptionType.Code == DescTypeCodes.Characteristic &&
                                                     d.StringValue == offerName)));

                                    if (existingOffer == null)
                                    {
                                        existingOffer = new Material
                                        {
                                            MaterialType = offerType,
                                            Code = code
                                        };
                                        existingOffer.SetDescription(DescTypeCodes.Characteristic, offerName);

                                        existingMaterial.ChildMaterials.Add(new MaterialRelation()
                                        {
                                            ChildMaterial = existingOffer,
                                            ParentMaterial = existingMaterial,
                                        });
                                    }

                                    existingOffer.Name = existingMaterial.Name + " " + offerName;

                                    if (currWarehouse != null && currWarehouse.ReferenceId != 0 &&
                                        !existingOffer.Descriptions.Any(
                                            d =>
                                                d.DescriptionType.Code == DescTypeCodes.Warehouses &&
                                                d.DescriptionValue.Id == currWarehouse.ReferenceId))
                                    {
                                        existingOffer.SetDescription(DescTypeCodes.Warehouses,
                                            currWarehouse.ReferenceId,
                                            null, null, null, 0, 0);
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (XmlNode child in nomenChild.ChildNodes)
                            {
                                if (child.Attributes == null || child.Attributes["Код"] == null ||
                                    child.Attributes["Наименование"] == null)
                                    continue;
                                 var no = child.Attributes["Код"].Value;
                                var name = child.Attributes["Наименование"].Value;

                                var descTypeCode = 0L;
                                switch (nomenChild.Name)
                                {
                                    case "Organ" :
                                        descTypeCode = DescTypeCodes.Organizations;
                                        break;
                                    case "Kassa":
                                        descTypeCode = DescTypeCodes.CashBoxDepartments;
                                        break;
                                    case "Sklad":
                                        descTypeCode = DescTypeCodes.Warehouses;
                                        break;
                                    case "Magaz":
                                        descTypeCode = DescTypeCodes.Shops;
                                        break;
                                    case "Kontragent":
                                        descTypeCode = DescTypeCodes.Vendors;
                                        break;
                                }

                                var descType =
                                    DaoContainer.GetDao<DescriptionTypeDao>()
                                        .Linq()
                                        .FirstOrDefault(d => d.Code == descTypeCode);

                                if (descType != null)
                                {
                                    var descVal =
                                        DaoContainer.GetDao<DescriptionValueDao>()
                                            .Linq()
                                            .FirstOrDefault(d => d.DescriptionType == descType && d.OuterCode == no);

                                    if (descVal == null)
                                    {
                                        DaoContainer.GetDao<DescriptionValueDao>()
                                            .Save(new DescriptionValue()
                                            {
                                                OuterCode = no,
                                                Name = name,
                                                DescriptionType = descType
                                            });
                                    }
                                    else
                                    {
                                        descVal.OuterCode = no;
                                        descVal.Name = name;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            try
            {
                DaoContainer.CommitChanges();
                files.ForEach(File.Delete);
            }
            catch
            {
            }
        }

        private void LoadOffers()
        {
            var offerType = _mTDao.Linq().First(mt => mt.Code == MaterialTypesCodes.Offer);
            var warehousesType = _dTDao.Linq().First(dt => dt.Code == DescTypeCodes.Warehouses);
            var currCashBox = Settings.Find(x => x.Code == SettingsConst.CashBoxDepartment);

            var files =
                Directory.GetFiles(_config.AppSettings.Settings["AgentLoadFolder"].Value)
                    .Where(f => f.Contains("rem"))
                    .Where(f => f.Contains(currCashBox == null ? "" : currCashBox.ReferenceValue))
                    .ToList();

            foreach (var file in files)
            {
                var doc = new XmlDocument();
                doc.Load(file);
                var xmlNodeList = doc.SelectNodes("Root");
                if (xmlNodeList == null) 
                    continue;
                foreach (XmlNode node in xmlNodeList)
                {

                    foreach (XmlNode list in node.ChildNodes)
                    {
                        if (list.Attributes == null)
                            continue;
                        var elNo = list.Attributes["КодНоменклатуры"].Value;
                        var element = _mDao.Linq().FirstOrDefault(m => m.No == elNo);
                        if (element == null)
                            continue;
                        foreach (XmlNode storage in list.ChildNodes)
                        {
                            if (storage.Attributes == null)
                                continue;
                            var name = storage.Attributes["Характеристика"].Value;
                            var warehouse =
                                warehousesType.DescriptionValues.FirstOrDefault(
                                    dv => dv.Name == storage.Attributes["Склад"].Value);
                            if(warehouse == null)
                                continue;

                            int quantity;
                            int.TryParse(storage.Attributes["Количество"].Value, out quantity);

                            var code = storage.Attributes["Штрихкод"].Value;

                            decimal price;
                            decimal.TryParse(storage.Attributes["Цена"].Value, out price);

                            var existingOffer =
                                element.AllChildMaterials.FirstOrDefault(
                                    m =>
                                        m.Descriptions.Any(
                                            d =>
                                                (d.DescriptionType.Code == DescTypeCodes.Characteristic &&
                                                 d.StringValue == name)));
                            if (existingOffer != null)
                            {
                                existingOffer.SetDescription(DescTypeCodes.Warehouses, warehouse.Id, null, null, null,
                                    quantity, price,null,null,null,null,DescriptionSearchMode.Value);
                            }
                            else
                            {
                                existingOffer = new Material
                                {
                                    MaterialType = offerType,
                                    Name = element.Name + " " + name,
                                    Code = code
                                };
                                existingOffer.SetDescription(DescTypeCodes.Warehouses, warehouse.Id, null, null, null,
                                    quantity, price);
                                existingOffer.SetDescription(DescTypeCodes.Characteristic, name);

                                element.ChildMaterials.Add(new MaterialRelation()
                                {
                                    ChildMaterial = existingOffer,
                                    ParentMaterial = element,
                                });
                            }
                            _mDao.Save(existingOffer);
                        }
                    }
                }
            }
            try
            {
                DaoContainer.CommitChanges();
                files.ForEach(File.Delete);
            }
            catch
            {
            }
        }

        [AgentJob(20)]
        public void UnLoadAll()
        {
            _config =
              ConfigurationManager.OpenExeConfiguration(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            
            if (!Directory.Exists(_config.AppSettings.Settings["AgentUnloadFolder"].Value))
                return;

            var shift = DaoContainer.GetDao<DocumentDao>()
                .Linq()
                .FirstOrDefault(
                    d =>
                        d.DocumentType.Code == DocumentTypeCodes.Shift && d.DocumentStatus == DocumentStatus.Closed &&
                        d.Descriptions.All(desc => desc.DescriptionType.Code != DescTypeCodes.ShiftIsUnloaded));

            if (shift == null)
                return;

            var cashBoxDept = Settings.Find(s => s.Code == SettingsConst.CashBoxDepartment);
            var shop = Settings.Find(s => s.Code == SettingsConst.ShopName);
            var org = Settings.Find(s => s.Code == SettingsConst.OrgName);
            var currWarehouse = Settings.Find(x => x.Code == SettingsConst.Warehouse);

            var unloadXml = new XmlDocument();
            unloadXml.AppendChild(unloadXml.CreateXmlDeclaration("1.0", "UTF-8", ""));

            var rootEl = unloadXml.CreateElement("Root");
            var itemEl = unloadXml.CreateElement("item");

            itemEl.SetAttribute("Ссылка", "Отчет о продажах от " + shift[DescTypeCodes.ShiftDate].EndDate);
            itemEl.SetAttribute("ВерсияДанных", "AAAAAQAAABw=");
            itemEl.SetAttribute("ПометкаУдаления", "Нет");
            itemEl.SetAttribute("Номер", "");
            itemEl.SetAttribute("Дата", shift[DescTypeCodes.ShiftDate].EndDate.ToString());
            itemEl.SetAttribute("Проведен", "Да");
            itemEl.SetAttribute("КассаККМ", cashBoxDept == null ? "" : cashBoxDept.ReferenceOuterCode);//TODO взять из настройки
            itemEl.SetAttribute("Комментарий", "");
            itemEl.SetAttribute("Ответственный", "");
            itemEl.SetAttribute("СуммаДокумента",
                shift.DerivedDocuments.Where(d => d.DocumentType.Code == DocumentTypeCodes.Cheque)
                    .Sum(d => d[DescTypeCodes.Sum].MinValue)
                    .ToString());
            itemEl.SetAttribute("СуммаВозвратов",
                shift.DerivedDocuments.Where(d => d.DocumentType.Code == DocumentTypeCodes.Return)
                    .Sum(d => d[DescTypeCodes.Sum].MinValue)
                    .ToString());
            itemEl.SetAttribute("Магазин", shop == null ? "" : shop.ReferenceOuterCode);//TODO взять из настройки
            itemEl.SetAttribute("ДоговорЭквайринга", "");
            itemEl.SetAttribute("Эквайрер", "");
            itemEl.SetAttribute("ЦенаВключаетНДС", "0");
            itemEl.SetAttribute("УчитыватьНДС", "0");
            itemEl.SetAttribute("ОплатаПодарочнымиСертификатами", "0");
            itemEl.SetAttribute("Организация", org == null ? "" : org.ReferenceOuterCode);//TODO взять из настройки
            itemEl.SetAttribute("КассоваяСмена", "");

            var strSaleIndex = 1;
            var strReturnIndex = 1;
            foreach (var cheque in shift.DerivedDocuments.Where(d=>d.DocumentType.Code == DocumentTypeCodes.Cheque || d.DocumentType.Code == DocumentTypeCodes.Return))
            {
                var isReturn = cheque.DocumentType.Code == DocumentTypeCodes.Return;
                //var returns =
                  //      cheque.DerivedDocuments.Where(d => d.DocumentType.Code == DocumentTypeCodes.Return).ToList();
                foreach (var chequeLine in cheque.Lines)
                {
                   /* var returnLine =
                        returns.Where(r => r.Lines.Any(rl => rl.Material == chequeLine.Material))
                            .SelectMany(r => r.Lines.Where(rl => rl.Material == chequeLine.Material))
                            .ToList();*/

                    //var quantity = returnLine!=null

                    var sum = chequeLine[DescTypeCodes.Quantity].MinValue*chequeLine[DescTypeCodes.Price].MinValue -
                              (chequeLine[DescTypeCodes.Discount] == null
                                  ? 0
                                  : (chequeLine[DescTypeCodes.Discount].MinValue ?? 0));

                    var strEl = unloadXml.CreateElement( isReturn ? "strreturn" : "strsale");
                    if (isReturn)
                        strEl.SetAttribute("Резервировать", "ТаблицаЗначений");

                    strEl.SetAttribute("Ссылка", "Отчет о продажах от" + shift[DescTypeCodes.ShiftDate].EndDate);

                    strEl.SetAttribute("НомерСтроки",
                        (isReturn ? strReturnIndex : strSaleIndex).ToString(CultureInfo.InvariantCulture));

                    strEl.SetAttribute("Номенклатура",
                        chequeLine.Material == null || chequeLine.Material.FirstParentMaterial == null
                            ? ""
                            : chequeLine.Material.FirstParentMaterial.No);

                    strEl.SetAttribute("Характеристика",
                        chequeLine.Material == null || chequeLine.Material[DescTypeCodes.Characteristic] == null
                            ? ""
                            : chequeLine.Material[DescTypeCodes.Characteristic].StringValue);
                    strEl.SetAttribute("Количество",  chequeLine[DescTypeCodes.Quantity].MinValue.ToString());
                    strEl.SetAttribute("Цена", chequeLine[DescTypeCodes.Price].MinValue.ToString());
                    strEl.SetAttribute("Сумма", sum.ToString());
                    strEl.SetAttribute("СтавкаНДС",
                        chequeLine.Material == null || chequeLine.Material.FirstParentMaterial == null ||
                        chequeLine.Material.FirstParentMaterial[DescTypeCodes.Nds] == null
                            ? ""
                            : chequeLine.Material.FirstParentMaterial[DescTypeCodes.Nds].MinValue.ToString());

                    strEl.SetAttribute("СуммаНДС", "0");//TODO РАСЧИТАТЬ ПО СТАВКЕ ВЫШЕ
                    strEl.SetAttribute("РегистрацияПродажи", "");
                    strEl.SetAttribute("Склад",
                        currWarehouse == null ? "" : currWarehouse.ReferenceOuterCode);
                    strEl.SetAttribute("Продавец", "");
                    strEl.SetAttribute("ДисконтнаяКарта", "");
                    
                    if (isReturn)
                        strEl.SetAttribute("АналитикаХозяйственнойОперации", "Возврат");//TODO ВЗЯТЬ ИЗ СПРАВОЧНИКА

                    strEl.SetAttribute("КоличествоУпаковок", chequeLine[DescTypeCodes.Quantity].MinValue.ToString());
                    strEl.SetAttribute("Упаковка", "");
                    strEl.SetAttribute("КлючСвязи", "0");
                    if (!isReturn)
                    {
                        strEl.SetAttribute("СтатусУказанияСерий", "0");
                        strEl.SetAttribute("КодСтроки", "0");
                        strEl.SetAttribute("ЗаказПокупателя", "");
                        strEl.SetAttribute("Резервировать", "Нет");
                    }

                    itemEl.AppendChild(strEl);
                    if (isReturn)
                        strReturnIndex++;
                    else
                        strSaleIndex++;

                }
            }

            rootEl.AppendChild(itemEl);
            unloadXml.AppendChild(rootEl);

            unloadXml.Save(_config.AppSettings.Settings["AgentUnloadFolder"].Value + "sales" + "_" + cashBoxDept.ReferenceValue + "_" +
                           shift[DescTypeCodes.ShiftDate].EndDate.ToString().Replace(".", "_").Replace(":", "_") +
                           ".xml");

            shift.SetDescription(DescTypeCodes.ShiftIsUnloaded, 1);
            DaoContainer.CommitChanges();
        }
    }
}
