﻿using System.Runtime.Serialization;
using TB.BOL.Documents;
using TB.DAL.DAO.Documents;
using TB.DAL.Interface;
using TB.Services.Infrastructure.Attributes.Agent;

namespace TB.Cashier.Server.Agents.Mail
{
    [KnownType(typeof (SendAgent))]
    [Agent]
    public class SendAgent
    {
        private IDao<Document> _docDao = DaoContainer.GetDao<DocumentDao>();

        [AgentJob(30)]
        public void SendMail()
        {
           // _docDao.Linq().
        }
    }
}
