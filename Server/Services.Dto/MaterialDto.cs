﻿using System.Collections.Generic;
using TB.Services.Infrastructure.Dto;

namespace TB.Cashier.Server.Services.Dto
{
    public class MaterialDto : DtoBase
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public string Warehouse { get; set; }

        public string Vendor { get; set; }

        public MaterialDto Folder { get; set; }

        public MaterialDto CatalogElement { get; set; }

        public List<MaterialDto> FolderElements { get; set; }

        public List<MaterialDto> Offers { get; set; }

        public bool IsFolder { get; set; }

        public bool IsOffer { get; set; }

        public bool IsCatalogElement { get; set; }

        public bool IsHasOnCurrentWarehouse { get; set; }

    }
}
