﻿using System;
using System.Runtime.Serialization;
using TB.Services.Infrastructure.Dto;

namespace TB.Cashier.Server.Services.Dto
{
    public class ShiftDto : DtoBase
    {

        /// <summary>
        /// Дата начала
        /// </summary>
        [DataMember]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата конца
        /// </summary>
        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public string ShiftDateShort {
            get { return StartDate.ToShortDateString() + " c " + StartDate.ToShortTimeString(); }
        }

        [DataMember]
        public string ShiftDateLong
        {
            get { return StartDate + " - " + EndDate; }
        }
    }

}
