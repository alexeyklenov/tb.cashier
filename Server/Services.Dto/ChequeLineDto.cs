﻿using System;
using System.Runtime.Serialization;
using TB.Services.Infrastructure.Dto;

namespace TB.Cashier.Server.Services.Dto
{
    [DataContract]
    public class ChequeLineDto : DtoBase
    {
        /// <summary>
        /// Идентификатор документа
        /// </summary>
        [DataMember]
        public Int64? ChequeId { get; set; }

        /// <summary>
        /// Номер строки
        /// </summary>
        [DataMember]
        public Int64? No { get; set; }

        /// <summary>
        /// Наименование связного материала
        /// </summary>
        [DataMember]
        public string MaterialName { get; set; }

        /// <summary>
        /// Ид связного материала
        /// </summary>
        [DataMember]
        public long MaterialId { get; set; }

        /// <summary>
        /// Артикул связного материала
        /// </summary>
        [DataMember]
        public string MaterialNo { get; set; }

        /// <summary>
        /// Наименование производителя
        /// </summary>
        [DataMember]
        public string VendorName{ get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        [DataMember]
        public int Quantity { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        [DataMember]
        public decimal Price { get; set; }

        /// <summary>
        /// Скидка
        /// </summary>
        [DataMember]
        public decimal Discount { get; set; }

        /// <summary>
        /// Сумма строчки чека
        /// </summary>
        [DataMember]
        public decimal Sum { get; set; }
    }
}
