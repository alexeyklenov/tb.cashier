﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TB.Services.Infrastructure.Dto;

namespace TB.Cashier.Server.Services.Dto
{
    [DataContract]
    public class ChequeDto : DtoBase
    {
        /// <summary>
        /// Код документа
        /// </summary>
        [DataMember]
        public Int64? No { get; set; }

        /// <summary>
        /// Дата документа
        /// </summary>
        [DataMember]
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма чека
        /// </summary>
        [DataMember]
        public decimal Sum { get; set; }

        /// <summary>
        /// Сумма, вносимая клиентом
        /// </summary>
        [DataMember]
        public decimal SumByClient { get; set; }

        /// <summary>
        /// Код формы оплаты
        /// </summary>
        [DataMember]
        public Dictionary<long, decimal> PaymentTypeSum { get; set; }
        
        /// <summary>
        /// Является ли чек возвратом 
        /// </summary>
        [DataMember]
        public bool IsReturn { get; set; }

        /// <summary>
        /// Является ли чек отложенным
        /// </summary>
        [DataMember]
        public bool IsPostponed { get; set; }

        /// <summary>
        /// Является ли чек закрытым
        /// </summary>
        [DataMember]
        public bool IsClosed { get; set; }

        /// <summary>
        /// Количество по всем строкам
        /// </summary>
        [DataMember]
        public int Quantity { get; set; }

        /// <summary>
        /// Агрегат наименований связный материалов
        /// </summary>
        [DataMember]
        public string MaterialName { get; set; }

        /// <summary>
        /// Строчки документа
        /// </summary>
        [DataMember]
        public List<ChequeLineDto> Lines { get; set; }
    }
}
