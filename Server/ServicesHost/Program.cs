﻿using System.IO;
using System.ServiceProcess;

namespace TB.Cashier.Server.ServicesHost
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        static void Main()
        {
            var servicesToRun = new ServiceBase[] 
            { 
                new MainService() 
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}
