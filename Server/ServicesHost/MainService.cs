﻿using System;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using log4net;
using log4net.Config;
using TB.Services.Infrastructure.Server;

namespace TB.Cashier.Server.ServicesHost
{
    public partial class MainService : ServiceBase
    {
        private ILog Log;
        public MainService()
        {
            InitializeComponent();

            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

            XmlConfigurator.ConfigureAndWatch(new FileInfo(ConfigurationManager.AppSettings["LogConfigPath"]));

            Log = LogManager.GetLogger("Main");
        }

        protected override void OnStart(string[] args)
        {
            Log.Info("Запуск сервера");
            WatchServiceHoster.Start(string.Empty);
        }

        protected override void OnStop()
        {
            Log.Info("Сервер остановлен");
        }
    }
}
