﻿using System.Collections.Generic;
using System.Linq;
using TB.BOL;
using TB.BOL.Documents;
using TB.BOL.Interfaces;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces.Const;
using TB.DAL.DAO.Documents;
using TB.DAL.Interface;
using TB.Services.Infrastructure.Dto;
using TB.Services.Infrastructure.Translators;

namespace TB.Cashier.Server.Services.DtoTranslators
{
    [TargetDto(typeof(ChequeDto))]
    public class ChequeDtoTranslator: DtoTranslator
    {
        public ChequeDtoTranslator()
        {
            DtoType = typeof (ChequeDto);
        }

        protected override DtoBase ToDtoInternal(DomainObject<long> source)
        {
            var chequeDto = base.ToDtoInternal(source) as ChequeDto;
            var chequeDoc = source as Document;

            if (chequeDto != null && chequeDoc != null)
            {
                chequeDto.Id = chequeDoc.Id;
                chequeDto.Date = chequeDoc.DocumentDate;
                chequeDto.No = chequeDoc.No;
                chequeDto.Lines = chequeDoc.Lines.Select(l => GetTranslator<ChequeLineDto>().ToDto(l) as ChequeLineDto).ToList();

                chequeDto.IsReturn = chequeDoc.DocumentType.Code == DocumentTypeCodes.Return;

                chequeDto.IsPostponed = chequeDoc.DocumentStatus == DocumentStatus.Delayed;

                chequeDto.IsClosed = chequeDoc.DocumentStatus == DocumentStatus.Closed;

                chequeDto.Sum = chequeDto.Lines.Sum(l => l.Sum);

                chequeDto.Quantity = chequeDto.Lines.Sum(l => l.Quantity);

                if (chequeDto.Lines.Any())
                    chequeDto.MaterialName =
                        chequeDto.Lines.Select(l => l.MaterialName).Aggregate((s, s1) => s + ";" + s1);

                var sumByClient = chequeDoc[DescTypeCodes.SumByClient];
                if(sumByClient!=null)
                    chequeDto.SumByClient = sumByClient.MinValue.GetValueOrDefault();


                chequeDto.PaymentTypeSum = new Dictionary<long, decimal>();
                foreach (var payment in chequeDoc.GetDescriptions(DescTypeCodes.PaymentType))
                {
                    chequeDto.PaymentTypeSum.Add(payment.DescriptionValue.Code, payment.MinValue.GetValueOrDefault());
                }

                
            }

            return chequeDto;
        }

        public override DomainObject<long> ToBusiness(DtoBase source)
        {
            var chequeDto = source as ChequeDto;
            if (chequeDto == null)
                return null;

            var cheque = chequeDto.Id == null
                ? new Document()
                : DaoContainer.GetDao<DocumentDao>().GetById(chequeDto.Id, false);

            cheque.DocumentDate = chequeDto.Date;
            cheque.No = chequeDto.No.GetValueOrDefault();
            foreach (var payment in chequeDto.PaymentTypeSum)
            {
                cheque.SetDescription(DescTypeCodes.PaymentType, null, payment.Key, null, null, payment.Value, null,
                    null, null, null, null, DescriptionSearchMode.Value);
            }
                
            cheque.SetDescription(DescTypeCodes.SumByClient, chequeDto.SumByClient);
            cheque.SetDescription(DescTypeCodes.Sum, chequeDto.Sum);

            return cheque;
        }
    }
}
