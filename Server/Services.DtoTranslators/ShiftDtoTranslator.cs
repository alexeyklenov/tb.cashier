﻿using TB.BOL;
using TB.BOL.Documents;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces.Const;
using TB.Services.Infrastructure.Dto;
using TB.Services.Infrastructure.Translators;

namespace TB.Cashier.Server.Services.DtoTranslators
{
    [TargetDto(typeof(ShiftDto))]
    public class ShiftDtoTranslator: DtoTranslator
    {
        public ShiftDtoTranslator()
        {
            DtoType = typeof(ShiftDto);
        }

        protected override DtoBase ToDtoInternal(DomainObject<long> source)
        {
            var shiftDto = base.ToDtoInternal(source) as ShiftDto;
            var shiftDoc = source as Document;

            if (shiftDto != null && shiftDoc != null)
            {
                shiftDto.Id = shiftDoc.Id;
                shiftDto.StartDate = shiftDoc[DescTypeCodes.ShiftDate].BeginDate.GetValueOrDefault();
                shiftDto.EndDate = shiftDoc[DescTypeCodes.ShiftDate].EndDate.GetValueOrDefault();
            }

            return shiftDto;
        }
    }
}
