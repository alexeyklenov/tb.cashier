﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using TB.BOL;
using TB.BOL.Materials;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces.Const;
using TB.Services.Common;
using TB.Services.Common.Dto.Administration;
using TB.Services.Infrastructure.Dto;
using TB.Services.Infrastructure.Translators;

namespace TB.Cashier.Server.Services.DtoTranslators
{
    [TargetDto(typeof(MaterialDto))]
    public class MaterialDtoTranslator : DtoTranslator
    {
        private List<SettingDto> _settings;
        private List<SettingDto> Settings
        {
            get { return _settings ?? (_settings = new SettingService().GetAllSettings()); }
        }

        public MaterialDtoTranslator()
        {
            DtoType = typeof(MaterialDto);
        }

        protected override DtoBase ToDtoInternal(DomainObject<long> source)
        {
            var materialDto = base.ToDtoInternal(source) as MaterialDto;
            var material = source as Material;

            if (materialDto != null && material != null)
            {
                
                var currWarehouse = Settings.Find(x => x.Code == SettingsConst.Warehouse);

                materialDto.Name = material.Name;
                materialDto.Code = material.Code;

                materialDto.IsFolder = material.MaterialType.Code == MaterialTypesCodes.Folder;
                materialDto.IsOffer = material.MaterialType.Code == MaterialTypesCodes.Offer;
                materialDto.IsCatalogElement = material.MaterialType.Code == MaterialTypesCodes.Catalog;

                if (materialDto.IsOffer)
                {
                   /* materialDto.CatalogElement =
                        GetTranslator<MaterialDto>().ToDto(material.FirstParentMaterial) as MaterialDto;*/
                    /* if (material[DescTypeCodes.Price] != null)
                         materialDto.Price = material[DescTypeCodes.Warehouses].MaxValue.GetValueOrDefault();
                     if (material[DescTypeCodes.Quantity] != null)
                         materialDto.Quantity = (int)material[DescTypeCodes.Warehouses].MinValue.GetValueOrDefault();

                     var warehouseDesc = material.Descriptions.FirstOrDefault(d => d.DescriptionType.Code == DescTypeCodes.Warehouses && d.DescriptionValue.ResultShortName == currWarehouse.ReferenceValue);
                     if (warehouseDesc != null)
                         materialDto.Warehouse = material[DescTypeCodes.Warehouses].DescriptionValue.ResultShortName;*/

                    var warehouseDescs =
                        material.Descriptions.Where(d => d.DescriptionType.Code == DescTypeCodes.Warehouses).ToList();
                    if (warehouseDescs.Any())
                    {
                        var currWarehouseDesc =
                            warehouseDescs.FirstOrDefault(
                                d => d.DescriptionValue.ResultShortName == currWarehouse.ReferenceValue);

                        if (currWarehouseDesc != null)
                        {
                            materialDto.Price = currWarehouseDesc.MaxValue.GetValueOrDefault();
                            materialDto.Quantity = (int) currWarehouseDesc.MinValue.GetValueOrDefault();
                            materialDto.IsHasOnCurrentWarehouse = materialDto.Quantity > 0;
                        }
                        //Пытаемся окончательно заполнить цену с другого склада
                        if (materialDto.Price == 0)
                        {
                            var warehouseDesc = warehouseDescs.FirstOrDefault(d => d.MaxValue > 0);
                            if (warehouseDesc != null)
                                materialDto.Price = warehouseDesc.MaxValue.GetValueOrDefault();
                        }
                        //Собираем общую строчку для всех складов с количеством и ценной
                        foreach (var warehouse in warehouseDescs)
                        {
                            materialDto.Warehouse += warehouse.DescriptionValue.ResultShortName + ":" +
                                                     (int)warehouse.MinValue + " ";
                        }
                    }

                    if (material.FirstParentMaterial != null &&
                        material.FirstParentMaterial[DescTypeCodes.Vendors] != null)
                        materialDto.Vendor =
                            material.FirstParentMaterial[DescTypeCodes.Vendors].DescriptionValue.ResultShortName;

                }
                if (materialDto.IsCatalogElement)
                {
                    //materialDto.Folder = GetTranslator<MaterialDto>().ToDto(material.FirstParentMaterial) as MaterialDto;
                    materialDto.Offers =
                        material.ChildMaterials.Where(
                            cm => cm.ChildMaterial.MaterialType.Code == MaterialTypesCodes.Offer).Select(
                                cm => GetTranslator<MaterialDto>().ToDto(cm.ChildMaterial) as MaterialDto)
                            .ToList();
                }
                if (materialDto.IsFolder)
                {
                    materialDto.FolderElements =
                        material.ChildMaterials.Select(
                            r => GetTranslator<ChequeLineDto>().ToDto(r.ChildMaterial) as MaterialDto).ToList();
                }
            }

            return materialDto;
        }
    }
}
