﻿using System;
using TB.BOL;
using TB.BOL.Documents;
using TB.BOL.Interfaces;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces.Const;
using TB.DAL.Interface;
using TB.Services.Infrastructure.Dto;
using TB.Services.Infrastructure.Translators;

namespace TB.Cashier.Server.Services.DtoTranslators
{
    [TargetDto(typeof(ChequeLineDto))]
    class ChequeLineDtoTranslator : DtoTranslator
    {
        public ChequeLineDtoTranslator()
        {
            DtoType = typeof(ChequeLineDto);
        }

        protected override DtoBase ToDtoInternal(DomainObject<Int64> source)
        {
            //TODO - перевести материалы на materialDto
            var chequeLineDto = base.ToDtoInternal(source) as ChequeLineDto;
            var chequeLine = source as DocumentLine;
           
            if (chequeLineDto != null && chequeLine != null)
            {
                chequeLineDto.Id = chequeLine.Id;
                chequeLineDto.ChequeId = chequeLine.Document.Id;
                if (chequeLine.Material != null)
                {
                    chequeLineDto.MaterialId = chequeLine.Material.Id;
                    chequeLineDto.MaterialNo = chequeLine.Material.No;
                    chequeLineDto.MaterialName = chequeLine.Material.Name;
                    if (chequeLine.Material.FirstParentMaterial[DescTypeCodes.Vendors] != null)
                        chequeLineDto.VendorName =
                            chequeLine.Material.FirstParentMaterial[DescTypeCodes.Vendors].DescriptionValue
                                .ResultShortName;
                }
                chequeLineDto.No = chequeLine.No;

                chequeLineDto.Quantity = Convert.ToInt32(chequeLine[DescTypeCodes.Quantity].MinValue);
                chequeLineDto.Price = chequeLine[DescTypeCodes.Price].MinValue.GetValueOrDefault();
                chequeLineDto.Discount = chequeLine[DescTypeCodes.Discount].MinValue.GetValueOrDefault();
                /*если скидка больше суммы, то 
                  прировнять ее к сумме*/
                if (chequeLineDto.Discount > chequeLineDto.Price*chequeLineDto.Quantity)
                    chequeLineDto.Discount = chequeLineDto.Price*chequeLineDto.Quantity;

                chequeLineDto.Sum = (chequeLineDto.Price * chequeLineDto.Quantity) - chequeLineDto.Discount;
            }

            return chequeLineDto;
        }

        public override DomainObject<long> ToBusiness(DtoBase source)
        {
            var chequeLineDto = source as ChequeLineDto;
            if (chequeLineDto == null)
                return null;

            var chequeLine = chequeLineDto.Id == null
              ? new DocumentLine()
              : DaoContainer.Dao<DocumentLine>().GetById(chequeLineDto.Id, false);

            chequeLine.SetDescription(DescTypeCodes.Discount, chequeLineDto.Discount);
            chequeLine.SetDescription(DescTypeCodes.Quantity, (decimal)chequeLineDto.Quantity);
            chequeLine.SetDescription(DescTypeCodes.Price, chequeLineDto.Price);

            return chequeLine;
        }
    }
}
