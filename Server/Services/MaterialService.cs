﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Linq;
using TB.BOL.Materials;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Cashier.Server.Services.Interfaces.Const;
using TB.DAL.Interface;
using TB.Services.Infrastructure.Server;
using TB.Services.Infrastructure.Translators;

namespace TB.Cashier.Server.Services
{
        public class MaterialService : ClientServiceBase, IMaterialService
    {
            public List<MaterialDto> SearchMaterialsByName(string name)
            {
                var query = DaoContainer.Dao<Material>()
                    .Linq()
                    .Where(m => m.MaterialType.Code == MaterialTypesCodes.Offer);

                query = name.Split('%')
                    .Aggregate(query, (current, nameEl) => current.Where(m => m.Name.Contains(nameEl)));

                if (name == string.Empty)
                    query = query.Take(100);


                return query.Fetch(x => x.Descriptions)
                    .ToList()
                    .Select(m => DtoTranslator.GetTranslator<MaterialDto>().ToDto(m) as MaterialDto).ToList();
            }

            public List<MaterialDto> GetAllMaterialsByFolders()
            {
                var materials = DaoContainer.Dao<Material>()
                        .Linq()
                        .Where(
                            m =>
                                m.MaterialType.Code == MaterialTypesCodes.Folder ||
                                (m.MaterialType.Code == MaterialTypesCodes.Catalog &&
                                 m.ParentMaterials.All(pm => pm.ParentMaterial.MaterialType.Code != MaterialTypesCodes.Folder)))
                        .ToList();

                return
                    materials.Select(m => DtoTranslator.GetTranslator<MaterialDto>().ToDto(m) as MaterialDto).ToList();


            }
    }
}
