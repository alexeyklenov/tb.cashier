﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using TB.BOL.Descriptions;
using TB.BOL.Documents;
using TB.BOL.Generators;
using TB.BOL.Interfaces;
using TB.BOL.Materials;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Cashier.Server.Services.Interfaces.Const;
using TB.Cashier.Server.Services.Interfaces.Faults;
using TB.DAL.DAO.Documents;
using TB.DAL.Interface;
using TB.Services.Common;
using TB.Services.Common.Dto.Administration;
using TB.Services.Infrastructure.Server;
using TB.Services.Infrastructure.Translators;

namespace TB.Cashier.Server.Services
{
    public class СhequeService : ClientServiceBase, IChequeService
    {

        private List<SettingDto> _settings;
        private List<SettingDto> Settings
        {
            get { return _settings ?? (_settings = new SettingService().GetAllSettings()); }
        }

        public int CheckCodeIsMaterial(string code)
        {
            if (
                DaoContainer.Dao<Material>()
                    .Linq()
                    .Any(
                        x =>
                            x.Code == code && x.MaterialType.Code == MaterialTypesCodes.Offer &&
                            x.MaterialStatus != MaterialStatus.Archived))
                return 1;
            return DaoContainer.Dao<Material>()
                .Linq()
                .Any(
                    x =>
                        x.Code == code && x.MaterialType.Code == MaterialTypesCodes.Catalog &&
                        x.MaterialStatus != MaterialStatus.Archived)
                ? 2
                : 0;
        }

        #region Чек

        public List<ChequeDto> GetAllChequeByCurrentShift()
        {
            CheckShift();
            return
                CurrentShift.DerivedDocuments.Select(d => DtoTranslator.GetTranslator<ChequeDto>().ToDto(d) as ChequeDto)
                    .ToList();
        }

        public ChequeDto GetActiveCheque()
        {
            CheckShift();

            var docDao = DaoContainer.GetDao<DocumentDao>();

            var cheque =
                docDao.Linq()
                    .FirstOrDefault(
                        c =>
                            c.DocumentType.Code == DocumentTypeCodes.Cheque &&
                            c.DocumentStatus == DocumentStatus.Created);
            if (cheque != null)
                return DtoTranslator.GetTranslator(typeof(ChequeDto)).ToDto(cheque) as ChequeDto;

            var newChequeDoc = new Document
            {
                DocumentType =
                    DaoContainer.Dao<DocumentType>()
                        .Linq()
                        .First(dt => dt.Code == DocumentTypeCodes.Cheque),
                DocumentStatus = DocumentStatus.Created,
                No =
                    (long)
                        DaoContainer.Dao<Generator>()
                            .Linq()
                            .First(g => g.Code == GeneratorsCodes.ChequeGenerator)
                            .NextValue
            };
            docDao.Save(newChequeDoc);

            CurrentShift.AddDocumentRelation(DocumentRelationType.Derived, newChequeDoc);

            DaoContainer.CommitChanges();

            return DtoTranslator.GetTranslator(typeof(ChequeDto)).ToDto(newChequeDoc) as ChequeDto;
        }

        public void ActivateCheque(long id)
        {
            CheckShift();
            DaoContainer.GetDao<DocumentDao>().GetById(id, false).DocumentStatus = DocumentStatus.Created;
            DaoContainer.CommitChanges();
        }

        public void ClearChequeById(long id)
        {
            var cheque = DaoContainer.GetDao<DocumentDao>().GetById(id, false);
            cheque.Lines.Clear();
            DaoContainer.CommitChanges();

            if (cheque.DocumentType.Code == DocumentTypeCodes.Return)
                DeleteChequeById(id);
        }

        public ChequeDto GetChequeById(long id)
        {
            try
            {
                return
                    DtoTranslator.GetTranslator(typeof (ChequeDto))
                        .ToDto(DaoContainer.GetDao<DocumentDao>().GetById(id, false)) as ChequeDto;
            }
            catch
            {
                return GetActiveCheque();
            }

        }

        public void DeleteChequeById(long id)
        {
            var cheque = DaoContainer.GetDao<DocumentDao>().GetById(id, false);
            if (cheque.DocumentStatus == DocumentStatus.Delayed || cheque.DocumentStatus == DocumentStatus.Created)
                DaoContainer.GetDao<DocumentDao>().Delete(cheque);
           
            DaoContainer.CommitChanges();

            CheckShift();

        }

        public void DeleteChequeLineById(long id)
        {
            //TODO Сделать удаление сразу по ИД
            var chequeLine = DaoContainer.Dao<DocumentLine>().GetById(id, false);
            DaoContainer.Dao<DocumentLine>().Delete(chequeLine);
            DaoContainer.CommitChanges();
        }

        public void UpdateChequeLine(ChequeLineDto chequeLine)
        {
            DtoTranslator.GetTranslator<ChequeLineDto>().ToBusiness(chequeLine);
            DaoContainer.CommitChanges();
        }

        public void AddFreeLineToCheque(long chequeId)
        {
            CheckShift();

            var chequeDoc = DaoContainer.GetDao<DocumentDao>().GetById(chequeId, false);
            var newLine = new DocumentLine(chequeDoc);
            newLine.SetDescription(DescTypeCodes.Price, (decimal)0);
            newLine.SetDescription(DescTypeCodes.Quantity, (decimal)1);
            newLine.SetDescription(DescTypeCodes.Discount, true);
            DaoContainer.CommitChanges();

        }

        public void AddMaterialToChequeByCode(long chequeId, string materialCode)
        {
            CheckShift();

            var chequeDoc = DaoContainer.GetDao<DocumentDao>().GetById(chequeId, false);

            var material =
                DaoContainer.Dao<Material>()
                    .Linq()
                    .FirstOrDefault(
                        x =>
                            x.Code == materialCode);

            if (material == null)
            {
                var errorMsg = string.Format("Материал с кодом {0} не найден!", materialCode);
                Log.Error(errorMsg);
                throw new Exception(errorMsg);
            }

            var newLine = chequeDoc.AddDocumentLine(material, false);
            //TODO - как-то сложно выглядет подбор цены + дублирует код из MaterialDtoTranslator - может вынести в extension подбор цены или в статику для materialService
            var currWarehouse = Settings.FirstOrDefault(s => s.Code == SettingsConst.Warehouse);

            var warehouseDescs =
                        material.Descriptions.Where(d => d.DescriptionType.Code == DescTypeCodes.Warehouses).ToList();
            if (warehouseDescs.Any())
            {
                var currWarehouseDesc =
                    warehouseDescs.FirstOrDefault(
                        d => d.DescriptionValue.ResultShortName == currWarehouse.ReferenceValue);

                if (currWarehouseDesc != null)
                {
                    newLine.SetDescription(DescTypeCodes.Price, currWarehouseDesc.MaxValue.GetValueOrDefault());
                }
                //Пытаемся окончательно заполнить цену с другого склада
                if (newLine[DescTypeCodes.Price]==null || newLine[DescTypeCodes.Price].MinValue == 0)
                {
                    var warehouseDesc = warehouseDescs.FirstOrDefault(d => d.MaxValue > 0);
                    if (warehouseDesc != null)
                        newLine.SetDescription(DescTypeCodes.Price, warehouseDesc.MaxValue.GetValueOrDefault());
                }
            }
            else
                newLine.SetDescription(DescTypeCodes.Price, (decimal) 0);
            

            //TODO - сделать контроль остатков из настроек
            //if (material[DescTypeCodes.Quantity].MinValue - newLine[DescTypeCodes.Quantity].MinValue > 0)
            //{
            if (newLine.Id == 0)
            {
                newLine.SetDescription(DescTypeCodes.Quantity, (decimal)1);
                newLine.SetDescription(DescTypeCodes.Discount, true);
            }
            else
                newLine[DescTypeCodes.Quantity].MinValue++;
            // }

            DaoContainer.CommitChanges();

            // return DtoTranslator.GetTranslator(typeof(ChequeDto)).ToDto(chequeDoc) as ChequeDto;
        }

        public void CloseChequeById(long id)
        {
            CheckShift();
            var chequeDoc = DaoContainer.GetDao<DocumentDao>().GetById(id, false);
            chequeDoc.DocumentStatus = DocumentStatus.Closed;

            //уменьшаем или увеличиваем количество остатков

            var currWarehouse = Settings.Find(x => x.Code == SettingsConst.Warehouse);
            
            foreach (var line in chequeDoc.Lines)
            {
                if (line.Material == null) 
                    continue;
                var materialWarehouse = line.Material.Descriptions.FirstOrDefault(
                    d =>
                        d.DescriptionType.Code == DescTypeCodes.Warehouses &&
                        d.DescriptionValue.Id == currWarehouse.ReferenceId);
                if(materialWarehouse!=null)
                    materialWarehouse.MinValue = materialWarehouse.MinValue +
                                                 (line[DescTypeCodes.Quantity].MinValue*
                                                  chequeDoc.DocumentType.Code ==
                                                  DocumentTypeCodes.Return
                                                     ? 1
                                                     : -1);
            }

            DaoContainer.CommitChanges();
        }

        public void CloseCheque(ChequeDto cheque)
        {
            var chequeDoc = DtoTranslator.GetTranslator<ChequeDto>().ToBusiness(cheque) as Document;
            DaoContainer.CommitChanges();
            if (chequeDoc != null) CloseChequeById(chequeDoc.Id);
        }

        public void CopyChequeLineById(long lineId, long toChequeId)
        {
            var toCheque = DaoContainer.GetDao<DocumentDao>().GetById(toChequeId, false);

            var sourceLine = DaoContainer.Dao<DocumentLine>().GetById(lineId, false);

            var newLine = sourceLine.Material != null ? toCheque.AddDocumentLine(sourceLine.Material) : new DocumentLine(toCheque) ;
          
            newLine.SetDescription(DescTypeCodes.Quantity, sourceLine[DescTypeCodes.Quantity].MinValue);
            newLine.SetDescription(DescTypeCodes.Price, sourceLine[DescTypeCodes.Price].MinValue);
            
            if (sourceLine[DescTypeCodes.Discount].MinValue == null)
                newLine.SetDescription(DescTypeCodes.Discount, true);
            else
                newLine.SetDescription(DescTypeCodes.Discount, sourceLine[DescTypeCodes.Discount].MinValue);

            DaoContainer.CommitChanges();
        }

        public void CopyChequeLines(long fromChequeId, long toChequeId)
        {
            var fromCheque = DaoContainer.GetDao<DocumentDao>().GetById(fromChequeId, false);
            foreach (var line in fromCheque.Lines)
            {
                CopyChequeLineById(line.Id, toChequeId);
            }
        }

        public void CopyChequeLines(long fromChequeId, long toChequeId, List<long> chequeLinesIds)
        {
            var fromCheque = DaoContainer.GetDao<DocumentDao>().GetById(fromChequeId, false);
            foreach (var line in fromCheque.Lines)
            {
                if (chequeLinesIds.Contains(line.Id))
                    CopyChequeLineById(line.Id, toChequeId);
            }
        }

        #endregion

        #region Смена

        private Document CurrentShift { get; set; }

        public ShiftDto OpenShift()
        {

            CheckShift();

            if (CurrentShift == null)
            {
                CurrentShift = DaoContainer.GetDao<DocumentDao>().Save(new Document
                {
                    DocumentType = DaoContainer.Dao<DocumentType>()
                        .Linq()
                        .FirstOrDefault(dt => dt.Code == DocumentTypeCodes.Shift),
                    DocumentStatus = DocumentStatus.Created,
                });

                //TODO поправить чтобы дата конца бралась из настроек
                CurrentShift[DescTypeCodes.ShiftDate] = new Description
                {
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.Date.AddDays(1)
                };

                DaoContainer.CommitChanges();
            }

            return DtoTranslator.GetTranslator<ShiftDto>().ToDto(CurrentShift) as ShiftDto;
        }

        public ShiftDto GetCurrentShift()
        {
            CheckShift();
            if(CurrentShift==null)
                throw new FaultException<ShiftFault>(new ShiftFault(@"Необходимо открыть смену"));
            return DtoTranslator.GetTranslator<ShiftDto>().ToDto(CurrentShift) as ShiftDto;
        }

        private void CheckShift()
        {
            if (!IsCurrentShiftIsActive() && CurrentShift!=null)
                throw new FaultException<ShiftFault>(new ShiftFault(@"Необходимо закрыть смену"));
        }

        private bool IsCurrentShiftIsActive()
        {
            CurrentShift = DaoContainer.GetDao<DocumentDao>()
                .Linq()
                .FirstOrDefault(
                    d => d.DocumentType.Code == DocumentTypeCodes.Shift && d.DocumentStatus == DocumentStatus.Created);

            return CurrentShift!=null && CurrentShift[DescTypeCodes.ShiftDate].EndDate >= DateTime.Now;
        }

        public bool CloseCurrentShift()
        {
            IsCurrentShiftIsActive();
            if(CurrentShift == null)
                throw new FaultException<ShiftFault>(new ShiftFault(@"Необходимо открыть смену"));

            CurrentShift.DocumentStatus = DocumentStatus.Closed;
            CurrentShift[DescTypeCodes.ShiftDate].EndDate = DateTime.Now;

            //Удаляем все не закрытые и отложенные чеки
            CurrentShift.DerivedDocuments.Where(
                d => d.DocumentStatus == DocumentStatus.Created || d.DocumentStatus == DocumentStatus.Delayed)
                .All(d =>
                {
                    CurrentShift.RemoveDocumentRelation(DocumentRelationType.Derived, d);
                    DaoContainer.GetDao<DocumentDao>().Delete(d);
                    return true;
                });

            //подсчитываем наличность в кассе и сохраняем

            CurrentShift.SetDescription(DescTypeCodes.CashSumByShift, CurrentShiftCashSum());

            DaoContainer.CommitChanges();
            
            return true;
        }

        public decimal CashSumInBox()
        {
            var closedShiftsSum = DaoContainer.GetDao<DocumentDao>()
                .Linq()
                .Where(d => d.DocumentType.Code == DocumentTypeCodes.Shift && d.DocumentStatus == DocumentStatus.Closed)
                .ToList()
                .Select(d => d.Descriptions.First(desc => desc.DescriptionType.Code == DescTypeCodes.CashSumByShift))
                .Sum(x => x.MinValue).GetValueOrDefault();

            return closedShiftsSum + CurrentShiftCashSum();
        }

        public decimal CurrentShiftCashSum()
        {
            IsCurrentShiftIsActive();
            if (CurrentShift != null)
            {
                // return
                var w = CurrentShift.DerivedDocuments.Where(
                    d =>
                        (d.DocumentType.Code == DocumentTypeCodes.Cheque ||
                         d.DocumentType.Code == DocumentTypeCodes.MakeMoney) &&
                        d.DocumentStatus == DocumentStatus.Closed)
                    .Select(
                        d =>
                            d.Descriptions.Where(
                                desc =>
                                    desc.DescriptionType.Code == DescTypeCodes.PaymentType &&
                                    desc.DescriptionValue.Code == DescValCodes.Cash).Sum(desc => desc.MinValue))
                    .Sum()
                    .GetValueOrDefault()
                    ;
                var q = CurrentShift.DerivedDocuments.Where(
                    d =>
                        (d.DocumentType.Code == DocumentTypeCodes.Return ||
                         d.DocumentType.Code == DocumentTypeCodes.IssuanceMoney) &&
                        d.DocumentStatus == DocumentStatus.Closed)
                    .Select(
                        d =>
                            d.Descriptions.Where(
                                desc =>
                                    desc.DescriptionType.Code == DescTypeCodes.PaymentType &&
                                    desc.DescriptionValue.Code == DescValCodes.Cash).Sum(desc => desc.MinValue))
                    .Sum()
                    .GetValueOrDefault();

                return w - q;
            }
            return 0;

        }

        public List<ShiftDto> GetAllShifts(DateTime? startPeriod, DateTime? endPeriod)
        {
            return DaoContainer.GetDao<DocumentDao>()
                .Linq()
                .Where(d => d.DocumentType.Code == DocumentTypeCodes.Shift)
                .OrderByDescending(d => d.Id)
                .Select(d => DtoTranslator.GetTranslator<ShiftDto>().ToDto(d) as ShiftDto)
                .ToList();
        }

        #endregion

        #region Отложенные

        public void PostponeChequeById(long id)
        {
            DaoContainer.GetDao<DocumentDao>().GetById(id, false).DocumentStatus = DocumentStatus.Delayed;
            DaoContainer.CommitChanges();
        }

        public List<ChequeDto> GetPostponedByCurrentShift()
        {
            CheckShift();

            return
                CurrentShift.DerivedDocuments.Where(d => d.DocumentStatus == DocumentStatus.Delayed)
                    .Select(d => DtoTranslator.GetTranslator<ChequeDto>().ToDto(d) as ChequeDto)
                    .ToList();
        }

        #endregion

        #region Возврат

        public ChequeDto CreateReturnCheque(long sourceChequeId)
        {
            CheckShift();

           var sourceCheque =  DaoContainer.GetDao<DocumentDao>().GetById(sourceChequeId,false);
            var returnCheque =
                DaoContainer.GetDao<DocumentDao>()
                    .Save(new Document()
                    {
                        DocumentStatus = DocumentStatus.Created,
                        DocumentType =
                            DaoContainer.Dao<DocumentType>().Linq().First(x => x.Code == DocumentTypeCodes.Return),
                        No =
                            (long)
                                DaoContainer.Dao<Generator>()
                                    .Linq()
                                    .First(g => g.Code == GeneratorsCodes.ChequeGenerator)
                                    .NextValue
                    });

            sourceCheque.AddDocumentRelation(DocumentRelationType.Derived, returnCheque);
            CurrentShift.AddDocumentRelation(DocumentRelationType.Derived, returnCheque);

            DaoContainer.CommitChanges();

            return DtoTranslator.GetTranslator<ChequeDto>().ToDto(returnCheque) as ChequeDto;
        }

        public List<ChequeDto> GetReadyForReturn(long shiftId)
        {
            var shift = DaoContainer.GetDao<DocumentDao>().GetById(shiftId, false);

            var shiftDocs = shift.DerivedDocuments.Where(d => d.DocumentStatus == DocumentStatus.Closed && d.DocumentType.Code != DocumentTypeCodes.Return).ToList();
            /*.Select(d => DtoTranslator.GetTranslator<ChequeDto>().ToDto(d) as ChequeDto)
                    .ToList();*/
            var returnedMaterialsList =
                shiftDocs.SelectMany(
                    d =>
                        d.DerivedDocuments.Where(
                            dd =>
                                dd.DocumentType.Code == DocumentTypeCodes.Return &&
                                dd.DocumentStatus == DocumentStatus.Closed).Select(
                                    dd => new {Key = d.Id, Materials = dd.Lines.Select(dl => dl.Material==null ? 0 : dl.Material.Id)})).ToList();

           var shiftDocsDtos = shiftDocs.Select(d => DtoTranslator.GetTranslator<ChequeDto>().ToDto(d) as ChequeDto).ToList();

           foreach (var returnedMaterials in returnedMaterialsList)
           {
               shiftDocsDtos.First(d => d.Id == returnedMaterials.Key)
                   .Lines.RemoveAll(line => returnedMaterials.Materials.Contains(line.MaterialId));
           }

            return shiftDocsDtos.Where(d => d.Lines.Any()).ToList();

        }

        #endregion

        #region Касса

        public void MakeIncome(decimal summ)
        {
            CheckShift();

            var incomeDoc = new Document()
            {
                DocumentType = DaoContainer.Dao<DocumentType>().Linq()
                    .FirstOrDefault(dt => dt.Code == DocumentTypeCodes.MakeMoney)
            };

            incomeDoc.SetDescription(DescTypeCodes.SumByClient, summ);
            incomeDoc.SetDescription(DescTypeCodes.PaymentType, null, DescValCodes.Cash, null, null, summ);
            
            incomeDoc.DocumentStatus = DocumentStatus.Closed;

            CurrentShift.AddDocumentRelation(DocumentRelationType.Derived, incomeDoc);

            DaoContainer.GetDao<DocumentDao>().Save(incomeDoc);
            DaoContainer.CommitChanges();
        }

        public void MakeOutcome(decimal summ)
        {
            CheckShift();

            if (CurrentShiftCashSum() < summ)
                throw new FaultException(@"Сумма превышает остаток в кассе!");

               var outcomeDoc = new Document()
            {
                DocumentType = DaoContainer.Dao<DocumentType>().Linq()
                    .FirstOrDefault(dt => dt.Code == DocumentTypeCodes.IssuanceMoney)
            };

            outcomeDoc.SetDescription(DescTypeCodes.SumByClient, summ);
            outcomeDoc.SetDescription(DescTypeCodes.PaymentType, null, DescValCodes.Cash, null, null, summ);

            outcomeDoc.DocumentStatus = DocumentStatus.Closed;

            CurrentShift.AddDocumentRelation(DocumentRelationType.Derived, outcomeDoc);

            DaoContainer.GetDao<DocumentDao>().Save(outcomeDoc);
            DaoContainer.CommitChanges();
        }

        #endregion

    }
}
