﻿
namespace Cashier.Equipment.Interface
{
    public delegate void BarCodeDelegate(string barCode);

    public interface IBarCodeScanner
    {
        event BarCodeDelegate BarCodeReaded;
    }
}
