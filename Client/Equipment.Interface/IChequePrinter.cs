﻿using TB.Cashier.Server.Services.Dto;

namespace Cashier.Equipment.Interface
{
    public interface IChequePrinter
    {
        bool PrintXReport();
        bool PrintZReport();
        bool CloseCheque(ChequeDto cheque,bool isReturn);
        bool OpenShift(ShiftDto shift);
        bool CloseShift(ShiftDto shift);

        void CancelCheque();

        bool CashIncome(decimal summ);
        bool CashOutcome(decimal summ);

        string LastError();
    }
}
