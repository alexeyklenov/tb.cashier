﻿using Cashier.Equipment.Interface;

namespace Cashier.Equipment.BarcodeScanners
{
    public class ComScanner: IBarCodeScanner
    {
        public event BarCodeDelegate BarCodeReaded;

        private SerialPortManager _serialManager;
        public ComScanner(string port)
        {
            _serialManager = new SerialPortManager(port);
            _serialManager.DataRecieved += _serialManager_DataRecieved;
        }

        void _serialManager_DataRecieved(object sender, SerialDataEventArgs e)
        {
            if (BarCodeReaded != null)
                BarCodeReaded(e.Data);
        }
    }
}
