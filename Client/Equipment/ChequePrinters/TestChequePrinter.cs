﻿using Cashier.Equipment.Interface;
using TB.Cashier.Server.Services.Dto;

namespace Cashier.Equipment.ChequePrinters
{
    public class TestChequePrinter : IChequePrinter
    {
        public bool PrintXReport()
        {
            return true;
        }

        public bool PrintZReport()
        {
            return true;
        }

        public bool CloseCheque(ChequeDto cheque, bool isReturn)
        {
            return true;
        }

        public bool OpenShift(ShiftDto shift)
        {
            return true;
        }

        public bool CloseShift(ShiftDto shift)
        {
            return true;
        }

        public void CancelCheque()
        {
        }

        public bool CashIncome(decimal summ)
        {
            return true;
        }

        public bool CashOutcome(decimal summ)
        {
            return true;
        }

        public string LastError()
        {
            return "TEST";
        }
    }
}
