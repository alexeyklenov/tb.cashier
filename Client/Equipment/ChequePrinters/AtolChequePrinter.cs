﻿using System;
using System.Linq;
using Cashier.Equipment.Interface;
using FprnM1C;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces.Const;

namespace Cashier.Equipment.ChequePrinters
{
    public enum Modes
    {
        Report
    }

    public class AtolChequePrinter: IChequePrinter
    {
        private readonly IFprnM45 _ecr;

        public AtolChequePrinter()
        {
           /* try
            {*/
                _ecr = new FprnM45Class();
           /* }*/
            /*catch (Exception)
            {
            }*/

            _ecr.DeviceEnabled = true;
        }

        private bool PrintReport(int reportType, int mode)
        {
            if (!_ecr.DeviceEnabled)
                return false;

            _ecr.Mode = mode;
            _ecr.SetMode();

            _ecr.ReportType = reportType;
            return _ecr.Report() == 0;
        }

        public bool PrintXReport()
        {
            return PrintReport(2,2);
        }

        public bool PrintZReport()
        {
            return PrintReport(1,3);
        }

        public bool CloseShift(ShiftDto shift)
        {
            return PrintZReport();
        }

        public bool OpenShift(ShiftDto shift)
        {
            if (_ecr.SessionOpened)
                return false;

            _ecr.Mode = 1;
            if (_ecr.SetMode() != 0)
                return false;

            _ecr.Caption = shift.ShiftDateShort;

            return _ecr.OpenSession() == 0;
        }

        public bool CloseCheque(ChequeDto cheque, bool isReturn)
        {
            if (!_ecr.DeviceEnabled)
                return false;

            _ecr.Mode = 1;
            if (_ecr.SetMode() != 0)
                return false;

            _ecr.CheckType = isReturn ? 2 : 1;
            _ecr.OpenCheck();


            if (cheque.Lines.Any(line => !Register(line, isReturn)))
            {
                return false;
            }

            foreach (var payment in cheque.PaymentTypeSum)
            {
                _ecr.Summ = Convert.ToDouble(payment.Value);
                _ecr.TypeClose = payment.Key == DescValCodes.Cash ? 0 : payment.Key == DescValCodes.Card ? 1 : 2;
                _ecr.Payment();
            }

            return _ecr.CloseCheck() == 0;
        }

        public void CancelCheque()
        {
            _ecr.ResetChargeDiscount();
            _ecr.CancelCheck();
        }

      
        private bool Register(ChequeLineDto line, bool isReturn)
        {
            _ecr.Name = line.MaterialName;
            _ecr.Quantity = line.Quantity;
            _ecr.Price = Convert.ToDouble(line.Price);
            var regResult = (isReturn ? _ecr.Return() : _ecr.Registration()) == 0;

            if (regResult && line.Discount > 0)
            {
                _ecr.Summ = Convert.ToDouble(line.Discount);
                _ecr.Destination = 1;
                _ecr.DiscountTypeNumber = 0;
                regResult = _ecr.SummDiscount() == 0;
            }

            return regResult;
        }

        public bool CashIncome(decimal summ)
        {
            if (!_ecr.DeviceEnabled)
                return false;

            _ecr.Mode = 1;
            if (_ecr.SetMode() != 0)
                return false;

            _ecr.Summ = Convert.ToDouble(summ);

            return _ecr.CashIncome() == 0;
        }

        public bool CashOutcome(decimal summ)
        {
            if (!_ecr.DeviceEnabled)
                return false;

            _ecr.Mode = 1;
            if (_ecr.SetMode() != 0)
                return false;

            _ecr.Summ = Convert.ToDouble(summ);

            return _ecr.CashOutcome() == 0;
        }

        public string LastError()
        {
            return _ecr.ResultDescription;
        }
    }
}
