﻿using System;
using System.IO;
using System.IO.Ports;
using System.Text;

namespace Cashier.Equipment
{
    /// <summary>
    /// Manager for serial port data
    /// </summary>
    public class SerialPortManager : IDisposable
    {
        private SerialPort _serialPort;
        private readonly string _portName;
        private SerialDataEventArgs _readDataArgs = new SerialDataEventArgs();
        private object _lockObj = new Object();

        public event EventHandler<SerialDataEventArgs> DataRecieved;

        public SerialPortManager(string portName)
        {
            foreach (var port in SerialPort.GetPortNames())
            {
                if (port == portName)
                {
                    _portName = port;
                    StartListening();
                }
            }

        }

        #region Event handlers

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            lock (_lockObj)
            {
                var dataLength = _serialPort.BytesToRead;
                var data = new byte[dataLength];
                var nbrDataRead = _serialPort.Read(data, 0, dataLength);
                if (nbrDataRead != 0)
                {
                    _readDataArgs.AddData(data);

                }
                if (_readDataArgs.DataIsLine)
                {
                    if (DataRecieved != null)
                        DataRecieved(this, _readDataArgs);
                    _readDataArgs = new SerialDataEventArgs();
                }
            }
        }

        void _serialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            StopListening();
            StartListening();
        }

        #endregion

        #region Methods

        public void StartListening()
        {

            if (_serialPort != null && _serialPort.IsOpen)
                _serialPort.Close();
            try
            {
                _serialPort = new SerialPort(_portName, 9600, Parity.None, 8, StopBits.One);

                _serialPort.DataReceived += _serialPort_DataReceived;
                _serialPort.ErrorReceived += _serialPort_ErrorReceived;
                _serialPort.Open();
            }
            catch (Exception e)
            {
            }
        }


        /// <summary>
        /// Closes the serial port
        /// </summary>
        public void StopListening()
        {
            _serialPort.Close();
        }


        public void Dispose()
        {
            _serialPort.DataReceived -= _serialPort_DataReceived;

            if (_serialPort == null)
                return;

            if (_serialPort.IsOpen)
                StopListening();

            _serialPort.Dispose();
        }

        #endregion

    }

    public class SerialDataEventArgs : EventArgs
    {
        public SerialDataEventArgs()
        {
            Data = "";
        }
        public SerialDataEventArgs(byte[] dataInByteArray)
        {
            Data = Encoding.Default.GetString(dataInByteArray);
        }

        public void AddData(byte[] dataInByteArray)
        {
            Data += Encoding.Default.GetString(dataInByteArray);
        }

        public bool DataIsLine
        {
            get { return Data.Contains(Environment.NewLine); }
        }

        public string Data;
    }
}
