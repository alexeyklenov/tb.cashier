﻿
using System.Configuration;

namespace TB.Cashier.Client.Shell
{
    public static class Config
    {

        static Config()
        {
            ScannerComPort = ConfigurationManager.AppSettings["ScannerComPort"];
            CurrentWarehouse = ConfigurationManager.AppSettings["CurrentWarehouse"];
        }

        public static string ScannerComPort { get; set; }

        public static string CurrentWarehouse { get; set; }
    }
}
