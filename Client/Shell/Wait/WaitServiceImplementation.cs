﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Practices.ObjectBuilder2;
using Timer = System.Threading.Timer;

namespace TB.Cashier.Client.Shell.Wait
{
    public class WaitServiceImplementation
    {
        readonly Object _lock = new Object();

        WaitDialogForm _waitDialog;

        Timer _timer;

        readonly List<BackgroundWorker> _backWork;

        public bool IsRunAsync { get; private set; }

        private Func<Boolean> _isCancellationPending;
        public Boolean IsCancellationPending()
        {
            Boolean res = false;
            if (_isCancellationPending != null)
                res = _isCancellationPending();
            return res;
        }

        private void CloseForms(object state)
        {
            if (_waitThread == null && Application.OpenForms["WaitDialogForm"] != null)
            {
                try
                {
                    _waitDialog.Invoke(new MethodInvoker(_waitDialog.Close));
                   /* Application.OpenForms["WaitDialogForm"].Invoke(
                    new MethodInvoker(Application.OpenForms["WaitDialogForm"].Close));*/
                }
                catch (Exception)
                {
                }
                
            }
        }

        public WaitServiceImplementation()
        {
            _backWork = new List<BackgroundWorker>();
            _timer = new Timer(CloseForms, null, 5, 1000);
        }
        /// <summary>
        /// Factory-метод, возвращает первого незанятого воркера или создает нового, если таких нет
        /// </summary>
        /// <returns></returns>
        private BackgroundWorker GetBackgroundWorker()
        {
            var backwrk = _backWork.FirstOrDefault(b => b.IsBusy == false);
            if (backwrk == default(BackgroundWorker))
            {
                backwrk = new BackgroundWorker {WorkerSupportsCancellation = true, WorkerReportsProgress = true};
                backwrk.DoWork += (sender, e) =>
                {
                    if (DoWork != null)
                        DoWork(sender, e);
                    e.Cancel = backwrk.CancellationPending;
                    // e.Result 
                };
                backwrk.RunWorkerCompleted += (sender, e) =>
                {
                    InternalStopProgress();
                    if (WaitCompleted != null)
                        WaitCompleted(sender, e);
                    if (_backWork.All(x => !x.IsBusy))
                    {
                        DoWork = null;
                        WaitCompleted = null;
                    }

                };
                backwrk.ProgressChanged += (sender, e) =>
                {
                    InternalStepProgress(e.ProgressPercentage);
                    if (ProgressChanged != null)
                        ProgressChanged(sender, e);
                };

                StopWork = (o, e) => backwrk.CancelAsync();
                _isCancellationPending = () => backwrk.CancellationPending;
                _backWork.Add(backwrk);

            }
            return backwrk;
        }

        #region IWaitService Members

        public event DoWorkEventHandler DoWork;
        public event RunWorkerCompletedEventHandler WaitCompleted;
        public event EventHandler StopWork;
        public ProgressChangedEventHandler ProgressChanged { get; set; }
        private Thread _waitThread;
        private delegate void InvokeStringDelegate(string param);

        private string _caption;
        private string _title;
        private Size? _size;
        private Form _parent;

        private void ShowThread()
        {
            _waitDialog = new WaitDialogForm(_caption, _title, _size, _parent);
            //_waitDialog.Show();
            Application.Run(_waitDialog);

        }
        /// <summary>
        /// Открыть окно ожидания с текстом
        /// </summary>
        /// <param name="caption">текст</param>
        public void OpenWaitDialog(string caption)
        {
            lock (_lock)
            {
                try
                {
                    _caption = caption;
                    _title = "Загрузка";

                    if (_waitThread != null) return;
                    _waitThread = new Thread(ShowThread) {IsBackground = true};
                    _waitThread.Start();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"OpenWaitDialog1", @"Ошибка");
                }
            }
        }

        /// <summary>
        /// Открыть окно ожидания с текстом
        /// </summary>
        /// <param name="caption">текст</param>
        /// <param name="title">заголовок</param>
        public void OpenWaitDialog(string caption, string title)
        {
            lock (_lock)
            {
                try
                {
                    _caption = caption;
                    _title = title;

                    if (_waitThread != null) return;
                    _waitThread = new Thread(ShowThread) {IsBackground = true};
                    _waitThread.Start();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"OpenWaitDialog2", @"Ошибка");
                }
            }
        }

        /// <summary>
        /// Открыть окно ожидания с текстом
        /// </summary>
        /// <param name="caption">текст</param>
        /// <param name="title">заголовок</param>
        /// <param name="size">размер окна</param>
        /// <param name="parent">родительское окно</param>
        public void OpenWaitDialog(string caption, string title, Size size, Form parent)
        {
            lock (_lock)
            {
                try
                {
                    _caption = caption;
                    _title = title;
                    _size = size;
                    _parent = parent;

                    if (_waitThread != null) return;
                    _waitThread = new Thread(ShowThread) {IsBackground = true};
                    _waitThread.Start();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"OpenWaitDialog3", @"Ошибка");
                }
            }
        }

        /// <summary>
        /// Сменить текст в окне
        /// </summary>
        /// <param name="newCaption">новый текст</param>
        public void SetCaption(string newCaption)
        {
            lock (_lock)
            {
                try
                {
                    _waitDialog.Invoke(new InvokeStringDelegate(_waitDialog.SetCaption), new object[] {newCaption});
                    Application.DoEvents();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"SetCaption", @"Ошибка");
                }

            }
        }

        public void Hide()
        {
            try
            {
                if (_waitDialog != null)
                    _waitDialog.Invoke(new MethodInvoker(_waitDialog.Hide));
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Закрыть окно
        /// </summary>
        public void Close()
        {
            lock (_lock)
            {
               // _waitDialog.Close();
                if (_waitThread == null) return;
                /*if (_waitDialog == null)
                {
                    if (_waitThread == null) 
                        return;
                    try
                    {
                        _waitThread.Abort();
                        _waitThread = null;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"before Close", @"Ошибка");
                    }
                    return;
                }*/

                try
                {
                    _waitDialog.Invoke(new MethodInvoker(_waitDialog.Close));
                      //_waitThread.Abort();
                }
                catch (Exception)
                {
                    try
                    {
                        if (_waitDialog != null)
                            _waitDialog.Invoke(new MethodInvoker(_waitDialog.Dispose));
                    }
                    catch(Exception)
                    {                        
                    }
                    // log.Error(ex.Message, ex);
                }
                _waitThread = null;


                // if (Form.ActiveForm != null) Form.ActiveForm.Activate();
            }
        }

        public void StartIncrementalProgressAsync(int min, int max)
        {
            lock (_lock)
            {
                var backwrk = GetBackgroundWorker();
                backwrk.WorkerReportsProgress = true;
                //TODO Доделать индикацию
                /*if (BarManager != null && BarManager.Items["_progressLoad"] != null)
                {

                    var baritem = (BarEditItem)BarManager.Items["_progressLoad"];
                    baritem.Visibility = BarItemVisibility.Always;
                    var edit = (DevExpress.XtraEditors.Repository.RepositoryItemProgressBar)BarManager.RepositoryItems["repositoryItemProgressBar"];
                    edit.Minimum = min;
                    edit.Maximum = max;
                    baritem.Edit = edit;
                    baritem.EditValue = min;
                }*/
                IsRunAsync = true;
                backwrk.RunWorkerAsync();
            }
        }

        public void StartIncrementalProgress(int min, int max)
        {
            lock (_lock)
            {
                //TODO Доделать индикацию
                /*
                if (BarManager != null && BarManager.Items["_progressLoad"] != null)
                {
                    var baritem = (BarEditItem)BarManager.Items["_progressLoad"];
                    baritem.Visibility = BarItemVisibility.Always;
                    var edit = (DevExpress.XtraEditors.Repository.RepositoryItemProgressBar)BarManager.RepositoryItems["repositoryItemProgressBar"];
                    edit.Minimum = min;
                    edit.Maximum = max;
                    baritem.Edit = edit;
                    baritem.EditValue = min;
                }
                 */
            }
        }

        public void StartMarqueeProgress()
        {
            lock (_lock)
            {
               /* if (BarManager != null && BarManager.Items["_progressLoad"] != null)
                {
                    var baritem = (BarEditItem)BarManager.Items["_progressLoad"];
                    baritem.Visibility = BarItemVisibility.Always;
                    var edit = (DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar)BarManager.RepositoryItems["repositoryItemMarqueeProgressBar"];
                    baritem.Edit = edit;
                }*/
            }
        }

        public void StartMarqueeProgressAsync()
        {
            lock (_lock)
            {
                /*if (BarManager != null && BarManager.Items["_progressLoad"] != null)
                {
                    var baritem = (BarEditItem)BarManager.Items["_progressLoad"];
                    baritem.Visibility = BarItemVisibility.Always;
                    var edit = (DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar)BarManager.RepositoryItems["repositoryItemMarqueeProgressBar"];
                    baritem.Edit = edit;

                }*/
                IsRunAsync = true;
                var backwrk = GetBackgroundWorker();
                //__backwrk.WorkerReportsProgress = false;
                backwrk.RunWorkerAsync();
            }
        }

        public void StepProgress(int procent)
        {
            lock (_lock)
            {
                if (IsRunAsync)
                {
                    var backwrk = _backWork.FirstOrDefault(b => b.IsBusy);
                    backwrk.ReportProgress(procent);
                }
                else
                {
                    InternalStepProgress(procent);
                }
            }
        }

        void InternalStepProgress(int procent)
        {
            lock (_lock)
            {
               /* if (BarManager != null && BarManager.Items["_progressLoad"] != null)
                {
                    var baritem = (BarEditItem)BarManager.Items["_progressLoad"];
                    baritem.EditValue = procent;
                }*/
                Application.DoEvents();
            }
        }

        public void StopProgress()
        {
            if (!IsRunAsync)
            {
                InternalStopProgress();
            }
            else
            {
                if (StopWork != null)
                    StopWork(this, new EventArgs());
                /*

                foreach(var __backwrk in _backWork.Where(b => b.IsBusy == true))
                {
                  __backwrk.CancelAsync();
                }*/
            }
        }

        public void ReportProgress(int procent, object userState)
        {
            lock (_lock)
            {
                if (IsRunAsync)
                {
                    var backwrk = _backWork.FirstOrDefault(b => b.IsBusy);
                    backwrk.ReportProgress(procent, userState);
                }
                else
                {
                    InternalStepProgress(procent);
                }
            }
        }

        void InternalStopProgress()
        {
            lock (_lock)
            {
                /*if (BarManager != null && BarManager.Items["_progressLoad"] != null)
                {
                    var baritem = (BarEditItem)BarManager.Items["_progressLoad"];
                    baritem.Visibility = BarItemVisibility.Never;

                }*/
                IsRunAsync = false;
            }
        }

        #endregion
    }

    public static class WaitService
    {
        /// <summary>
        /// Открыть окно ожидания с текстом
        /// </summary>
        /// <param name="text">текст</param>
        public static WaitDialog OpenWaitDialog(string text)
        {
            return new WaitDialog(text);
        }

        /// <summary>
        /// Открыть окно ожидания с текстом
        /// </summary>
        /// <param name="text">текст</param>
        /// <param name="title">заголовок</param>
        public static WaitDialog OpenWaitDialog(string text, string title)
        {
            return new WaitDialog(text, title);
        }

        /// <summary>
        /// Открыть окно ожидания с текстом
        /// </summary>
        /// <param name="text">текст</param>
        /// <param name="title">заголовок</param>
        /// <param name="size">размер окна</param>
        /// <param name="parent">родительское окно</param>
        public static WaitDialog OpenWaitDialog(string text, string title, System.Drawing.Size size, Form parent)
        {
            return new WaitDialog(text, title, size, parent);
        }

        public static void HideWaitDialog()
        {
            try
            {
                if (Application.OpenForms["WaitDialogForm"] != null)
                    Application.OpenForms.Cast<Form>()
                        .ForEach(delegate(Form form)
                        {
                            if (form.GetType() == typeof (WaitDialogForm)) form.Invoke(new MethodInvoker(form.Hide));
                        }); 
            }
            catch (Exception)
            {
            }
        }
    }

    public class WaitDialog : IDisposable
    {
        readonly WaitServiceImplementation _waitService;

        public WaitDialog()
        {
            _waitService = new WaitServiceImplementation();
        }

        public WaitDialog(string text)
            : this()
        {
            _waitService.OpenWaitDialog(text);
        }

        public WaitDialog(string text, string title)
            : this()
        {
            _waitService.OpenWaitDialog(text, title);
        }

        public WaitDialog(string text, string title, Size size, Form parent)
            : this()
        {
            _waitService.OpenWaitDialog(text, title, size, parent);
        }

        public void Hide()
        {
            _waitService.Hide();
        }

        public void Dispose()
        {
            _waitService.Close();
        }
    }


}
