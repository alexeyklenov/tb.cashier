﻿using System.Drawing;
using System.Windows.Forms;

namespace TB.Cashier.Client.Shell.Wait
{
    public partial class WaitDialogForm : Form
    {

        

        private PictureBox _pic;
        private string _caption = "";
        private string _title = "";

        private static readonly Size DefSize = new Size(210, 60);

        public WaitDialogForm() : this("") { }
        public WaitDialogForm(string caption) : this(caption, "") { }
        public WaitDialogForm(string caption, string title) : this(caption, title, DefSize, ActiveForm) { }
        public WaitDialogForm(string caption, Size? size) : this(caption, "", size, ActiveForm) { }
        public WaitDialogForm(string caption, string title, Size? size) : this(caption, title, size, ActiveForm) { }
        public WaitDialogForm(string caption, string title, Size? size, Form parent)
        {
            InitializeComponent();
            _caption = caption;
            _title = title == "" ? "Загрузка данных. Подождите." : title;
            _pic = new PictureBox();
            FormBorderStyle = FormBorderStyle.None;
            ControlBox = false;
            ClientSize = size ?? DefSize;

            if ((parent ?? (parent = ActiveForm)) == null)
                StartPosition = FormStartPosition.CenterScreen;
            else
            {
                StartPosition = FormStartPosition.Manual;
                Left = parent.Left + (parent.Width - Width)/2;
                Top = parent.Top + (parent.Height - Height)/2;
                //TopMost = true;
            }
            ShowInTaskbar = false;
            
            TopLevel = true;
            Paint += WaitDialogPaint;
            _pic.Size = new Size(30, 30);
            _pic.SizeMode = PictureBoxSizeMode.StretchImage;
            _pic.Location = new Point(8, ClientSize.Height / 2 - 15);
            _pic.Image = Properties.Resources.hourglass;
            Controls.Add(_pic);
            //Show(parent);
            Refresh();
        }

        public string GetCaption()
        {
            return Caption;
        }

        public void SetCaption(string newCaption)
        {
            Caption = newCaption;
        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value;
                Refresh();
            }
        }

        readonly Font _boldFont = new Font("Segoe UI", 11f, FontStyle.Bold);
        readonly Font _font = new Font("Segoe UI", 11f);

        private void WaitDialogPaint(object sender, PaintEventArgs e)
        {
            var r = e.ClipRectangle;
            using (var sf = new StringFormat())
            {
                Brush brush = new SolidBrush(SystemColors.WindowText);
                sf.Alignment = sf.LineAlignment = StringAlignment.Center;
                sf.Trimming = StringTrimming.EllipsisCharacter;
                ControlPaint.DrawBorder3D(e.Graphics, r, Border3DStyle.RaisedInner);
                
                r.X += 30; r.Width -= 30;
                r.Height /= 3;
                r.Y += r.Height / 2;
                e.Graphics.DrawString(_title, _boldFont, brush, r, sf);
                r.Y += r.Height;
                e.Graphics.DrawString(_caption, _font, brush, r, sf);
                brush.Dispose();
            }
        }

    }
}
