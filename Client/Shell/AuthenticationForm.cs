﻿using System;
using System.Windows.Forms;
using TB.Services.Common.Adm.Interface;
using TB.Services.Infrastructure.Client;
using TB.Services.Infrastructure.Interface;

namespace TB.Cashier.Client.Shell
{
    public partial class AuthenticationForm : Form
    {
        public AuthenticationForm()
        {
            InitializeComponent();
            WindowState = FormWindowState.Normal;
           
        }

        private void AuthenticationForm_Load(object sender, EventArgs e)
        {
            using (var proxy = new CabClientProxy<IAuthenticationService>())
            {
                UserCb.Items.AddRange(proxy.Get(x => x.GetAllUsers()).ToArray());
            }
            if (UserCb.Items.Count > 0)
            {
                UserCb.SelectedIndex = 0;
                PwdTb.Select();
            }
        }

        private void OkBt_Click(object sender, EventArgs e)
        {
            using (var proxy = new CabClientProxy<IAuthenticationService>())
            {
                UserContext.ClientInstance = proxy.Get(x => x.Authenticate((string)UserCb.SelectedItem, PwdTb.Text));
                if (!UserContext.ClientInstance.IsAuthenticated)
                {
                    ErrorLb.Text = @"Неверный пароль!";
                    return;
                }

                //AppDomain.CurrentDomain.SetData("UserContext", UserContext.ClientInstance);
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void PwdTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            //enter pressed
            if (e.KeyChar == (char)13)
                OkBt_Click(OkBt, new EventArgs());
        }
    }
}
