﻿using TB.Cashier.Client.Shell.Controls;

namespace TB.Cashier.Client.Shell
{
    partial class MainFormV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFormV2));
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.BtnPanel = new System.Windows.Forms.Panel();
            this.DiscountPanel = new System.Windows.Forms.Panel();
            this.SummaryPanel = new System.Windows.Forms.Panel();
            this.SumLb = new System.Windows.Forms.Label();
            this.chequeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SummaryTextLb = new System.Windows.Forms.Label();
            this.CodePanel = new System.Windows.Forms.Panel();
            this.NameTb = new System.Windows.Forms.TextBox();
            this.CodeLb = new System.Windows.Forms.Label();
            this.CodeTb = new System.Windows.Forms.TextBox();
            this.CurrentPanel = new System.Windows.Forms.Panel();
            this.CurrentPosLb = new System.Windows.Forms.Label();
            this.GridPanel = new System.Windows.Forms.Panel();
            this.ChequeLineGrid = new System.Windows.Forms.DataGridView();
            this.materialNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.InfoPanel = new System.Windows.Forms.Panel();
            this.InfoCenterPanel = new System.Windows.Forms.Panel();
            this.ModeTextLb = new System.Windows.Forms.Label();
            this.InfoRightPanel = new System.Windows.Forms.Panel();
            this.ShiftTextLb = new System.Windows.Forms.Label();
            this.ShiftLb = new System.Windows.Forms.Label();
            this.shiftBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.InfoLeftPanel = new System.Windows.Forms.Panel();
            this.ChequeNoTextLb = new System.Windows.Forms.Label();
            this.ChequeNoLb = new System.Windows.Forms.Label();
            this.KeyboardCtrl = new TB.Cashier.Client.Shell.Controls.KeyboardControl();
            this.FinishChequeBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.FreeSellBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.ServiceBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.DeleteCurrentPosBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.ReturnBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.PostponeBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.ClearChequeBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.ClearDiscountBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.MinusQuantityBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.PlusQuantityBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.SetAmountBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.SetSumBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.DiscountPercentBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.DiscountSumBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.NameBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.LeftPanel.SuspendLayout();
            this.BtnPanel.SuspendLayout();
            this.DiscountPanel.SuspendLayout();
            this.SummaryPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chequeBindingSource)).BeginInit();
            this.CodePanel.SuspendLayout();
            this.CurrentPanel.SuspendLayout();
            this.GridPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChequeLineGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource)).BeginInit();
            this.InfoPanel.SuspendLayout();
            this.InfoCenterPanel.SuspendLayout();
            this.InfoRightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shiftBindingSource)).BeginInit();
            this.InfoLeftPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LeftPanel
            // 
            this.LeftPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LeftPanel.Controls.Add(this.KeyboardCtrl);
            this.LeftPanel.Controls.Add(this.BtnPanel);
            this.LeftPanel.Controls.Add(this.DiscountPanel);
            this.LeftPanel.Controls.Add(this.SummaryPanel);
            this.LeftPanel.Controls.Add(this.CodePanel);
            this.LeftPanel.Controls.Add(this.CurrentPanel);
            this.LeftPanel.Controls.Add(this.GridPanel);
            this.LeftPanel.Controls.Add(this.InfoPanel);
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(1443, 1045);
            this.LeftPanel.TabIndex = 0;
            // 
            // BtnPanel
            // 
            this.BtnPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BtnPanel.Controls.Add(this.FinishChequeBt);
            this.BtnPanel.Controls.Add(this.FreeSellBt);
            this.BtnPanel.Controls.Add(this.ServiceBt);
            this.BtnPanel.Controls.Add(this.DeleteCurrentPosBt);
            this.BtnPanel.Controls.Add(this.ReturnBt);
            this.BtnPanel.Controls.Add(this.PostponeBt);
            this.BtnPanel.Controls.Add(this.ClearChequeBt);
            this.BtnPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnPanel.Location = new System.Drawing.Point(0, 547);
            this.BtnPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.BtnPanel.Name = "BtnPanel";
            this.BtnPanel.Size = new System.Drawing.Size(1439, 120);
            this.BtnPanel.TabIndex = 5;
            // 
            // DiscountPanel
            // 
            this.DiscountPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DiscountPanel.Controls.Add(this.ClearDiscountBt);
            this.DiscountPanel.Controls.Add(this.MinusQuantityBt);
            this.DiscountPanel.Controls.Add(this.PlusQuantityBt);
            this.DiscountPanel.Controls.Add(this.SetAmountBt);
            this.DiscountPanel.Controls.Add(this.SetSumBt);
            this.DiscountPanel.Controls.Add(this.DiscountPercentBt);
            this.DiscountPanel.Controls.Add(this.DiscountSumBt);
            this.DiscountPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DiscountPanel.Location = new System.Drawing.Point(0, 427);
            this.DiscountPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.DiscountPanel.Name = "DiscountPanel";
            this.DiscountPanel.Size = new System.Drawing.Size(1439, 120);
            this.DiscountPanel.TabIndex = 6;
            // 
            // SummaryPanel
            // 
            this.SummaryPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SummaryPanel.Controls.Add(this.SumLb);
            this.SummaryPanel.Controls.Add(this.SummaryTextLb);
            this.SummaryPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.SummaryPanel.Font = new System.Drawing.Font("Segoe UI", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SummaryPanel.ForeColor = System.Drawing.Color.Blue;
            this.SummaryPanel.Location = new System.Drawing.Point(0, 349);
            this.SummaryPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.SummaryPanel.Name = "SummaryPanel";
            this.SummaryPanel.Size = new System.Drawing.Size(1439, 78);
            this.SummaryPanel.TabIndex = 4;
            // 
            // SumLb
            // 
            this.SumLb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.chequeBindingSource, "Sum", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "C2"));
            this.SumLb.Dock = System.Windows.Forms.DockStyle.Right;
            this.SumLb.Location = new System.Drawing.Point(991, 0);
            this.SumLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SumLb.Name = "SumLb";
            this.SumLb.Size = new System.Drawing.Size(444, 74);
            this.SumLb.TabIndex = 1;
            this.SumLb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chequeBindingSource
            // 
            this.chequeBindingSource.DataSource = typeof(TB.Cashier.Server.Services.Dto.ChequeDto);
            // 
            // SummaryTextLb
            // 
            this.SummaryTextLb.Dock = System.Windows.Forms.DockStyle.Left;
            this.SummaryTextLb.Location = new System.Drawing.Point(0, 0);
            this.SummaryTextLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SummaryTextLb.Name = "SummaryTextLb";
            this.SummaryTextLb.Size = new System.Drawing.Size(323, 74);
            this.SummaryTextLb.TabIndex = 0;
            this.SummaryTextLb.Text = "Итог чека:";
            this.SummaryTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CodePanel
            // 
            this.CodePanel.Controls.Add(this.NameBt);
            this.CodePanel.Controls.Add(this.NameTb);
            this.CodePanel.Controls.Add(this.CodeLb);
            this.CodePanel.Controls.Add(this.CodeTb);
            this.CodePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.CodePanel.Location = new System.Drawing.Point(0, 284);
            this.CodePanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.CodePanel.Name = "CodePanel";
            this.CodePanel.Size = new System.Drawing.Size(1439, 65);
            this.CodePanel.TabIndex = 3;
            // 
            // NameTb
            // 
            this.NameTb.Location = new System.Drawing.Point(733, 14);
            this.NameTb.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.NameTb.Name = "NameTb";
            this.NameTb.Size = new System.Drawing.Size(316, 32);
            this.NameTb.TabIndex = 2;
            this.NameTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NameTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NameTb_KeyPress);
            // 
            // CodeLb
            // 
            this.CodeLb.Image = global::TB.Cashier.Client.Shell.Properties.Resources.BarCodeSmall;
            this.CodeLb.Location = new System.Drawing.Point(70, 3);
            this.CodeLb.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.CodeLb.Name = "CodeLb";
            this.CodeLb.Size = new System.Drawing.Size(50, 50);
            this.CodeLb.TabIndex = 1;
            this.CodeLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CodeTb
            // 
            this.CodeTb.Location = new System.Drawing.Point(129, 12);
            this.CodeTb.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.CodeTb.Name = "CodeTb";
            this.CodeTb.Size = new System.Drawing.Size(279, 32);
            this.CodeTb.TabIndex = 0;
            this.CodeTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CurrentPanel
            // 
            this.CurrentPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CurrentPanel.Controls.Add(this.CurrentPosLb);
            this.CurrentPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.CurrentPanel.Location = new System.Drawing.Point(0, 268);
            this.CurrentPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.CurrentPanel.Name = "CurrentPanel";
            this.CurrentPanel.Size = new System.Drawing.Size(1439, 16);
            this.CurrentPanel.TabIndex = 2;
            this.CurrentPanel.Visible = false;
            // 
            // CurrentPosLb
            // 
            this.CurrentPosLb.Dock = System.Windows.Forms.DockStyle.Right;
            this.CurrentPosLb.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.CurrentPosLb.Location = new System.Drawing.Point(1140, 0);
            this.CurrentPosLb.Name = "CurrentPosLb";
            this.CurrentPosLb.Size = new System.Drawing.Size(295, 12);
            this.CurrentPosLb.TabIndex = 0;
            this.CurrentPosLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GridPanel
            // 
            this.GridPanel.Controls.Add(this.ChequeLineGrid);
            this.GridPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.GridPanel.Location = new System.Drawing.Point(0, 73);
            this.GridPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.GridPanel.Name = "GridPanel";
            this.GridPanel.Size = new System.Drawing.Size(1439, 195);
            this.GridPanel.TabIndex = 1;
            // 
            // ChequeLineGrid
            // 
            this.ChequeLineGrid.AllowUserToAddRows = false;
            this.ChequeLineGrid.AllowUserToResizeRows = false;
            this.ChequeLineGrid.AutoGenerateColumns = false;
            this.ChequeLineGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ChequeLineGrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.ChequeLineGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChequeLineGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ChequeLineGrid.ColumnHeadersHeight = 35;
            this.ChequeLineGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ChequeLineGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialNameDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.discountDataGridViewTextBoxColumn,
            this.sumDataGridViewTextBoxColumn});
            this.ChequeLineGrid.DataSource = this.linesBindingSource;
            this.ChequeLineGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChequeLineGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.ChequeLineGrid.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.ChequeLineGrid.Location = new System.Drawing.Point(0, 0);
            this.ChequeLineGrid.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.ChequeLineGrid.Name = "ChequeLineGrid";
            this.ChequeLineGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.ChequeLineGrid.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChequeLineGrid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.ChequeLineGrid.RowTemplate.DividerHeight = 1;
            this.ChequeLineGrid.RowTemplate.Height = 40;
            this.ChequeLineGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ChequeLineGrid.ShowEditingIcon = false;
            this.ChequeLineGrid.Size = new System.Drawing.Size(1439, 195);
            this.ChequeLineGrid.TabIndex = 1;
            this.ChequeLineGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.ChequeLineGrid_CellBeginEdit);
            this.ChequeLineGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ChequeLineGrid_CellEndEdit);
            // 
            // materialNameDataGridViewTextBoxColumn
            // 
            this.materialNameDataGridViewTextBoxColumn.DataPropertyName = "MaterialName";
            this.materialNameDataGridViewTextBoxColumn.FillWeight = 228.4264F;
            this.materialNameDataGridViewTextBoxColumn.HeaderText = "Наименование";
            this.materialNameDataGridViewTextBoxColumn.Name = "materialNameDataGridViewTextBoxColumn";
            this.materialNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            this.quantityDataGridViewTextBoxColumn.FillWeight = 67.89333F;
            this.quantityDataGridViewTextBoxColumn.HeaderText = "Количество";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.priceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.priceDataGridViewTextBoxColumn.FillWeight = 67.89333F;
            this.priceDataGridViewTextBoxColumn.HeaderText = "Цена";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // discountDataGridViewTextBoxColumn
            // 
            this.discountDataGridViewTextBoxColumn.DataPropertyName = "Discount";
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.discountDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.discountDataGridViewTextBoxColumn.FillWeight = 67.89333F;
            this.discountDataGridViewTextBoxColumn.HeaderText = "Скидка";
            this.discountDataGridViewTextBoxColumn.Name = "discountDataGridViewTextBoxColumn";
            // 
            // sumDataGridViewTextBoxColumn
            // 
            this.sumDataGridViewTextBoxColumn.DataPropertyName = "Sum";
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = null;
            this.sumDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.sumDataGridViewTextBoxColumn.FillWeight = 67.89333F;
            this.sumDataGridViewTextBoxColumn.HeaderText = "Сумма";
            this.sumDataGridViewTextBoxColumn.Name = "sumDataGridViewTextBoxColumn";
            this.sumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // linesBindingSource
            // 
            this.linesBindingSource.DataMember = "Lines";
            this.linesBindingSource.DataSource = this.chequeBindingSource;
            this.linesBindingSource.CurrentChanged += new System.EventHandler(this.linesBindingSource_CurrentChanged);
            this.linesBindingSource.PositionChanged += new System.EventHandler(this.linesBindingSource_PositionChanged);
            // 
            // InfoPanel
            // 
            this.InfoPanel.Controls.Add(this.InfoCenterPanel);
            this.InfoPanel.Controls.Add(this.InfoRightPanel);
            this.InfoPanel.Controls.Add(this.InfoLeftPanel);
            this.InfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.InfoPanel.ForeColor = System.Drawing.Color.MidnightBlue;
            this.InfoPanel.Location = new System.Drawing.Point(0, 0);
            this.InfoPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.InfoPanel.Name = "InfoPanel";
            this.InfoPanel.Size = new System.Drawing.Size(1439, 73);
            this.InfoPanel.TabIndex = 0;
            // 
            // InfoCenterPanel
            // 
            this.InfoCenterPanel.Controls.Add(this.ModeTextLb);
            this.InfoCenterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoCenterPanel.Location = new System.Drawing.Point(143, 0);
            this.InfoCenterPanel.Name = "InfoCenterPanel";
            this.InfoCenterPanel.Size = new System.Drawing.Size(822, 73);
            this.InfoCenterPanel.TabIndex = 6;
            // 
            // ModeTextLb
            // 
            this.ModeTextLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.ModeTextLb.ForeColor = System.Drawing.Color.Red;
            this.ModeTextLb.Location = new System.Drawing.Point(0, 0);
            this.ModeTextLb.Name = "ModeTextLb";
            this.ModeTextLb.Size = new System.Drawing.Size(822, 84);
            this.ModeTextLb.TabIndex = 0;
            this.ModeTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // InfoRightPanel
            // 
            this.InfoRightPanel.Controls.Add(this.ShiftTextLb);
            this.InfoRightPanel.Controls.Add(this.ShiftLb);
            this.InfoRightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.InfoRightPanel.Location = new System.Drawing.Point(965, 0);
            this.InfoRightPanel.Name = "InfoRightPanel";
            this.InfoRightPanel.Size = new System.Drawing.Size(474, 73);
            this.InfoRightPanel.TabIndex = 5;
            // 
            // ShiftTextLb
            // 
            this.ShiftTextLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.ShiftTextLb.Location = new System.Drawing.Point(0, 0);
            this.ShiftTextLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ShiftTextLb.Name = "ShiftTextLb";
            this.ShiftTextLb.Size = new System.Drawing.Size(474, 45);
            this.ShiftTextLb.TabIndex = 3;
            this.ShiftTextLb.Text = "Смена:";
            this.ShiftTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ShiftLb
            // 
            this.ShiftLb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shiftBindingSource, "ShiftDateShort", true));
            this.ShiftLb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ShiftLb.Location = new System.Drawing.Point(0, -11);
            this.ShiftLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ShiftLb.Name = "ShiftLb";
            this.ShiftLb.Size = new System.Drawing.Size(474, 84);
            this.ShiftLb.TabIndex = 2;
            this.ShiftLb.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // shiftBindingSource
            // 
            this.shiftBindingSource.DataSource = typeof(TB.Cashier.Server.Services.Dto.ShiftDto);
            // 
            // InfoLeftPanel
            // 
            this.InfoLeftPanel.Controls.Add(this.ChequeNoTextLb);
            this.InfoLeftPanel.Controls.Add(this.ChequeNoLb);
            this.InfoLeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.InfoLeftPanel.Location = new System.Drawing.Point(0, 0);
            this.InfoLeftPanel.Name = "InfoLeftPanel";
            this.InfoLeftPanel.Size = new System.Drawing.Size(143, 73);
            this.InfoLeftPanel.TabIndex = 4;
            // 
            // ChequeNoTextLb
            // 
            this.ChequeNoTextLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.ChequeNoTextLb.Location = new System.Drawing.Point(0, 0);
            this.ChequeNoTextLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ChequeNoTextLb.Name = "ChequeNoTextLb";
            this.ChequeNoTextLb.Size = new System.Drawing.Size(143, 45);
            this.ChequeNoTextLb.TabIndex = 0;
            this.ChequeNoTextLb.Text = "Номер Чека:";
            this.ChequeNoTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ChequeNoLb
            // 
            this.ChequeNoLb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.chequeBindingSource, "No", true));
            this.ChequeNoLb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChequeNoLb.Location = new System.Drawing.Point(0, -29);
            this.ChequeNoLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ChequeNoLb.Name = "ChequeNoLb";
            this.ChequeNoLb.Size = new System.Drawing.Size(143, 102);
            this.ChequeNoLb.TabIndex = 1;
            // 
            // KeyboardCtrl
            // 
            this.KeyboardCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.KeyboardCtrl.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyboardCtrl.Location = new System.Drawing.Point(0, 667);
            this.KeyboardCtrl.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.KeyboardCtrl.Name = "KeyboardCtrl";
            this.KeyboardCtrl.Padding = new System.Windows.Forms.Padding(9, 10, 9, 10);
            this.KeyboardCtrl.Size = new System.Drawing.Size(1439, 358);
            this.KeyboardCtrl.TabIndex = 290;
            this.KeyboardCtrl.KeyboardClick += new TB.Cashier.Client.Shell.Controls.KeyboardClickDelegate(this.KeyboardCtrl_KeyboardClick);
            // 
            // FinishChequeBt
            // 
            this.FinishChequeBt.Font = new System.Drawing.Font("Segoe UI", 7.5F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.FinishChequeBt.ForeColor = System.Drawing.Color.Red;
            this.FinishChequeBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.FinishChequeSmall;
            this.FinishChequeBt.Location = new System.Drawing.Point(934, 20);
            this.FinishChequeBt.Name = "FinishChequeBt";
            this.FinishChequeBt.Size = new System.Drawing.Size(95, 90);
            this.FinishChequeBt.TabIndex = 200;
            this.FinishChequeBt.Text = "Закрытие чека";
            this.FinishChequeBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.FinishChequeBt.UseVisualStyleBackColor = true;
            this.FinishChequeBt.Click += new System.EventHandler(this.FinishChequeBt_Click);
            // 
            // FreeSellBt
            // 
            this.FreeSellBt.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.FreeSellBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.FreeSaleSmall;
            this.FreeSellBt.Location = new System.Drawing.Point(243, 20);
            this.FreeSellBt.Name = "FreeSellBt";
            this.FreeSellBt.Size = new System.Drawing.Size(95, 90);
            this.FreeSellBt.TabIndex = 260;
            this.FreeSellBt.Text = "Свобод. продажа";
            this.FreeSellBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.FreeSellBt.UseVisualStyleBackColor = true;
            this.FreeSellBt.Click += new System.EventHandler(this.FreeSellBt_Click);
            // 
            // ServiceBt
            // 
            this.ServiceBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ServiceBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.ServiceSmall;
            this.ServiceBt.Location = new System.Drawing.Point(824, 20);
            this.ServiceBt.Name = "ServiceBt";
            this.ServiceBt.Size = new System.Drawing.Size(90, 90);
            this.ServiceBt.TabIndex = 210;
            this.ServiceBt.Text = "Сервис";
            this.ServiceBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ServiceBt.UseVisualStyleBackColor = true;
            this.ServiceBt.Click += new System.EventHandler(this.ServiceBt_Click);
            // 
            // DeleteCurrentPosBt
            // 
            this.DeleteCurrentPosBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.DeleteCurrentPosBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.WasteSmall;
            this.DeleteCurrentPosBt.Location = new System.Drawing.Point(360, 20);
            this.DeleteCurrentPosBt.Name = "DeleteCurrentPosBt";
            this.DeleteCurrentPosBt.Size = new System.Drawing.Size(90, 90);
            this.DeleteCurrentPosBt.TabIndex = 250;
            this.DeleteCurrentPosBt.Text = "Тек. позицию";
            this.DeleteCurrentPosBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.DeleteCurrentPosBt.UseVisualStyleBackColor = true;
            this.DeleteCurrentPosBt.Click += new System.EventHandler(this.DeleteCurrentPosBt_Click);
            // 
            // ReturnBt
            // 
            this.ReturnBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ReturnBt.ForeColor = System.Drawing.SystemColors.Highlight;
            this.ReturnBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.ReturnSmall1;
            this.ReturnBt.Location = new System.Drawing.Point(707, 20);
            this.ReturnBt.Name = "ReturnBt";
            this.ReturnBt.Size = new System.Drawing.Size(90, 90);
            this.ReturnBt.TabIndex = 220;
            this.ReturnBt.Text = "Возврат";
            this.ReturnBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ReturnBt.UseVisualStyleBackColor = true;
            this.ReturnBt.Click += new System.EventHandler(this.ReturnBt_Click);
            // 
            // PostponeBt
            // 
            this.PostponeBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.PostponeBt.ForeColor = System.Drawing.Color.OliveDrab;
            this.PostponeBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.DelaySmall1;
            this.PostponeBt.Location = new System.Drawing.Point(590, 20);
            this.PostponeBt.Name = "PostponeBt";
            this.PostponeBt.Size = new System.Drawing.Size(90, 90);
            this.PostponeBt.TabIndex = 230;
            this.PostponeBt.Text = "Отложить";
            this.PostponeBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.PostponeBt.UseVisualStyleBackColor = true;
            this.PostponeBt.Click += new System.EventHandler(this.PostponeBt_Click);
            // 
            // ClearChequeBt
            // 
            this.ClearChequeBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ClearChequeBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.ClearSmall;
            this.ClearChequeBt.Location = new System.Drawing.Point(470, 20);
            this.ClearChequeBt.Name = "ClearChequeBt";
            this.ClearChequeBt.Size = new System.Drawing.Size(90, 90);
            this.ClearChequeBt.TabIndex = 240;
            this.ClearChequeBt.Text = "Сторно";
            this.ClearChequeBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ClearChequeBt.UseVisualStyleBackColor = true;
            this.ClearChequeBt.Click += new System.EventHandler(this.ClearChequeBt_Click);
            // 
            // ClearDiscountBt
            // 
            this.ClearDiscountBt.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.ClearDiscountBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.ClearDiscountSmall;
            this.ClearDiscountBt.Location = new System.Drawing.Point(707, 5);
            this.ClearDiscountBt.Name = "ClearDiscountBt";
            this.ClearDiscountBt.Size = new System.Drawing.Size(95, 95);
            this.ClearDiscountBt.TabIndex = 240;
            this.ClearDiscountBt.Text = "Сбросить скидку";
            this.ClearDiscountBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ClearDiscountBt.UseVisualStyleBackColor = true;
            this.ClearDiscountBt.Click += new System.EventHandler(this.ClearDiscountBt_Click);
            // 
            // MinusQuantityBt
            // 
            this.MinusQuantityBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.MinusQuantityBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.MinusQuantitySmall;
            this.MinusQuantityBt.Location = new System.Drawing.Point(294, 5);
            this.MinusQuantityBt.Name = "MinusQuantityBt";
            this.MinusQuantityBt.Size = new System.Drawing.Size(95, 95);
            this.MinusQuantityBt.TabIndex = 245;
            this.MinusQuantityBt.Text = "кол-во -1";
            this.MinusQuantityBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.MinusQuantityBt.UseVisualStyleBackColor = true;
            this.MinusQuantityBt.Click += new System.EventHandler(this.MinusQuantityBt_Click);
            // 
            // PlusQuantityBt
            // 
            this.PlusQuantityBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.PlusQuantityBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.PlusQuantitySmall;
            this.PlusQuantityBt.Location = new System.Drawing.Point(92, 5);
            this.PlusQuantityBt.Name = "PlusQuantityBt";
            this.PlusQuantityBt.Size = new System.Drawing.Size(95, 95);
            this.PlusQuantityBt.TabIndex = 246;
            this.PlusQuantityBt.Text = "кол-во +1";
            this.PlusQuantityBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.PlusQuantityBt.UseVisualStyleBackColor = true;
            this.PlusQuantityBt.Click += new System.EventHandler(this.PlusQuantityBt_Click);
            // 
            // SetAmountBt
            // 
            this.SetAmountBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.SetAmountBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.ChangeQuantitySmall;
            this.SetAmountBt.Location = new System.Drawing.Point(193, 5);
            this.SetAmountBt.Name = "SetAmountBt";
            this.SetAmountBt.Size = new System.Drawing.Size(95, 95);
            this.SetAmountBt.TabIndex = 244;
            this.SetAmountBt.Text = "Задать кол-во";
            this.SetAmountBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.SetAmountBt.UseVisualStyleBackColor = true;
            this.SetAmountBt.Click += new System.EventHandler(this.SetAmountBt_Click);
            // 
            // SetSumBt
            // 
            this.SetSumBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.SetSumBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.SumSmall;
            this.SetSumBt.Location = new System.Drawing.Point(395, 5);
            this.SetSumBt.Name = "SetSumBt";
            this.SetSumBt.Size = new System.Drawing.Size(95, 95);
            this.SetSumBt.TabIndex = 243;
            this.SetSumBt.Text = "Задать сумму";
            this.SetSumBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.SetSumBt.UseVisualStyleBackColor = true;
            this.SetSumBt.Click += new System.EventHandler(this.SetSumBt_Click);
            // 
            // DiscountPercentBt
            // 
            this.DiscountPercentBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.DiscountPercentBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.DiscountPercSmall;
            this.DiscountPercentBt.Location = new System.Drawing.Point(496, 5);
            this.DiscountPercentBt.Name = "DiscountPercentBt";
            this.DiscountPercentBt.Size = new System.Drawing.Size(95, 95);
            this.DiscountPercentBt.TabIndex = 242;
            this.DiscountPercentBt.Text = "Скидка в %";
            this.DiscountPercentBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.DiscountPercentBt.UseVisualStyleBackColor = true;
            this.DiscountPercentBt.Click += new System.EventHandler(this.DiscountPercentBt_Click);
            // 
            // DiscountSumBt
            // 
            this.DiscountSumBt.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.DiscountSumBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.DiscountMoneySmall;
            this.DiscountSumBt.Location = new System.Drawing.Point(597, 5);
            this.DiscountSumBt.Name = "DiscountSumBt";
            this.DiscountSumBt.Size = new System.Drawing.Size(95, 95);
            this.DiscountSumBt.TabIndex = 241;
            this.DiscountSumBt.Text = "Скидка суммой";
            this.DiscountSumBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.DiscountSumBt.UseVisualStyleBackColor = true;
            this.DiscountSumBt.Click += new System.EventHandler(this.DiscountSumBt_Click);
            // 
            // NameBt
            // 
            this.NameBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.NameBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.SearchCat;
            this.NameBt.Location = new System.Drawing.Point(676, 6);
            this.NameBt.Name = "NameBt";
            this.NameBt.Size = new System.Drawing.Size(50, 50);
            this.NameBt.TabIndex = 3;
            this.NameBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.NameBt.UseVisualStyleBackColor = true;
            this.NameBt.Click += new System.EventHandler(this.NameBt_Click);
            // 
            // MainFormV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1443, 1045);
            this.ControlBox = false;
            this.Controls.Add(this.LeftPanel);
            this.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainFormV2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Фронт кассира";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.LeftPanel.ResumeLayout(false);
            this.BtnPanel.ResumeLayout(false);
            this.DiscountPanel.ResumeLayout(false);
            this.SummaryPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chequeBindingSource)).EndInit();
            this.CodePanel.ResumeLayout(false);
            this.CodePanel.PerformLayout();
            this.CurrentPanel.ResumeLayout(false);
            this.GridPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChequeLineGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource)).EndInit();
            this.InfoPanel.ResumeLayout(false);
            this.InfoCenterPanel.ResumeLayout(false);
            this.InfoRightPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.shiftBindingSource)).EndInit();
            this.InfoLeftPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.Panel DiscountPanel;
        private System.Windows.Forms.Panel SummaryPanel;
        private System.Windows.Forms.Panel CodePanel;
        private System.Windows.Forms.Panel CurrentPanel;
        private System.Windows.Forms.Panel GridPanel;
        private System.Windows.Forms.Panel InfoPanel;
        private System.Windows.Forms.DataGridView ChequeLineGrid;
        private System.Windows.Forms.Label CodeLb;
        private System.Windows.Forms.TextBox CodeTb;
        private System.Windows.Forms.Label ChequeNoLb;
        private System.Windows.Forms.Label ChequeNoTextLb;
        private System.Windows.Forms.BindingSource chequeBindingSource;
        private System.Windows.Forms.Label SumLb;
        private System.Windows.Forms.Label SummaryTextLb;
        private System.Windows.Forms.BindingSource linesBindingSource;
        private ButtonControl ClearChequeBt;
        private ButtonControl FinishChequeBt;
        private System.Windows.Forms.Label ShiftLb;
        private System.Windows.Forms.Label ShiftTextLb;
        private System.Windows.Forms.BindingSource shiftBindingSource;
        private ButtonControl PostponeBt;
        private ButtonControl ReturnBt;
        private ButtonControl DeleteCurrentPosBt;
        private System.Windows.Forms.Label CurrentPosLb;
        private System.Windows.Forms.Panel InfoRightPanel;
        private System.Windows.Forms.Panel InfoLeftPanel;
        private System.Windows.Forms.Panel InfoCenterPanel;
        private System.Windows.Forms.Label ModeTextLb;
        private ButtonControl ServiceBt;
        private Controls.KeyboardControl KeyboardCtrl;
        private System.Windows.Forms.TextBox NameTb;
        private ButtonControl DiscountPercentBt;
        private ButtonControl DiscountSumBt;
        private ButtonControl FreeSellBt;
        private ButtonControl SetSumBt;
        private System.Windows.Forms.Panel BtnPanel;
        private ButtonControl NameBt;
        private ButtonControl SetAmountBt;
        private ButtonControl MinusQuantityBt;
        private ButtonControl PlusQuantityBt;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn discountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumDataGridViewTextBoxColumn;
        private ButtonControl ClearDiscountBt;
    }
}