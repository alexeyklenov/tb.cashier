﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Cashier.Equipment.Interface;
using Microsoft.Practices.Unity;
using TB.Cashier.Client.Shell.Wait;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces.Const;
using Utils;

namespace TB.Cashier.Client.Shell.FinishForms
{
    public partial class FinishCheque : Form
    {
        private readonly ChequeDto _cheque;

        public FinishCheque(ChequeDto cheque)
        {
            InitializeComponent();
            _cheque = cheque;

            if (_cheque.SumByClient == 0)
                _cheque.SumByClient = _cheque.Sum;

            chequeDtoBindingSource.DataSource = _cheque;

            CashTb.GotFocus += ClientMoneySum_GotFocus;
            CashTb.MouseUp += ClientMoneySum_MouseUp;
            CashTb.Leave += ClientMoneySum_Leave;
            CashTb.Tag = false;

            CardTb.GotFocus += ClientMoneySum_GotFocus;
            CardTb.MouseUp += ClientMoneySum_MouseUp;
            CardTb.Leave += ClientMoneySum_Leave;
            CardTb.Tag = false;

            CreditTb.GotFocus += ClientMoneySum_GotFocus;
            CreditTb.MouseUp += ClientMoneySum_MouseUp;
            CreditTb.Leave += ClientMoneySum_Leave;
            CreditTb.Tag = false;

            if (_cheque.PaymentTypeSum.Any())
            {
                foreach (var payment in _cheque.PaymentTypeSum)
                {
                    switch (payment.Key)
                    {
                        case DescValCodes.Card:
                            CardTb.Text = payment.Value.ToString("N2");
                            break;
                        case DescValCodes.Credit:
                            CreditTb.Text = payment.Value.ToString("N2");
                            break;
                        case DescValCodes.Cash:
                            CashTb.Text = payment.Value.ToString("N2");
                            break;
                    }

                }
            }
            else
            {
                _cheque.PaymentTypeSum.Add(DescValCodes.Cash, 0);
                _cheque.PaymentTypeSum.Add(DescValCodes.Card, 0);
                _cheque.PaymentTypeSum.Add(DescValCodes.Credit, 0);

                CardTb.Text = @"0";
                CreditTb.Text = @"0";
                CashTb.Text = _cheque.Sum.ToString("N2");
            }

        }

        private void PrintButton_Click(object sender, EventArgs e)
        {

            _cheque.PaymentTypeSum[DescValCodes.Cash] = decimal.Parse(CashTb.Text);
            _cheque.PaymentTypeSum[DescValCodes.Card] = decimal.Parse(CardTb.Text);
            _cheque.PaymentTypeSum[DescValCodes.Credit] = decimal.Parse(CreditTb.Text);
            if (_cheque.Sum > _cheque.PaymentTypeSum.Sum(p => p.Value))
            {
                MessageBox.Show(@"Суммы оплаты не достаточно", @"Внимание", MessageBoxButtons.OK);
                return;
            }
            using (var wD = new WaitDialog(@"Печать чека"))
            {
                var chequePrinter = UnityContext.Container.Resolve<IChequePrinter>();

                var res = chequePrinter.CloseCheque(_cheque, _cheque.IsReturn);
                if (!res)
                {
                    wD.Hide();
                    MessageBox.Show(chequePrinter.LastError(), @"Ошибка", MessageBoxButtons.OK);
                    
                    chequePrinter.CancelCheque();

                    DialogResult = DialogResult.No;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }


        }

        private void FinishCheque_Shown(object sender, EventArgs e)
        {
        }


        void ClientMoneySum_Leave(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox != null) textBox.Tag = false;
        }


        void ClientMoneySum_GotFocus(object sender, EventArgs e)
        {
            // Select all text only if the mouse isn't down.
            // This makes tabbing to the textbox give focus.
            if (MouseButtons != MouseButtons.None) 
                return;
            var textBox = sender as TextBox;
            if (textBox == null) 
                return;
            textBox.SelectAll();
            textBox.Tag = true;
        }

        void ClientMoneySum_MouseUp(object sender, MouseEventArgs e)
        {
            // Web browsers like Google Chrome select the text on mouse up.
            // They only do it if the textbox isn't already focused,
            // and if the user hasn't selected all text.
            var textBox = sender as TextBox;
            if (textBox != null && (!((bool)textBox.Tag) && textBox.SelectionLength == 0))
            {
                textBox.Tag = true;
                textBox.SelectAll();
            }
        }

        private void CardCreditTb_TextChanged(object sender, EventArgs e)
        {
            if (CardTb.Text == string.Empty || CreditTb.Text == string.Empty || CashTb.Text == string.Empty)
                return;
            var cashSum = _cheque.Sum - decimal.Parse(CardTb.Text) - decimal.Parse(CreditTb.Text);
            CashTb.Text = cashSum < 0 ? @"0" : cashSum.ToString("N2");
        }

        private void CashTb_TextChanged(object sender, EventArgs e)
        {
            if (CardTb.Text == string.Empty || CreditTb.Text == string.Empty || CashTb.Text == string.Empty)
                return;
            _cheque.SumByClient = decimal.Parse(CardTb.Text) + decimal.Parse(CashTb.Text) + decimal.Parse(CreditTb.Text);
            ChangeLb.Text = _cheque.SumByClient - _cheque.Sum > 0
                ? (_cheque.SumByClient - _cheque.Sum).ToString("C")
                : @"0";
        }

        private void FinishCheque_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.LineFeed)
            {
                e.Handled = true;
                PrintButton_Click(this, null);
            }
        }
    }
}
