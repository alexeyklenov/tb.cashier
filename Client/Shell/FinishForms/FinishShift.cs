﻿using System;
using System.Windows.Forms;
using Cashier.Equipment.Interface;
using Microsoft.Practices.Unity;
using TB.Cashier.Server.Services.Interfaces;
using TB.Services.Infrastructure.Client;
using Utils;

namespace TB.Cashier.Client.Shell.FinishForms
{
    public partial class FinishShift : Form
    {

        private readonly IChequePrinter _chequePrinter;
        public FinishShift()
        {
            InitializeComponent();

            _chequePrinter = UnityContext.Container.Resolve<IChequePrinter>();
        }

        private void FinishShiftBt_Click(object sender, EventArgs e)
        {
            new CabClientProxy<IChequeService>().Execute(x=>x.CloseCurrentShift());
            Close();
        }

        private void ZBt_Click(object sender, EventArgs e)
        {
            if (!_chequePrinter.PrintZReport())
                MessageBox.Show(_chequePrinter.LastError(), @"Ошибка", MessageBoxButtons.OK);
            else
                FinishShiftBt_Click(this, new EventArgs());
        }

        private void XBt_Click(object sender, EventArgs e)
        {
            if (!_chequePrinter.PrintXReport())
                MessageBox.Show(_chequePrinter.LastError(), @"Ошибка", MessageBoxButtons.OK);
        }
    }
}
