﻿using TB.Cashier.Client.Shell.Controls;

namespace TB.Cashier.Client.Shell.FinishForms
{
    partial class FinishCheque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SumTextLb = new System.Windows.Forms.Label();
            this.SumLb = new System.Windows.Forms.Label();
            this.chequeDtoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PaymentFormLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CreditTb = new TB.Cashier.Client.Shell.Controls.NumericTextBox();
            this.CreditLb = new System.Windows.Forms.Label();
            this.CardTb = new TB.Cashier.Client.Shell.Controls.NumericTextBox();
            this.CashTb = new TB.Cashier.Client.Shell.Controls.NumericTextBox();
            this.CardLb = new System.Windows.Forms.Label();
            this.CashLb = new System.Windows.Forms.Label();
            this.PrintButton = new System.Windows.Forms.Button();
            this.ChangeTextLb = new System.Windows.Forms.Label();
            this.ChangeLb = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chequeDtoBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SumTextLb
            // 
            this.SumTextLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.SumTextLb.Location = new System.Drawing.Point(5, 5);
            this.SumTextLb.Name = "SumTextLb";
            this.SumTextLb.Size = new System.Drawing.Size(374, 25);
            this.SumTextLb.TabIndex = 0;
            this.SumTextLb.Text = "Сумма к оплате:";
            this.SumTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SumLb
            // 
            this.SumLb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SumLb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.chequeDtoBindingSource, "Sum", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "C2"));
            this.SumLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.SumLb.Font = new System.Drawing.Font("Segoe UI", 16.25F);
            this.SumLb.ForeColor = System.Drawing.Color.DarkRed;
            this.SumLb.Location = new System.Drawing.Point(5, 30);
            this.SumLb.Name = "SumLb";
            this.SumLb.Size = new System.Drawing.Size(374, 40);
            this.SumLb.TabIndex = 1;
            this.SumLb.Text = "1000";
            this.SumLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chequeDtoBindingSource
            // 
            this.chequeDtoBindingSource.DataSource = typeof(TB.Cashier.Server.Services.Dto.ChequeDto);
            // 
            // PaymentFormLabel
            // 
            this.PaymentFormLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.PaymentFormLabel.Location = new System.Drawing.Point(5, 70);
            this.PaymentFormLabel.Name = "PaymentFormLabel";
            this.PaymentFormLabel.Size = new System.Drawing.Size(374, 43);
            this.PaymentFormLabel.TabIndex = 2;
            this.PaymentFormLabel.Text = "Способ оплаты:";
            this.PaymentFormLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.CreditTb);
            this.panel1.Controls.Add(this.CreditLb);
            this.panel1.Controls.Add(this.CardTb);
            this.panel1.Controls.Add(this.CashTb);
            this.panel1.Controls.Add(this.CardLb);
            this.panel1.Controls.Add(this.CashLb);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(5, 113);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(374, 195);
            this.panel1.TabIndex = 3;
            // 
            // CreditTb
            // 
            this.CreditTb.EmptyIsZero = true;
            this.CreditTb.Location = new System.Drawing.Point(86, 134);
            this.CreditTb.Name = "CreditTb";
            this.CreditTb.Size = new System.Drawing.Size(274, 33);
            this.CreditTb.TabIndex = 3;
            this.CreditTb.TextChanged += new System.EventHandler(this.CardCreditTb_TextChanged);
            // 
            // CreditLb
            // 
            this.CreditLb.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.CreditLb.Image = global::TB.Cashier.Client.Shell.Properties.Resources.CreditExtraSmall;
            this.CreditLb.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CreditLb.Location = new System.Drawing.Point(1, 126);
            this.CreditLb.Name = "CreditLb";
            this.CreditLb.Size = new System.Drawing.Size(64, 41);
            this.CreditLb.TabIndex = 4;
            this.CreditLb.Text = "Кредит:";
            this.CreditLb.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // CardTb
            // 
            this.CardTb.EmptyIsZero = true;
            this.CardTb.Location = new System.Drawing.Point(86, 79);
            this.CardTb.Name = "CardTb";
            this.CardTb.Size = new System.Drawing.Size(274, 33);
            this.CardTb.TabIndex = 2;
            this.CardTb.TextChanged += new System.EventHandler(this.CardCreditTb_TextChanged);
            // 
            // CashTb
            // 
            this.CashTb.EmptyIsZero = true;
            this.CashTb.Location = new System.Drawing.Point(86, 24);
            this.CashTb.Name = "CashTb";
            this.CashTb.Size = new System.Drawing.Size(274, 33);
            this.CashTb.TabIndex = 1;
            this.CashTb.TextChanged += new System.EventHandler(this.CashTb_TextChanged);
            // 
            // CardLb
            // 
            this.CardLb.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.CardLb.Image = global::TB.Cashier.Client.Shell.Properties.Resources.CardsExtraSmall;
            this.CardLb.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CardLb.Location = new System.Drawing.Point(1, 71);
            this.CardLb.Name = "CardLb";
            this.CardLb.Size = new System.Drawing.Size(64, 41);
            this.CardLb.TabIndex = 1;
            this.CardLb.Text = "Карта:";
            this.CardLb.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // CashLb
            // 
            this.CashLb.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.CashLb.Image = global::TB.Cashier.Client.Shell.Properties.Resources.CashExtraSmall;
            this.CashLb.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CashLb.Location = new System.Drawing.Point(2, 16);
            this.CashLb.Name = "CashLb";
            this.CashLb.Size = new System.Drawing.Size(64, 41);
            this.CashLb.TabIndex = 0;
            this.CashLb.Text = "Наличные:";
            this.CashLb.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // PrintButton
            // 
            this.PrintButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PrintButton.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.PrintButton.ForeColor = System.Drawing.Color.Green;
            this.PrintButton.Image = global::TB.Cashier.Client.Shell.Properties.Resources.PrintChequeSmall;
            this.PrintButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.PrintButton.Location = new System.Drawing.Point(5, 402);
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(374, 67);
            this.PrintButton.TabIndex = 0;
            this.PrintButton.Text = "ПЕЧАТЬ";
            this.PrintButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.PrintButton.UseVisualStyleBackColor = true;
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // ChangeTextLb
            // 
            this.ChangeTextLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.ChangeTextLb.Location = new System.Drawing.Point(5, 308);
            this.ChangeTextLb.Name = "ChangeTextLb";
            this.ChangeTextLb.Size = new System.Drawing.Size(374, 38);
            this.ChangeTextLb.TabIndex = 4;
            this.ChangeTextLb.Text = "Сдача:";
            this.ChangeTextLb.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // ChangeLb
            // 
            this.ChangeLb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ChangeLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.ChangeLb.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.ChangeLb.ForeColor = System.Drawing.SystemColors.Highlight;
            this.ChangeLb.Location = new System.Drawing.Point(5, 346);
            this.ChangeLb.Name = "ChangeLb";
            this.ChangeLb.Size = new System.Drawing.Size(374, 41);
            this.ChangeLb.TabIndex = 5;
            this.ChangeLb.Text = "0";
            this.ChangeLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FinishCheque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 474);
            this.Controls.Add(this.ChangeLb);
            this.Controls.Add(this.ChangeTextLb);
            this.Controls.Add(this.PrintButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PaymentFormLabel);
            this.Controls.Add(this.SumLb);
            this.Controls.Add(this.SumTextLb);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FinishCheque";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Закрытие чека";
            this.Shown += new System.EventHandler(this.FinishCheque_Shown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FinishCheque_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.chequeDtoBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label SumTextLb;
        private System.Windows.Forms.Label SumLb;
        private System.Windows.Forms.BindingSource chequeDtoBindingSource;
        private System.Windows.Forms.Label PaymentFormLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button PrintButton;
        private System.Windows.Forms.Label CashLb;
        private NumericTextBox CardTb;
        private NumericTextBox CashTb;
        private System.Windows.Forms.Label CardLb;
        private System.Windows.Forms.Label ChangeTextLb;
        private System.Windows.Forms.Label ChangeLb;
        private NumericTextBox CreditTb;
        private System.Windows.Forms.Label CreditLb;
    }
}