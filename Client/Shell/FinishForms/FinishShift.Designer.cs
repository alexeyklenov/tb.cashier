﻿namespace TB.Cashier.Client.Shell.FinishForms
{
    partial class FinishShift
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.XBt = new System.Windows.Forms.Button();
            this.ZBt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // XBt
            // 
            this.XBt.Location = new System.Drawing.Point(12, 12);
            this.XBt.Name = "XBt";
            this.XBt.Size = new System.Drawing.Size(360, 50);
            this.XBt.TabIndex = 0;
            this.XBt.Text = "X отчет";
            this.XBt.UseVisualStyleBackColor = true;
            this.XBt.Click += new System.EventHandler(this.XBt_Click);
            // 
            // ZBt
            // 
            this.ZBt.Location = new System.Drawing.Point(12, 68);
            this.ZBt.Name = "ZBt";
            this.ZBt.Size = new System.Drawing.Size(360, 50);
            this.ZBt.TabIndex = 1;
            this.ZBt.Text = "Z отчет";
            this.ZBt.UseVisualStyleBackColor = true;
            this.ZBt.Click += new System.EventHandler(this.ZBt_Click);
            // 
            // FinishShift
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 134);
            this.Controls.Add(this.ZBt);
            this.Controls.Add(this.XBt);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FinishShift";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Закрытие смены";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button XBt;
        private System.Windows.Forms.Button ZBt;
    }
}