﻿
using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TB.Cashier.Client.Shell
{
    /// <summary>
    /// Selected Win AI Function Calls
    /// </summary>

    public class WinApi
    {
        [DllImport("user32.dll", EntryPoint = "GetSystemMetrics")]
        public static extern int GetSystemMetrics(int which);

        [DllImport("user32.dll")]
        public static extern void
            SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter,
                         int X, int Y, int width, int height, uint flags);

        private const int SM_CXSCREEN = 0;
        private const int SM_CYSCREEN = 1;
        private static IntPtr HWND_TOP = IntPtr.Zero;
        private const int SWP_SHOWWINDOW = 64; // 0x0040

        public static int ScreenX
        {
            get { return GetSystemMetrics(SM_CXSCREEN); }
        }

        public static int ScreenY
        {
            get { return GetSystemMetrics(SM_CYSCREEN); }
        }

        public static void SetWinFullScreen(IntPtr hwnd)
        {
            SetWindowPos(hwnd, HWND_TOP, 0, 0, ScreenX, ScreenY, SWP_SHOWWINDOW);
        }
    }

    public class CaptureSystemKeys
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct Kbdllhookstruct
        {
            public readonly Keys key;
            private readonly int scanCode;
            public readonly int flags;
            private readonly int time;
            private readonly IntPtr extra;
        }
        //System level functions to be used for hook and unhook keyboard input  
        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int id, LowLevelKeyboardProc callback, IntPtr hMod, uint dwThreadId);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool UnhookWindowsHookEx(IntPtr hook);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hook, int nCode, IntPtr wp, IntPtr lp);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string name);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern short GetAsyncKeyState(Keys key);
        //Declaring Global objects     
        private IntPtr _ptrHook;
        private LowLevelKeyboardProc _objKeyboardProcess;

        public event Action CaptureHookedEvent;

        private IntPtr CaptureKey(int nCode, IntPtr wp, IntPtr lp)
        {
            if (nCode >= 0)
            {
                var objKeyInfo = (Kbdllhookstruct)Marshal.PtrToStructure(lp, typeof(Kbdllhookstruct));

                // Disabling Windows keys 

                if (objKeyInfo.key == Keys.RWin || objKeyInfo.key == Keys.LWin ||
                    objKeyInfo.key == Keys.Tab && HasAltModifier(objKeyInfo.flags) ||
                    objKeyInfo.key == Keys.Escape && (Control.ModifierKeys & Keys.Control) == Keys.Control)
                {
                    if (CaptureHookedEvent != null) CaptureHookedEvent();
                    return (IntPtr) 1; // if 0 is returned then All the above keys will be enabled
                }
            }
            return CallNextHookEx(_ptrHook, nCode, wp, lp);
        }

        static bool HasAltModifier(int flags)
        {
            return (flags & 0x20) == 0x20;
        }

        public static CaptureSystemKeys StartCapture()
        {
            var captureContainer = new CaptureSystemKeys();
            var objCurrentModule = Process.GetCurrentProcess().MainModule;
            captureContainer._objKeyboardProcess = captureContainer.CaptureKey;
            captureContainer._ptrHook = SetWindowsHookEx(13, captureContainer._objKeyboardProcess, GetModuleHandle(objCurrentModule.ModuleName), 0);
            return captureContainer;
        }
    }

    /// <summary>
    /// Class used to preserve / restore state of the form
    /// </summary>
    public static class FormState
    {
        private static CaptureSystemKeys _captureContainer;
        private static Form _targetForm;

        public static void BlockedFullScreen(Form targetForm)
        {
            targetForm.WindowState = FormWindowState.Maximized;
            WinApi.SetWinFullScreen(targetForm.Handle);

            _targetForm = targetForm;
            _captureContainer = CaptureSystemKeys.StartCapture();
            _captureContainer.CaptureHookedEvent += _captureContainer_CaptureHookedEvent;
        }

        static void _captureContainer_CaptureHookedEvent()
        {
            _targetForm.Invoke(new Action(_targetForm.Controls[0].Select));
        }



        public static void NormalFullScreen(Form targetForm)
        {
            targetForm.WindowState = FormWindowState.Normal;
            targetForm.Width = Screen.PrimaryScreen.WorkingArea.Width;
            targetForm.Height = Screen.PrimaryScreen.WorkingArea.Height;
            targetForm.TopMost = false;
            targetForm.Left = targetForm.Top = 0;

        }
    }
}
