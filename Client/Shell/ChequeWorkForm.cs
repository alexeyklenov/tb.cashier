﻿using System;
using System.Windows.Forms;
using TB.Cashier.Client.Shell.SelectForms;
using TB.Cashier.Client.Shell.Wait;

namespace TB.Cashier.Client.Shell
{
    public partial class ChequeWorkForm : Form
    {
        private readonly IMainForm _mainForm = (Program.AppContext.MainForm as IMainForm);

        public ChequeWorkForm()
        {
            InitializeComponent();
        }

        private void ShowPostponedBt_Click(object sender, EventArgs e)
        {
            using (var pf = new PostponedForm())
            {
                if (pf.ShowDialog() != DialogResult.OK || pf.SelectedId <= 0)
                {
                    return;
                }
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    _mainForm.ChequeService.Execute(
                        s => s.DeleteChequeById(_mainForm.CurrentCheque.Id.GetValueOrDefault()));
                    _mainForm.ChequeService.Execute(s => s.ActivateCheque(pf.SelectedId));
                    _mainForm.CurrentCheque.Id = pf.SelectedId;
                    _mainForm.RefreshCurrentCheque();
                    Close();
                }
            }
        }


        private void ShowShiftChequesBt_Click(object sender, EventArgs e)
        {
            using (var pf = new ShiftChequesForm())
            {
                pf.ShowDialog();
            }
        }
    }
}
