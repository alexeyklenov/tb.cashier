﻿using System;
using System.Configuration;
using System.Windows.Forms;
using Cashier.Equipment.BarcodeScanners;
using Cashier.Equipment.Interface;
using Microsoft.Practices.Unity;
using Utils;

namespace TB.Cashier.Client.Shell
{
    static class Program
    {
        public static ApplicationContext AppContext { get; set; }
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.SetData("appAddress",
              string.Format("{0}://{1}:{2}", "net.tcp", ConfigurationManager.AppSettings["ServerName"],
                  ConfigurationManager.AppSettings["ServerPort"]));

            UnityContext.Container.RegisterType(typeof(IChequePrinter), Type.GetType(ConfigurationManager.AppSettings["ChequePrinter"]), null,
                new ContainerControlledLifetimeManager());

            UnityContext.Container.RegisterType(typeof(IBarCodeScanner), typeof(ComScanner),null,
                new ContainerControlledLifetimeManager());

            UnityContext.Container.RegisterType<ComScanner>(new InjectionConstructor(new object[] { Config.ScannerComPort }));


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            AppContext = new ApplicationContext(new SplashScreen());
            Application.Run(AppContext);
            
        }
        public static void SetMainForm(Form form)
        {
            var oldForm = AppContext.MainForm;
            AppContext.MainForm = form;
            AppContext.MainForm.Show();
            oldForm.Close();
        }
    }
}
