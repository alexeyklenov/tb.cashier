﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Mime;
using System.Windows.Forms;

namespace TB.Cashier.Client.Shell.Controls
{
    public partial class BaseForm : Form
    {
        public bool OnlyCloseButton { get; set; }
        public bool NoKeyboard { get; set; }

        private List<TextBox> TbControls { get; set; }
        private TextBox SelectedTb { get; set; }

        private KeyboardControl _kbControl;

        protected DialogResult? OverrideDialogResult { get; set; }

        public BaseForm()
        {
            InitializeComponent();
            if(!NoKeyboard)
            _kbControl = new KeyboardControl(){Parent = KbdPanel, Dock = DockStyle.Fill};
        }


        private void BaseForm_Load(object sender, EventArgs e)
        {
            if (OnlyCloseButton)
            {
                OkBtn.Visible = false;
                CloseBtn.Dock = DockStyle.Fill;
            }
            if (!NoKeyboard)
            {
                _kbControl.KeyboardClick += _kbControl_KeyboardClick;
                KbdPanel.Height = 250;
                Height += KbdPanel.Height;
            }

            BaseForm_Resize(this,new EventArgs());

            TbControls = Controls.Cast<Control>().OfType<TextBox>().ToList();
            TbControls.ForEach(c => c.GotFocus += (o, args) => SelectedTb = o as TextBox);
        }

        void _kbControl_KeyboardClick(char key)
        {
            if (SelectedTb != null)
            {
                bool isEnter;
                bool isLineFeed;
                SelectedTb.Text = _kbControl.ApplyValue(SelectedTb.Text, key, out isEnter, out isLineFeed);

                SelectedTb.SelectionStart = SelectedTb.TextLength;
                SelectedTb.SelectionLength = 0;

                if(isEnter)
                    OkClose(this, new EventArgs());
            }
        }

        private void BaseForm_Resize(object sender, EventArgs e)
        {
            OkBtn.Width = CloseBtn.Width = BtnPanel.Width/2;
        }

        protected void OkClose(object sender = null, EventArgs e = null)
        {
            DialogResult = OverrideDialogResult ?? DialogResult.OK;
            CloseForm();
        }

        protected void CancelClose(object sender = null, EventArgs e = null)
        {
            DialogResult = OverrideDialogResult ?? DialogResult.Cancel;
            CloseForm();
         
        }

        protected void CloseForm()
        {
            FormClosing -= BaseForm_FormClosing;
            Close();
        }

        private void BaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
