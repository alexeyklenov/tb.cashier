﻿namespace TB.Cashier.Client.Shell.Controls
{
    partial class KeyboardControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // KeyboardControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold);
            this.Margin = new System.Windows.Forms.Padding(9);
            this.Name = "KeyboardControl";
            this.Padding = new System.Windows.Forms.Padding(13, 15, 13, 15);
            this.Size = new System.Drawing.Size(393, 420);
            this.Resize += new System.EventHandler(this.KeyboardControl_Resize);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
