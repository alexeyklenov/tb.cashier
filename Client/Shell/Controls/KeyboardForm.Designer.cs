﻿namespace TB.Cashier.Client.Shell.Controls
{
    partial class KeyboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.keyboardControl1 = new TB.Cashier.Client.Shell.Controls.KeyboardControl();
            this.SuspendLayout();
            // 
            // keyboardControl1
            // 
            this.keyboardControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keyboardControl1.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold);
            this.keyboardControl1.Location = new System.Drawing.Point(0, 0);
            this.keyboardControl1.Margin = new System.Windows.Forms.Padding(9);
            this.keyboardControl1.Name = "keyboardControl1";
            this.keyboardControl1.Padding = new System.Windows.Forms.Padding(13, 15, 13, 15);
            this.keyboardControl1.Size = new System.Drawing.Size(284, 261);
            this.keyboardControl1.TabIndex = 0;
            // 
            // KeyboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.ControlBox = false;
            this.Controls.Add(this.keyboardControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "KeyboardForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "KeyboardForm";
            this.ResumeLayout(false);

        }

        #endregion

        private KeyboardControl keyboardControl1;
    }
}