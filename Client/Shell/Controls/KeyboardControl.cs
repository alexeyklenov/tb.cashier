﻿using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace TB.Cashier.Client.Shell.Controls
{
    public delegate void KeyboardClickDelegate(char key);


    public partial class KeyboardControl : UserControl
    {
        private int _buttonHeight;
        private int _buttonWidth;
        private const int PadStep = 2;
        private const int RowCount = 5;
        private const int ColCount = 3;
        private const string BackspaceText = "<-";


        private readonly List<Button> _numericBts = new List<Button>();
        private readonly Button _enter;

        public event KeyboardClickDelegate KeyboardClick; 

        public KeyboardControl()
        {
            InitializeComponent();
            
            _numericBts.Clear();
            if(_enter!=null)
                _enter.Dispose();
            
            for (var i = 1; i <= 9; i++)
            {
                var bt = new Button
                {
                    Text = i.ToString(CultureInfo.InvariantCulture),
                    TabIndex = i,
                    Parent = this,
                    Visible = true,
                    FlatStyle = FlatStyle.System,
                    Font =  Font,
                    UseVisualStyleBackColor = true
                };
                _numericBts.Add(bt);

            }

            _numericBts.Add(new Button
            {
                Text = @"0",
                TabIndex = 10,
                Parent = this,
                Visible = true,
                FlatStyle = FlatStyle.System,
                Font = Font,
                UseVisualStyleBackColor = true
            });
            
            _numericBts.Add(new Button
            {
                Text = @",",
                TabIndex = 11,
                Parent = this,
                Visible = true,
                FlatStyle = FlatStyle.System,
                Font = Font,
                UseVisualStyleBackColor = true
            });
            _numericBts.Add(new Button
            {
                Text = BackspaceText,
                TabIndex = 12,
                Parent = this,
                Visible = true,
                FlatStyle = FlatStyle.System,
                Font = Font,
                UseVisualStyleBackColor = true
            });

            _numericBts.All(b =>
            {
                b.Click += KeyClick;
                return true;
            });

            _enter = new Button
            {
                Text = Properties.Resources.KeyboardEnterText,
                Parent = this,
                Visible = true,
                FlatStyle = FlatStyle.Standard,
                ForeColor = Color.DarkGreen,
                UseVisualStyleBackColor = true
            };
            _enter.Click += KeyClick;
        }

        private void KeyClick(object sender, System.EventArgs e)
        {
            if (KeyboardClick == null)
                return;
            var button = sender as Button;
            if (button != null)
                KeyboardClick.Invoke(sender == _enter
                    ? (char) Keys.Enter
                    : button.Text == BackspaceText ? (char) Keys.Back : button.Text[0]);
        }

        public string ApplyValue(string sourceValue, char key, out bool isEnter, out bool isLineFeed )
        {
            isEnter = isLineFeed = false;

            switch (key)
            {
                case (char)Keys.LineFeed:
                    isLineFeed = true;
                    break;

                case (char)Keys.Enter:
                    isEnter = true;
                    break;

                case (char)Keys.Back:
                    if (sourceValue != string.Empty)
                        sourceValue = sourceValue.Remove(sourceValue.Count() - 1);
                    break;

                default:
                    sourceValue += key;
                    break;

            }

            return sourceValue;
        }

        private void KeyboardControl_Resize(object sender, System.EventArgs e)
        {
            _buttonHeight = (ClientSize.Height - PadStep * 2) / RowCount;

            _buttonWidth = (ClientSize.Width - PadStep*2)/ColCount;

            SizeButtons();
        }

        private void SizeButtons()
        {
            _numericBts.All(b =>
            {
                b.Width = _buttonWidth;
                b.Height = _buttonHeight;
                return true;
            });

            for (var y = 0; y < RowCount-1; y++)
            {
                for (var x = 0; x < ColCount; x++)
                {
                    _numericBts[y*3 + (x)].Location = new Point(x*_buttonWidth+PadStep, y*_buttonHeight+PadStep);
                }
            }

            _enter.Width = _numericBts[2].Location.X + _buttonWidth;//_buttonWidth*ColCount + PadStep*(ColCount - 1);//ClientSize.Width-(PadStep*2);
            _enter.Height = _buttonHeight;

            _enter.Location = new Point(PadStep,(RowCount-1)*_buttonHeight+PadStep);

        }
    }
}
