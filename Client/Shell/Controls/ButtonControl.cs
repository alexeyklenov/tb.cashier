﻿using System.Drawing;
using System.Windows.Forms;

namespace TB.Cashier.Client.Shell.Controls
{
    public partial class ButtonControl : Button
    {
        public ButtonControl()
        {
            InitializeComponent();
            TextAlign = ContentAlignment.BottomRight;
            Font = new Font(Program.AppContext == null ? new FontFamily("Segoe UI") : Program.AppContext.MainForm.Font.FontFamily, 8);
        }
    }
}
