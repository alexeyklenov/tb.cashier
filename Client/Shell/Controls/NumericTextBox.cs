﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace TB.Cashier.Client.Shell.Controls
{
    public partial class NumericTextBox : TextBox
    {
        public bool EmptyIsZero { get; set; }

        public NumericTextBox()
        {
            InitializeComponent();
            KeyPress += NumericTextBox_KeyPress;
            TextChanged += NumericTextBox_TextChanged;
        }

        private void NumericTextBox_TextChanged(object sender, EventArgs e)
        {
            Text = Text.Replace('.', CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.First());
            Text = Text.Replace(',', CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.First());

            if (EmptyIsZero && Text == string.Empty)
                Text = @"0";

        }

        void NumericTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Back)
            {
            }
            else if (e.KeyChar == ',' ||
                     e.KeyChar == '.' && !Text.Contains(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
            {
                e.KeyChar = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.First();
            }
            else
            {
                int digit;
                if (!int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out digit))
                {
                    e.Handled = true;
                }

            }
        }
    }
}
