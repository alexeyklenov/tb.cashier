﻿namespace TB.Cashier.Client.Shell.Controls
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.BtnPanel = new System.Windows.Forms.Panel();
            this.OkBtn = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.CloseBtn = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.KbdPanel = new System.Windows.Forms.Panel();
            this.BtnPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnPanel
            // 
            this.BtnPanel.Controls.Add(this.OkBtn);
            this.BtnPanel.Controls.Add(this.CloseBtn);
            this.BtnPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BtnPanel.Location = new System.Drawing.Point(0, 460);
            this.BtnPanel.Name = "BtnPanel";
            this.BtnPanel.Size = new System.Drawing.Size(971, 66);
            this.BtnPanel.TabIndex = 0;
            // 
            // OkBtn
            // 
            this.OkBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.OkBtn.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.OkBtn.ForeColor = System.Drawing.Color.OliveDrab;
            this.OkBtn.Image = ((System.Drawing.Image)(resources.GetObject("OkBtn.Image")));
            this.OkBtn.Location = new System.Drawing.Point(0, 0);
            this.OkBtn.Name = "OkBtn";
            this.OkBtn.Size = new System.Drawing.Size(239, 66);
            this.OkBtn.TabIndex = 1;
            this.OkBtn.Text = "ОК";
            this.OkBtn.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.OkBtn.UseVisualStyleBackColor = true;
            this.OkBtn.Click += new System.EventHandler(this.OkClose);
            // 
            // CloseBtn
            // 
            this.CloseBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.CloseBtn.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.CloseBtn.ForeColor = System.Drawing.Color.DarkRed;
            this.CloseBtn.Image = global::TB.Cashier.Client.Shell.Properties.Resources.CloseSmall;
            this.CloseBtn.Location = new System.Drawing.Point(740, 0);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(231, 66);
            this.CloseBtn.TabIndex = 0;
            this.CloseBtn.Text = "ЗАКРЫТЬ";
            this.CloseBtn.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CancelClose);
            // 
            // KbdPanel
            // 
            this.KbdPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KbdPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.KbdPanel.Location = new System.Drawing.Point(0, 460);
            this.KbdPanel.Name = "KbdPanel";
            this.KbdPanel.Size = new System.Drawing.Size(971, 0);
            this.KbdPanel.TabIndex = 2;
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 526);
            this.ControlBox = false;
            this.Controls.Add(this.KbdPanel);
            this.Controls.Add(this.BtnPanel);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BaseForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BaseForm_FormClosing);
            this.Load += new System.EventHandler(this.BaseForm_Load);
            this.Resize += new System.EventHandler(this.BaseForm_Resize);
            this.BtnPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel BtnPanel;
        private ButtonControl OkBtn;
        private ButtonControl CloseBtn;
        private System.Windows.Forms.Panel KbdPanel;
    }
}