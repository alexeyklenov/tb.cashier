﻿namespace TB.Cashier.Client.Shell.SelectForms
{
    partial class PostponedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeListView = new BrightIdeasSoftware.TreeListView();
            this.NoColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.DateColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MaterialColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.PriceColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.QuantityColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.SumColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.DeleteBt = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.treeListView)).BeginInit();
            this.SuspendLayout();
            // 
            // treeListView
            // 
            this.treeListView.AllColumns.Add(this.NoColumn);
            this.treeListView.AllColumns.Add(this.DateColumn);
            this.treeListView.AllColumns.Add(this.MaterialColumn);
            this.treeListView.AllColumns.Add(this.PriceColumn);
            this.treeListView.AllColumns.Add(this.QuantityColumn);
            this.treeListView.AllColumns.Add(this.SumColumn);
            this.treeListView.CellEditUseWholeCell = false;
            this.treeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NoColumn,
            this.DateColumn,
            this.MaterialColumn,
            this.PriceColumn,
            this.QuantityColumn,
            this.SumColumn});
            this.treeListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeListView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.treeListView.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.treeListView.FullRowSelect = true;
            this.treeListView.GridLines = true;
            this.treeListView.HeaderWordWrap = true;
            this.treeListView.HighlightBackgroundColor = System.Drawing.Color.Empty;
            this.treeListView.HighlightForegroundColor = System.Drawing.Color.Empty;
            this.treeListView.Location = new System.Drawing.Point(0, -192);
            this.treeListView.Name = "treeListView";
            this.treeListView.RowHeight = 50;
            this.treeListView.ShowGroups = false;
            this.treeListView.Size = new System.Drawing.Size(984, 317);
            this.treeListView.TabIndex = 1;
            this.treeListView.UseCompatibleStateImageBehavior = false;
            this.treeListView.View = System.Windows.Forms.View.Details;
            this.treeListView.VirtualMode = true;
            this.treeListView.CellClick += new System.EventHandler<BrightIdeasSoftware.CellClickEventArgs>(this.treeListView_CellClick);
            this.treeListView.SelectedIndexChanged += new System.EventHandler(this.treeListView_SelectedIndexChanged);
            // 
            // NoColumn
            // 
            this.NoColumn.AspectName = "No";
            this.NoColumn.AutoCompleteEditor = false;
            this.NoColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.NoColumn.FillsFreeSpace = true;
            this.NoColumn.Hideable = false;
            this.NoColumn.Text = "Номер";
            this.NoColumn.WordWrap = true;
            // 
            // DateColumn
            // 
            this.DateColumn.AspectName = "Date";
            this.DateColumn.AspectToStringFormat = "";
            this.DateColumn.AutoCompleteEditor = false;
            this.DateColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.DateColumn.FillsFreeSpace = true;
            this.DateColumn.Text = "Дата";
            this.DateColumn.WordWrap = true;
            // 
            // MaterialColumn
            // 
            this.MaterialColumn.AspectName = "MaterialName";
            this.MaterialColumn.AutoCompleteEditor = false;
            this.MaterialColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MaterialColumn.FillsFreeSpace = true;
            this.MaterialColumn.Text = "Наименование";
            this.MaterialColumn.WordWrap = true;
            // 
            // PriceColumn
            // 
            this.PriceColumn.AspectName = "Price";
            this.PriceColumn.FillsFreeSpace = true;
            this.PriceColumn.Text = "Цена";
            this.PriceColumn.Width = 95;
            this.PriceColumn.WordWrap = true;
            // 
            // QuantityColumn
            // 
            this.QuantityColumn.AspectName = "Quantity";
            this.QuantityColumn.AutoCompleteEditor = false;
            this.QuantityColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.QuantityColumn.FillsFreeSpace = true;
            this.QuantityColumn.Text = "Количество";
            this.QuantityColumn.WordWrap = true;
            // 
            // SumColumn
            // 
            this.SumColumn.AspectName = "Sum";
            this.SumColumn.AspectToStringFormat = "{0:C}";
            this.SumColumn.AutoCompleteEditor = false;
            this.SumColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.SumColumn.FillsFreeSpace = true;
            this.SumColumn.Text = "Сумма";
            this.SumColumn.WordWrap = true;
            // 
            // DeleteBt
            // 
            this.DeleteBt.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.DeleteBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.DeleteSmall;
            this.DeleteBt.Location = new System.Drawing.Point(12, 2);
            this.DeleteBt.Name = "DeleteBt";
            this.DeleteBt.Size = new System.Drawing.Size(122, 53);
            this.DeleteBt.TabIndex = 3;
            this.DeleteBt.Text = "Удалить";
            this.DeleteBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.DeleteBt.UseVisualStyleBackColor = true;
            this.DeleteBt.Click += new System.EventHandler(this.DeleteBt_Click);
            // 
            // PostponedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 441);
            this.Controls.Add(this.DeleteBt);
            this.Controls.Add(this.treeListView);
            this.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "PostponedForm";
            this.NoKeyboard = true;
            this.Text = "Отложенные чеки за смену";
            this.Shown += new System.EventHandler(this.PostponedForm_Shown);
            this.Controls.SetChildIndex(this.treeListView, 0);
            this.Controls.SetChildIndex(this.DeleteBt, 0);
            ((System.ComponentModel.ISupportInitialize)(this.treeListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView treeListView;
        private BrightIdeasSoftware.OLVColumn NoColumn;
        private BrightIdeasSoftware.OLVColumn DateColumn;
        private BrightIdeasSoftware.OLVColumn SumColumn;
        private BrightIdeasSoftware.OLVColumn QuantityColumn;
        private BrightIdeasSoftware.OLVColumn MaterialColumn;
        private BrightIdeasSoftware.OLVColumn PriceColumn;
        private System.Windows.Forms.Button DeleteBt;



    }
}