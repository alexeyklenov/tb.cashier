﻿using System;
using System.Windows.Forms;

namespace TB.Cashier.Client.Shell.SelectForms
{
    public partial class SumForm : DefaultSelectionForm
    {

        public decimal Sum;
        public SumForm()
        {
            InitializeComponent();
        }

        private void SumTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Enter)
            {
                DialogResult = DialogResult.OK;
                CloseForm();
            }
        }

        private void SumForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            decimal.TryParse(SumTb.Text, out Sum);
        }
    }
}
