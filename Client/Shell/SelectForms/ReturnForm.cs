﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using TB.Cashier.Client.Shell.Wait;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Services.Infrastructure.Client;

namespace TB.Cashier.Client.Shell.SelectForms
{
    public partial class ReturnForm : DefaultSelectionForm
    {
        private readonly CabClientProxy<IChequeService> _chequeService;

        public long SelectedLineId;
        public long SelectedId;
        public List<long> SelectedLinesIds; 

        public ReturnForm()
        {
            InitializeComponent();
            _chequeService = new CabClientProxy<IChequeService>();
        }

        private void GetShifts()
        {
            using (new WaitDialog(@"Загрузка смен"))
            {
                shiftDtoBindingSource.DataSource = _chequeService.Get(s => s.GetAllShifts(null, null));
                GetCheques();
            }
        }

        public void GetCheques()
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                var shiftDto = shiftDtoBindingSource.Current as ShiftDto;
                if (shiftDto == null) 
                    return;

                treeListView.SetObjects(
                    _chequeService.Get(s => s.GetReadyForReturn(shiftDto.Id.GetValueOrDefault())));

                treeListView.CanExpandGetter = model =>
                {
                    var chequeDto = model as ChequeDto;
                    return chequeDto != null && chequeDto.Lines.Any();
                };
                treeListView.ChildrenGetter = model =>
                {
                    var chequeDto = model as ChequeDto;
                    return chequeDto != null ? chequeDto.Lines : null;
                };

             
            }
        }

        private void ReturnForm_Shown(object sender, EventArgs e)
        {

            treeListView.FormatRow += treeListView_FormatRow;

            GetShifts();

            shiftDtoBindingSource.CurrentItemChanged += shiftDtoBindingSource_CurrentItemChanged;
        }

        void treeListView_FormatRow(object sender, FormatRowEventArgs e)
        {
            var chequeLineDto = e.Model as ChequeLineDto;
            if (chequeLineDto != null)
                e.Item.BackColor = Color.LightGoldenrodYellow;
            else
                e.Item.Font = new Font(e.Item.Font, FontStyle.Bold);
        }

        void shiftDtoBindingSource_CurrentItemChanged(object sender, EventArgs e)
        {
            GetCheques();
        }

        private void SelectBt_Click(object sender, EventArgs e)
        {
            DialogResult = SelectedId > 0 ? DialogResult.OK : DialogResult.Cancel;
            Close();
        }

        private void treeListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            var chequeDto = treeListView.SelectedObject as ChequeDto;
            if (chequeDto != null)
            {

                SelectedId = chequeDto.Id.GetValueOrDefault();
                SelectedLineId = -1;
                SelectedLinesIds = chequeDto.Lines.Select(l => l.Id.GetValueOrDefault()).ToList();

            }
            else
            {
                var chequeLineDto = treeListView.SelectedObject as ChequeLineDto;
                if (chequeLineDto != null)
                {
                    SelectedLineId = chequeLineDto.Id.GetValueOrDefault();
                    SelectedId = chequeLineDto.ChequeId.GetValueOrDefault();
                    chequeDto = treeListView.GetParent(treeListView.SelectedObject) as ChequeDto;
                    if (chequeDto != null)
                        SelectedId = chequeDto.Id.GetValueOrDefault();

                }
            }
        }

        private void treeListView_CellClick(object sender, CellClickEventArgs e)
        {
            if (e.ClickCount > 1)
                SelectBt_Click(SelectBt, new EventArgs());
        }

        private void treeListView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                SelectBt_Click(SelectBt, new EventArgs());
        }
    }
}
