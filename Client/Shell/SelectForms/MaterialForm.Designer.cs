﻿namespace TB.Cashier.Client.Shell.SelectForms
{
    partial class MaterialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SelectBt = new System.Windows.Forms.Button();
            this.OurWarehouseColorTextLb = new System.Windows.Forms.Label();
            this.OurWarehouseColorLb = new System.Windows.Forms.Label();
            this.treeListView = new BrightIdeasSoftware.TreeListView();
            this.MaterialColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.PriceColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.QuantityColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.VendorColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.WarehouseColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.OnlyCurrentCb = new System.Windows.Forms.CheckBox();
            this.SearchTb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.treeListView)).BeginInit();
            this.SuspendLayout();
            // 
            // SelectBt
            // 
            this.SelectBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.CheckedExtraSmall;
            this.SelectBt.Location = new System.Drawing.Point(12, 5);
            this.SelectBt.Name = "SelectBt";
            this.SelectBt.Size = new System.Drawing.Size(50, 50);
            this.SelectBt.TabIndex = 4;
            this.SelectBt.UseVisualStyleBackColor = true;
            this.SelectBt.Click += new System.EventHandler(this.SelectBt_Click);
            // 
            // OurWarehouseColorTextLb
            // 
            this.OurWarehouseColorTextLb.AutoSize = true;
            this.OurWarehouseColorTextLb.Location = new System.Drawing.Point(157, 18);
            this.OurWarehouseColorTextLb.Name = "OurWarehouseColorTextLb";
            this.OurWarehouseColorTextLb.Size = new System.Drawing.Size(154, 25);
            this.OurWarehouseColorTextLb.TabIndex = 3;
            this.OurWarehouseColorTextLb.Text = "- Текущий склад";
            this.OurWarehouseColorTextLb.Visible = false;
            // 
            // OurWarehouseColorLb
            // 
            this.OurWarehouseColorLb.Location = new System.Drawing.Point(88, 10);
            this.OurWarehouseColorLb.Name = "OurWarehouseColorLb";
            this.OurWarehouseColorLb.Size = new System.Drawing.Size(63, 45);
            this.OurWarehouseColorLb.TabIndex = 2;
            this.OurWarehouseColorLb.Visible = false;
            // 
            // treeListView
            // 
            this.treeListView.AllColumns.Add(this.MaterialColumn);
            this.treeListView.AllColumns.Add(this.PriceColumn);
            this.treeListView.AllColumns.Add(this.QuantityColumn);
            this.treeListView.AllColumns.Add(this.VendorColumn);
            this.treeListView.AllColumns.Add(this.WarehouseColumn);
            this.treeListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeListView.CellEditUseWholeCell = false;
            this.treeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.MaterialColumn,
            this.PriceColumn,
            this.QuantityColumn,
            this.VendorColumn,
            this.WarehouseColumn});
            this.treeListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeListView.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.treeListView.FullRowSelect = true;
            this.treeListView.GridLines = true;
            this.treeListView.HeaderWordWrap = true;
            this.treeListView.HighlightBackgroundColor = System.Drawing.Color.Empty;
            this.treeListView.HighlightForegroundColor = System.Drawing.Color.Empty;
            this.treeListView.Location = new System.Drawing.Point(0, 61);
            this.treeListView.Name = "treeListView";
            this.treeListView.RowHeight = 50;
            this.treeListView.ShowGroups = false;
            this.treeListView.Size = new System.Drawing.Size(1008, 444);
            this.treeListView.TabIndex = 1;
            this.treeListView.UseCompatibleStateImageBehavior = false;
            this.treeListView.View = System.Windows.Forms.View.Details;
            this.treeListView.VirtualMode = true;
            this.treeListView.CellClick += new System.EventHandler<BrightIdeasSoftware.CellClickEventArgs>(this.treeListView_CellClick);
            this.treeListView.SelectedIndexChanged += new System.EventHandler(this.treeListView_SelectedIndexChanged);
            this.treeListView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.treeListView_KeyPress);
            // 
            // MaterialColumn
            // 
            this.MaterialColumn.AspectName = "Name";
            this.MaterialColumn.AutoCompleteEditor = false;
            this.MaterialColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MaterialColumn.FillsFreeSpace = true;
            this.MaterialColumn.Text = "Наименование";
            this.MaterialColumn.WordWrap = true;
            // 
            // PriceColumn
            // 
            this.PriceColumn.AspectName = "Price";
            this.PriceColumn.AspectToStringFormat = "{0:C}";
            this.PriceColumn.AutoCompleteEditor = false;
            this.PriceColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.PriceColumn.FillsFreeSpace = true;
            this.PriceColumn.Text = "Цена";
            // 
            // QuantityColumn
            // 
            this.QuantityColumn.AspectName = "Quantity";
            this.QuantityColumn.AutoCompleteEditor = false;
            this.QuantityColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.QuantityColumn.Text = "Кол-во";
            // 
            // VendorColumn
            // 
            this.VendorColumn.AspectName = "Vendor";
            this.VendorColumn.AutoCompleteEditor = false;
            this.VendorColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.VendorColumn.FillsFreeSpace = true;
            this.VendorColumn.Text = "Производитель";
            this.VendorColumn.WordWrap = true;
            // 
            // WarehouseColumn
            // 
            this.WarehouseColumn.AspectName = "Warehouse";
            this.WarehouseColumn.AutoCompleteEditor = false;
            this.WarehouseColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.WarehouseColumn.FillsFreeSpace = true;
            this.WarehouseColumn.Text = "Склад";
            this.WarehouseColumn.WordWrap = true;
            // 
            // OnlyCurrentCb
            // 
            this.OnlyCurrentCb.AutoSize = true;
            this.OnlyCurrentCb.Checked = true;
            this.OnlyCurrentCb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OnlyCurrentCb.Font = new System.Drawing.Font("Segoe UI", 12.25F);
            this.OnlyCurrentCb.Location = new System.Drawing.Point(329, 21);
            this.OnlyCurrentCb.Name = "OnlyCurrentCb";
            this.OnlyCurrentCb.Size = new System.Drawing.Size(265, 27);
            this.OnlyCurrentCb.TabIndex = 5;
            this.OnlyCurrentCb.Text = "В наличии на текущем складе";
            this.OnlyCurrentCb.UseVisualStyleBackColor = true;
            this.OnlyCurrentCb.CheckedChanged += new System.EventHandler(this.OnlyCurrentCb_CheckedChanged);
            // 
            // SearchTb
            // 
            this.SearchTb.Location = new System.Drawing.Point(697, 15);
            this.SearchTb.Name = "SearchTb";
            this.SearchTb.Size = new System.Drawing.Size(301, 33);
            this.SearchTb.TabIndex = 6;
            this.SearchTb.TextChanged += new System.EventHandler(this.SearchTb_TextChanged);
            this.SearchTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SearchTb_KeyPress);
            // 
            // label1
            // 
            this.label1.Image = global::TB.Cashier.Client.Shell.Properties.Resources.SearchExtraSmall;
            this.label1.Location = new System.Drawing.Point(661, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 30);
            this.label1.TabIndex = 7;
            // 
            // MaterialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 573);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SearchTb);
            this.Controls.Add(this.OnlyCurrentCb);
            this.Controls.Add(this.SelectBt);
            this.Controls.Add(this.OurWarehouseColorTextLb);
            this.Controls.Add(this.OurWarehouseColorLb);
            this.Controls.Add(this.treeListView);
            this.DoubleBuffered = false;
            this.Name = "MaterialForm";
            this.NoKeyboard = true;
            this.Text = "Номенклатура";
            this.Shown += new System.EventHandler(this.MaterialForm_Shown);
            this.Controls.SetChildIndex(this.treeListView, 0);
            this.Controls.SetChildIndex(this.OurWarehouseColorLb, 0);
            this.Controls.SetChildIndex(this.OurWarehouseColorTextLb, 0);
            this.Controls.SetChildIndex(this.SelectBt, 0);
            this.Controls.SetChildIndex(this.OnlyCurrentCb, 0);
            this.Controls.SetChildIndex(this.SearchTb, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.treeListView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BrightIdeasSoftware.TreeListView treeListView;
        private BrightIdeasSoftware.OLVColumn MaterialColumn;
        private BrightIdeasSoftware.OLVColumn PriceColumn;
        private BrightIdeasSoftware.OLVColumn QuantityColumn;
        private BrightIdeasSoftware.OLVColumn WarehouseColumn;
        private System.Windows.Forms.Label OurWarehouseColorLb;
        private System.Windows.Forms.Label OurWarehouseColorTextLb;
        private System.Windows.Forms.Button SelectBt;
        private System.Windows.Forms.CheckBox OnlyCurrentCb;
        private System.Windows.Forms.TextBox SearchTb;
        private System.Windows.Forms.Label label1;
        private BrightIdeasSoftware.OLVColumn VendorColumn;
    }
}