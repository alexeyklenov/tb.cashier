﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;
using TB.Cashier.Client.Shell.Wait;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Services.Infrastructure.Client;

namespace TB.Cashier.Client.Shell.SelectForms
{
    public partial class PostponedForm : DefaultSelectionForm
    {
        private long _selectedId;
        public long SelectedId
        {
            get { return _selectedId; }
            set
            {
                _selectedId = value;
                OverrideDialogResult = _selectedId > 0 ? DialogResult.OK : DialogResult.Cancel;
            }
        }

        public PostponedForm()
        {
            InitializeComponent();
        }

        private void PostponedForm_Shown(object sender, EventArgs e)
        {
            treeListView.FormatRow += treeListView_FormatRow;

            GetData();
        }

        private void GetData()
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                treeListView.SetObjects(
                    new CabClientProxy<IChequeService>().Get(s => s.GetPostponedByCurrentShift()));

                treeListView.CanExpandGetter = model =>
                {
                    var chequeDto = model as ChequeDto;
                    return chequeDto != null && chequeDto.Lines.Any();
                };
                treeListView.ChildrenGetter = model =>
                {
                    var chequeDto = model as ChequeDto;
                    return chequeDto != null ? chequeDto.Lines : null;
                };
            }
        }

        private void DeleteSelectedItem()
        {
            if (SelectedId <= 0)
                return;
            using (new WaitDialog(@"Удаление данных"))
            {
                new CabClientProxy<IChequeService>().Execute(s => s.DeleteChequeById(SelectedId));
                GetData();
            }
        }

        private void DeleteBt_Click(object sender, EventArgs e)
        {
            DeleteSelectedItem();
            SelectedId = 0;
        }

        private void treeListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            var chequeDto = treeListView.SelectedObject as ChequeDto;
            if (chequeDto != null)
                SelectedId = chequeDto.Id.GetValueOrDefault();
            else
            {
                var chequeLineDto  = treeListView.SelectedObject as ChequeLineDto;
                if (chequeLineDto != null)
                    SelectedId = chequeLineDto.ChequeId.GetValueOrDefault();
            }
        }

        void treeListView_FormatRow(object sender, FormatRowEventArgs e)
        {
            var chequeLineDto = e.Model as ChequeLineDto;
            if (chequeLineDto != null)
                e.Item.BackColor = Color.LightGoldenrodYellow;
            else
                e.Item.Font = new Font(e.Item.Font, FontStyle.Bold);

        }

        private void treeListView_CellClick(object sender, CellClickEventArgs e)
        {
            if(e.ClickCount > 1)
                OkClose();
        }
    }
}
