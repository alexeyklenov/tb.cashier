﻿using TB.Cashier.Client.Shell.Controls;

namespace TB.Cashier.Client.Shell.SelectForms
{
    partial class SumForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SumTb = new TB.Cashier.Client.Shell.Controls.NumericTextBox();
            this.SuspendLayout();
            // 
            // SumTb
            // 
            this.SumTb.EmptyIsZero = false;
            this.SumTb.Location = new System.Drawing.Point(12, 16);
            this.SumTb.Name = "SumTb";
            this.SumTb.Size = new System.Drawing.Size(360, 33);
            this.SumTb.TabIndex = 0;
            this.SumTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SumTb_KeyPress);
            // 
            // SumForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 121);
            this.Controls.Add(this.SumTb);
            this.Name = "SumForm";
            this.Text = "Ввод значения";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SumForm_FormClosed);
            this.Controls.SetChildIndex(this.SumTb, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NumericTextBox SumTb;
    }
}