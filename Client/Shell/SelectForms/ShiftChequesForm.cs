﻿
using System.Drawing;
using System.Linq;
using TB.Cashier.Client.Shell.Wait;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Services.Infrastructure.Client;

namespace TB.Cashier.Client.Shell.SelectForms
{
    public partial class ShiftChequesForm : DefaultSelectionForm
    {

        private Color _postponedColor = Color.LightGoldenrodYellow;
        private Color _returnColor = Color.Brown;
        private Color _closedColor = Color.DarkSeaGreen;

        public ShiftChequesForm()
        {
            InitializeComponent();
        }

        private void ShiftChequesForm_Shown(object sender, System.EventArgs e)
        {
            treeListView.FormatRow +=treeListView_FormatRow;
            GetCheques();
        }

        private void GetCheques()
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                treeListView.SetObjects(
                    new CabClientProxy<IChequeService>().Get(s => s.GetAllChequeByCurrentShift()));

                treeListView.CanExpandGetter = model =>
                {
                    var chequeDto = model as ChequeDto;
                    return chequeDto != null && chequeDto.Lines.Any();
                };
                treeListView.ChildrenGetter = model =>
                {
                    var chequeDto = model as ChequeDto;
                    return chequeDto != null ? chequeDto.Lines : null;
                };
            }
        }

        private void treeListView_FormatRow(object sender, BrightIdeasSoftware.FormatRowEventArgs e)
        {
            var chequeDto = e.Model as ChequeDto;
            if (chequeDto == null) 
                return;

            if (chequeDto.IsPostponed)
                e.Item.BackColor = _postponedColor;
            else if (chequeDto.IsReturn)
                e.Item.BackColor = _returnColor;
            else if (chequeDto.IsClosed)
                e.Item.BackColor = _closedColor;
        }


    }
}
