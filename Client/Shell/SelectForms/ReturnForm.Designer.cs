﻿namespace TB.Cashier.Client.Shell.SelectForms
{
    partial class ReturnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SelectBt = new System.Windows.Forms.Button();
            this.treeListView = new BrightIdeasSoftware.TreeListView();
            this.NoColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.DateColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MaterialColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.PriceColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.QuantityColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.SumColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ShiftCb = new System.Windows.Forms.ComboBox();
            this.shiftDtoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ShiftLb = new System.Windows.Forms.Label();
            this.VendorColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.treeListView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shiftDtoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // SelectBt
            // 
            this.SelectBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.CheckedExtraSmall;
            this.SelectBt.Location = new System.Drawing.Point(12, 5);
            this.SelectBt.Name = "SelectBt";
            this.SelectBt.Size = new System.Drawing.Size(50, 50);
            this.SelectBt.TabIndex = 3;
            this.SelectBt.UseVisualStyleBackColor = true;
            this.SelectBt.Click += new System.EventHandler(this.SelectBt_Click);
            // 
            // treeListView
            // 
            this.treeListView.AllColumns.Add(this.NoColumn);
            this.treeListView.AllColumns.Add(this.DateColumn);
            this.treeListView.AllColumns.Add(this.MaterialColumn);
            this.treeListView.AllColumns.Add(this.VendorColumn);
            this.treeListView.AllColumns.Add(this.PriceColumn);
            this.treeListView.AllColumns.Add(this.QuantityColumn);
            this.treeListView.AllColumns.Add(this.SumColumn);
            this.treeListView.CellEditUseWholeCell = false;
            this.treeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NoColumn,
            this.DateColumn,
            this.MaterialColumn,
            this.VendorColumn,
            this.PriceColumn,
            this.QuantityColumn,
            this.SumColumn});
            this.treeListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeListView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.treeListView.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.treeListView.FullRowSelect = true;
            this.treeListView.GridLines = true;
            this.treeListView.HeaderWordWrap = true;
            this.treeListView.HighlightBackgroundColor = System.Drawing.Color.Empty;
            this.treeListView.HighlightForegroundColor = System.Drawing.Color.Empty;
            this.treeListView.Location = new System.Drawing.Point(0, -187);
            this.treeListView.Name = "treeListView";
            this.treeListView.RowHeight = 50;
            this.treeListView.ShowGroups = false;
            this.treeListView.Size = new System.Drawing.Size(984, 312);
            this.treeListView.TabIndex = 0;
            this.treeListView.UseCompatibleStateImageBehavior = false;
            this.treeListView.View = System.Windows.Forms.View.Details;
            this.treeListView.VirtualMode = true;
            this.treeListView.CellClick += new System.EventHandler<BrightIdeasSoftware.CellClickEventArgs>(this.treeListView_CellClick);
            this.treeListView.SelectedIndexChanged += new System.EventHandler(this.treeListView_SelectedIndexChanged);
            this.treeListView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.treeListView_KeyPress);
            // 
            // NoColumn
            // 
            this.NoColumn.AspectName = "No";
            this.NoColumn.AutoCompleteEditor = false;
            this.NoColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.NoColumn.FillsFreeSpace = true;
            this.NoColumn.Text = "Номер";
            this.NoColumn.WordWrap = true;
            // 
            // DateColumn
            // 
            this.DateColumn.AspectName = "Date";
            this.DateColumn.AutoCompleteEditor = false;
            this.DateColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.DateColumn.FillsFreeSpace = true;
            this.DateColumn.Text = "Дата";
            this.DateColumn.WordWrap = true;
            // 
            // MaterialColumn
            // 
            this.MaterialColumn.AspectName = "MaterialName";
            this.MaterialColumn.AutoCompleteEditor = false;
            this.MaterialColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MaterialColumn.FillsFreeSpace = true;
            this.MaterialColumn.Text = "Наименование";
            this.MaterialColumn.WordWrap = true;
            // 
            // PriceColumn
            // 
            this.PriceColumn.AspectName = "Price";
            this.PriceColumn.AspectToStringFormat = "{0:C}";
            this.PriceColumn.AutoCompleteEditor = false;
            this.PriceColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.PriceColumn.FillsFreeSpace = true;
            this.PriceColumn.Text = "Цена";
            this.PriceColumn.WordWrap = true;
            // 
            // QuantityColumn
            // 
            this.QuantityColumn.AspectName = "Quantity";
            this.QuantityColumn.AutoCompleteEditor = false;
            this.QuantityColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.QuantityColumn.Text = "Кол-во";
            // 
            // SumColumn
            // 
            this.SumColumn.AspectName = "Sum";
            this.SumColumn.AspectToStringFormat = "{0:C}";
            this.SumColumn.AutoCompleteEditor = false;
            this.SumColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.SumColumn.FillsFreeSpace = true;
            this.SumColumn.Text = "Сумма";
            this.SumColumn.WordWrap = true;
            // 
            // ShiftCb
            // 
            this.ShiftCb.DataSource = this.shiftDtoBindingSource;
            this.ShiftCb.DisplayMember = "ShiftDateLong";
            this.ShiftCb.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.ShiftCb.FormattingEnabled = true;
            this.ShiftCb.Location = new System.Drawing.Point(172, 18);
            this.ShiftCb.Name = "ShiftCb";
            this.ShiftCb.Size = new System.Drawing.Size(343, 29);
            this.ShiftCb.TabIndex = 4;
            // 
            // shiftDtoBindingSource
            // 
            this.shiftDtoBindingSource.DataSource = typeof(TB.Cashier.Server.Services.Dto.ShiftDto);
            // 
            // ShiftLb
            // 
            this.ShiftLb.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ShiftLb.Image = global::TB.Cashier.Client.Shell.Properties.Resources.ShiftExtraSmall;
            this.ShiftLb.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.ShiftLb.Location = new System.Drawing.Point(122, 9);
            this.ShiftLb.Name = "ShiftLb";
            this.ShiftLb.Size = new System.Drawing.Size(44, 41);
            this.ShiftLb.TabIndex = 5;
            this.ShiftLb.Text = "Смена:";
            this.ShiftLb.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // VendorColumn
            // 
            this.VendorColumn.AspectName = "VendorName";
            this.VendorColumn.AutoCompleteEditor = false;
            this.VendorColumn.AutoCompleteEditorMode = System.Windows.Forms.AutoCompleteMode.None;
            this.VendorColumn.FillsFreeSpace = true;
            this.VendorColumn.Text = "Производитель";
            this.VendorColumn.WordWrap = true;
            // 
            // ReturnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 441);
            this.Controls.Add(this.ShiftLb);
            this.Controls.Add(this.ShiftCb);
            this.Controls.Add(this.SelectBt);
            this.Controls.Add(this.treeListView);
            this.Name = "ReturnForm";
            this.NoKeyboard = true;
            this.Text = "Выбор чека для возврата";
            this.Shown += new System.EventHandler(this.ReturnForm_Shown);
            this.Controls.SetChildIndex(this.treeListView, 0);
            this.Controls.SetChildIndex(this.SelectBt, 0);
            this.Controls.SetChildIndex(this.ShiftCb, 0);
            this.Controls.SetChildIndex(this.ShiftLb, 0);
            ((System.ComponentModel.ISupportInitialize)(this.treeListView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shiftDtoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView treeListView;
        private System.Windows.Forms.Button SelectBt;
        private System.Windows.Forms.ComboBox ShiftCb;
        private System.Windows.Forms.BindingSource shiftDtoBindingSource;
        private System.Windows.Forms.Label ShiftLb;
        private BrightIdeasSoftware.OLVColumn NoColumn;
        private BrightIdeasSoftware.OLVColumn DateColumn;
        private BrightIdeasSoftware.OLVColumn MaterialColumn;
        private BrightIdeasSoftware.OLVColumn PriceColumn;
        private BrightIdeasSoftware.OLVColumn QuantityColumn;
        private BrightIdeasSoftware.OLVColumn SumColumn;
        private BrightIdeasSoftware.OLVColumn VendorColumn;
    }
}