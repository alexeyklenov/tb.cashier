﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TB.Cashier.Client.Shell.Wait;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Services.Infrastructure.Client;

namespace TB.Cashier.Client.Shell.SelectForms
{
    public partial class MaterialForm : DefaultSelectionForm
    {
        private readonly Color _ourWarehouseColor = Color.DarkSeaGreen;
        private List<MaterialDto> _data;

        private List<MaterialDto> _visibleData;

        public string SelectedOfferCode;
        public MaterialDto SelectedOffer;

        public MaterialForm(string searchPredicate)
        {
            InitializeComponent();
            OurWarehouseColorLb.BackColor = _ourWarehouseColor;
            SearchTb.Text = searchPredicate;
        }

        private void treeListView_FormatRow(object sender, BrightIdeasSoftware.FormatRowEventArgs e)
        {
            var materialDto = e.Model as MaterialDto;
            if (materialDto == null)
                return;

            if (materialDto.IsHasOnCurrentWarehouse && !OnlyCurrentCb.Checked)
                e.Item.BackColor = _ourWarehouseColor;

        }

        private void MaterialForm_Shown(object sender, EventArgs e)
        {
            treeListView.FormatRow += treeListView_FormatRow;
            GetMaterials(SearchTb.Text);
        }

        private void GetMaterials(string searchPredicate = null)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                if (searchPredicate != null)
                    _data = new CabClientProxy<IMaterialService>().Get(s => s.SearchMaterialsByName(searchPredicate));
                else if (_data == null)
                    _data = new CabClientProxy<IMaterialService>().Get(s => s.GetAllMaterialsByFolders());
                
                treeListView.CanExpandGetter = model =>
                {
                    var materialDto = model as MaterialDto;
                    return materialDto != null &&
                           ((materialDto.IsFolder && materialDto.FolderElements.Any()) ||
                            (materialDto.IsCatalogElement && materialDto.Offers.Any()));
                };
                treeListView.ChildrenGetter = model =>
                {
                    var materialDto = model as MaterialDto;
                    return materialDto != null
                        ? (materialDto.IsFolder
                            ? materialDto.FolderElements
                            : materialDto.IsCatalogElement ? materialDto.Offers : null)
                        : null;
                };

                ShowData();
            }
        }

        private void ShowData(bool refresh = true)
        {
            if (OnlyCurrentCb.Checked)
            {
                _visibleData =
                    _data.Where(
                        x => x.IsFolder || x.IsCatalogElement || (x.IsOffer && x.IsHasOnCurrentWarehouse)).ToList();
                _visibleData.ForEach(RemoveNotCurrentWarehouse);
            }
            else
                _visibleData = _data;
            if (refresh)
                treeListView.SetObjects(_visibleData);
        }

        private void RemoveNotCurrentWarehouse(MaterialDto material)
        {
            if (material.IsFolder)
                material.FolderElements.ForEach(RemoveNotCurrentWarehouse);
            else if (material.IsCatalogElement)
                material.Offers.RemoveAll(o => !o.IsHasOnCurrentWarehouse);
        }

        private void FilterOffers(MaterialDto material)
        {
            if(material.IsFolder)
                material.FolderElements.ForEach(FilterOffers);
            else if (material.IsCatalogElement)
                material.Offers.RemoveAll(o => !o.Name.Contains(SearchTb.Text));
        }

        private void SelectBt_Click(object sender, EventArgs e)
        {
            DialogResult = SelectedOfferCode != string.Empty ? DialogResult.OK : DialogResult.Cancel;
            CloseForm();
        }

        private void treeListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            var materialDto = treeListView.SelectedObject as MaterialDto;
            SelectedOffer = materialDto;
            SelectedOfferCode = materialDto != null && materialDto.IsOffer ? materialDto.Code : string.Empty;
        }

        private void treeListView_CellClick(object sender, BrightIdeasSoftware.CellClickEventArgs e)
        {
            if (e.ClickCount > 1)
                SelectBt_Click(SelectBt, new EventArgs());
        }

        private void treeListView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Enter)
                SelectBt_Click(SelectBt, new EventArgs());
        }

        private void OnlyCurrentCb_CheckedChanged(object sender, EventArgs e)
        {
           // SearchTb_TextChanged(null,null);
            ShowData();
            //OurWarehouseColorLb.Visible = OurWarehouseColorTextLb.Visible = !OnlyCurrentCb.Checked;
        }

        private void SearchTb_TextChanged(object sender, EventArgs e)
        {
           /* if (SearchTb.Text != string.Empty)
            {
                var regExpForSearch =
                    new Regex(SearchTb.Text.Split(new[] {' '})
                        .Aggregate("^", (current, str) => current + ("(?=.*" + str + ")")),RegexOptions.IgnoreCase);

                ShowData(false);
                _visibleData =
                    _visibleData.Where(
                        x => x.IsFolder || x.IsCatalogElement || (x.IsOffer && regExpForSearch.IsMatch(x.Name) )).ToList();
                _visibleData.ForEach(FilterOffers);
                treeListView.SetObjects(_visibleData);
            }
            else
                ShowData();*/
        }

        private void SearchTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Enter)
            {
                GetMaterials(SearchTb.Text.Replace(" ", "%"));
            }
        }
    }
}
