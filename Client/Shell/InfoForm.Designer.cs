﻿namespace TB.Cashier.Client.Shell
{
    partial class InfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextLb = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextLb
            // 
            this.TextLb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextLb.Font = new System.Drawing.Font("Segoe UI", 24F);
            this.TextLb.Location = new System.Drawing.Point(0, 0);
            this.TextLb.Name = "TextLb";
            this.TextLb.Size = new System.Drawing.Size(384, 0);
            this.TextLb.TabIndex = 0;
            this.TextLb.Text = "label1";
            this.TextLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 152);
            this.Controls.Add(this.TextLb);
            this.Margin = new System.Windows.Forms.Padding(11, 12, 11, 12);
            this.Name = "InfoForm";
            this.NoKeyboard = true;
            this.OnlyCloseButton = true;
            this.Text = "InfoForm";
            this.Controls.SetChildIndex(this.TextLb, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label TextLb;
    }
}