﻿namespace TB.Cashier.Client.Shell
{
    partial class AuthenticationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthenticationForm));
            this.UserCb = new System.Windows.Forms.ComboBox();
            this.UserLb = new System.Windows.Forms.Label();
            this.PwdTb = new System.Windows.Forms.TextBox();
            this.PwdLb = new System.Windows.Forms.Label();
            this.OkBt = new System.Windows.Forms.Button();
            this.ErrorLb = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UserCb
            // 
            this.UserCb.FormattingEnabled = true;
            this.UserCb.Location = new System.Drawing.Point(65, 24);
            this.UserCb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UserCb.Name = "UserCb";
            this.UserCb.Size = new System.Drawing.Size(310, 33);
            this.UserCb.TabIndex = 0;
            // 
            // UserLb
            // 
            this.UserLb.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.UserLb.Image = global::TB.Cashier.Client.Shell.Properties.Resources.UserSmall;
            this.UserLb.Location = new System.Drawing.Point(13, 17);
            this.UserLb.Name = "UserLb";
            this.UserLb.Size = new System.Drawing.Size(46, 40);
            this.UserLb.TabIndex = 1;
            this.UserLb.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // PwdTb
            // 
            this.PwdTb.Location = new System.Drawing.Point(65, 74);
            this.PwdTb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PwdTb.Name = "PwdTb";
            this.PwdTb.PasswordChar = '*';
            this.PwdTb.Size = new System.Drawing.Size(310, 32);
            this.PwdTb.TabIndex = 2;
            this.PwdTb.UseSystemPasswordChar = true;
            this.PwdTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PwdTb_KeyPress);
            // 
            // PwdLb
            // 
            this.PwdLb.Image = global::TB.Cashier.Client.Shell.Properties.Resources.PasswordSmall;
            this.PwdLb.Location = new System.Drawing.Point(13, 66);
            this.PwdLb.Name = "PwdLb";
            this.PwdLb.Size = new System.Drawing.Size(46, 40);
            this.PwdLb.TabIndex = 3;
            // 
            // OkBt
            // 
            this.OkBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.OkBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.LogInSmall;
            this.OkBt.Location = new System.Drawing.Point(390, 22);
            this.OkBt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OkBt.Name = "OkBt";
            this.OkBt.Size = new System.Drawing.Size(77, 85);
            this.OkBt.TabIndex = 4;
            this.OkBt.Text = "Войти";
            this.OkBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.OkBt.UseVisualStyleBackColor = true;
            this.OkBt.Click += new System.EventHandler(this.OkBt_Click);
            // 
            // ErrorLb
            // 
            this.ErrorLb.AutoSize = true;
            this.ErrorLb.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ErrorLb.ForeColor = System.Drawing.Color.Red;
            this.ErrorLb.Location = new System.Drawing.Point(23, 117);
            this.ErrorLb.Name = "ErrorLb";
            this.ErrorLb.Size = new System.Drawing.Size(0, 15);
            this.ErrorLb.TabIndex = 5;
            // 
            // AuthenticationForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(480, 133);
            this.Controls.Add(this.ErrorLb);
            this.Controls.Add(this.OkBt);
            this.Controls.Add(this.PwdLb);
            this.Controls.Add(this.PwdTb);
            this.Controls.Add(this.UserLb);
            this.Controls.Add(this.UserCb);
            this.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthenticationForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторизация";
            this.Load += new System.EventHandler(this.AuthenticationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox UserCb;
        private System.Windows.Forms.Label UserLb;
        private System.Windows.Forms.TextBox PwdTb;
        private System.Windows.Forms.Label PwdLb;
        private System.Windows.Forms.Button OkBt;
        private System.Windows.Forms.Label ErrorLb;
    }
}