﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using Cashier.Equipment.Interface;
using Microsoft.Practices.Unity;
using TB.Cashier.Client.Shell.FinishForms;
using TB.Cashier.Client.Shell.SelectForms;
using TB.Cashier.Client.Shell.Wait;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Cashier.Server.Services.Interfaces.Faults;
using TB.Services.Infrastructure.Client;
using Utils;

namespace TB.Cashier.Client.Shell
{
    public partial class MainFormV2_1 : Form, IMainForm
    {
        #region static resize methods

        private const int DefaultMarginOnX = 15;
        private const int DefaultMarginOnY = 5;

        private const int ImageBtnHeight = 85;
        private const int ImageBtnWidth = 90;

        private static int HeightByPercent(int percent, Control owner)
        {
            return (int) (owner.ClientSize.Height/100f*percent);
        }

        private static int WidthByPercent(int percent, Control owner)
        {
            return (int) (owner.ClientSize.Width/100f*percent);
        }

        private static int MiddleTopY(Control inner, Control owner, bool noRelation = false)
        {
            return (int)(owner.ClientSize.Height / 2f - inner.ClientSize.Height / 2f + (noRelation ? owner.Location.Y : 0));
        }

        private static int MiddleLeftX(Control inner, Control owner, bool noRelation = false)
        {
            return (int) (owner.ClientSize.Width/2f - inner.ClientSize.Width/2f + (noRelation ? owner.Location.X : 0));
        }

        private static void PushToLeft(Control pushable, Control owner, Control before = null, bool noRelation = false, int? yPos = null)
        {

            pushable.Location =
                new Point(
                    DefaultMarginOnX + (before == null ? 0 : before.Location.X + before.Width) +
                    (noRelation ? owner.Location.X : 0),
                    yPos == null ? MiddleTopY(pushable, owner, noRelation) : yPos.GetValueOrDefault());
        }

        private static void PushToRight(Control pushable, Control owner, Control before = null, bool noRelation = false)
        {

            pushable.Location =
                new Point(
                    (before == null ? 
                    owner.ClientSize.Width - pushable.ClientSize.Width - DefaultMarginOnX : before.Location.X - pushable.Width - DefaultMarginOnX)  + (noRelation ? owner.Location.X : 0),
                    MiddleTopY(pushable, owner, noRelation));
        }

        #endregion

        public ChequeDto CurrentCheque { get; set; }
        public ShiftDto CurrentShift { get; set; }

        public bool ActiveShiftIsAbsent { get; set; }

        public CabClientProxy<IChequeService> ChequeService { get; set; }
        public CabClientProxy<IMaterialService> MaterialService { get; set; }

        public int CurrentSelectedIndex = 0;

        private string _inputCode = string.Empty;

        public string InputCode
        {
            get { return _inputCode; }
            set
            {
                _inputCode = value;
                CodeTb.Text = value;
            }
        }


        public MainFormV2_1()
        {
            InitializeComponent();
            ChequeService = new CabClientProxy<IChequeService>();
            MaterialService = new CabClientProxy<IMaterialService>();

            ClientProxyBase.OnFault += CabClientProxyOnOnFault;

            #region Getting Data

            SetActiveShift();

            #endregion

            #region Set Scanner

            UnityContext.Container.Resolve<IBarCodeScanner>().BarCodeReaded += MainForm_BarCodeReaded;

            #endregion
        }

        private void CabClientProxyOnOnFault(object sender, Exception exception)
        {
            if (exception is FaultException<ShiftFault>)
            {
                WaitService.HideWaitDialog();
                ShowError((exception as FaultException<ShiftFault>).Detail.Message);
                EnableControl(this, false);

                ActiveShiftIsAbsent = true;
                ServiceBt.Enabled = true;
            }
            else
            {
                WaitService.HideWaitDialog();
                ShowError(exception.Message);
            }
        }

        void MainForm_BarCodeReaded(string barCode)
        {
            InputCode = barCode.Trim();
            SendCode();
            InputCode = string.Empty;
        }

        public void SetActiveShift()
        {

            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                CurrentShift = null;
                shiftBindingSource.DataSource = typeof (ShiftDto);
                CurrentCheque = null;
                chequeBindingSource.DataSource = typeof (ChequeDto);

                CurrentShift = ChequeService.Get(s => s.GetCurrentShift());
                if (CurrentShift != null)
                {
                    ActiveShiftIsAbsent = false;

                    shiftBindingSource.DataSource = CurrentShift;
                    RefreshCurrentCheque();

                    EnableControl(this, true);
                }
            }

        }

        public void OpenShift()
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                CurrentShift = ChequeService.Get(s => s.OpenShift());
            }
            if (CurrentShift != null)
            {
                if (!UnityContext.Container.Resolve<IChequePrinter>().OpenShift(CurrentShift))
                    ShowError(UnityContext.Container.Resolve<IChequePrinter>().LastError());
                SetActiveShift();
            }
        }

        static void EnableControl(Control control, bool state)
        {
            foreach (Control cntrl in control.Controls)
            {
                if (cntrl.Controls.Count > 0)
                    EnableControl(cntrl, state);
                else
                    cntrl.Enabled = state;
            }
        }

        public void SizePanels()
        {
            #region Size Panels

            TopPanel.Height = HeightByPercent(80, this);
            BottomPanel.Height = HeightByPercent(20, this);

            LeftPanel.Width = WidthByPercent(63, TopPanel);

            RightPanel.Width = WidthByPercent(37, TopPanel);

            InfoPanel.Height = HeightByPercent(10, LeftPanel);
            GridPanel.Height = HeightByPercent(55, LeftPanel);
            //CurrentPanel.Height = HeightByPercent(7, LeftPanel);
            CodePanel.Height = HeightByPercent(10, LeftPanel);
            SummaryPanel.Height = HeightByPercent(10, LeftPanel);
            FinishChequeBt.Height = HeightByPercent(15, LeftPanel);

            KeyboardCtrl.Height = HeightByPercent(50, RightPanel);
            DiscountPanel.Height = HeightByPercent(50, RightPanel);
            
            #endregion

            #region Size InfoPanel items

            InfoLeftPanel.Width = WidthByPercent(20, InfoPanel);
            InfoRightPanel.Width = WidthByPercent(30, InfoPanel);

            ChequeNoTextLb.Height = ChequeNoLb.Height = HeightByPercent(50, InfoLeftPanel);
            ModeTextLb.Height = HeightByPercent(50, InfoCenterPanel);
            ShiftTextLb.Height = ShiftLb.Height = HeightByPercent(50, InfoRightPanel);

            #endregion

            #region Size Current Panel

            CurrentPosLb.Width = WidthByPercent(80, CurrentPanel);

            #endregion

            #region Size CodePanel items

            CodeTb.Width = NameTb.Width = WidthByPercent(30, CodePanel);

            PushToLeft(CodeLb, CodePanel);
            PushToLeft(CodeTb, CodePanel, CodeLb);

            PushToRight(NameTb, CodePanel);
            PushToRight(NameBt, CodePanel, NameTb);


            #endregion

            #region Size RightPanel Items

            /*var controls = RightPanel.Controls.Cast<Control>().OrderBy(c => c.TabIndex).ToList();
            var topY = RightPanel.Height - DefaultMarginOnY;
            var buttonsBlockHeight = 0;

            foreach (var control in controls)
            {
                control.Width = RightPanel.Width - DefaultMarginOnX*2;
                if (control.GetType() == typeof (Button))
                    control.Height = HeightByPercent(6, RightPanel);

                if (control != KeyboardCtrl)
                    buttonsBlockHeight += control.Height + DefaultMarginOnY;

                control.Location = new Point(MiddleLeftX(control, RightPanel), topY - control.Height);
                topY -= control.Height + DefaultMarginOnY;
            }

            KeyboardCtrl.Width = RightPanel.Width - DefaultMarginOnX*2;
            KeyboardCtrl.Height = RightPanel.Height - buttonsBlockHeight - DefaultMarginOnY;
            KeyboardCtrl.Location = new Point(MiddleLeftX(KeyboardCtrl, RightPanel), 0);*/

            #endregion


            var btnPanelControls = BtnPanel.Controls.Cast<Button>().OrderByDescending(c => c.TabIndex).ToList();
            //var controlWidth = (BtnPanel.ClientSize.Width - (btnPanelControls.Count + 1) * DefaultMarginOnX) / btnPanelControls.Count;

            #region Size DiscountPanel Items

            var discountPanelControls = DiscountPanel.Controls.Cast<Button>().OrderByDescending(c => c.TabIndex).ToList();

            var maxRowCount = DiscountPanel.Height/(ImageBtnHeight + (DefaultMarginOnY*2));
            var maxColCount = DiscountPanel.Width/(ImageBtnWidth + (DefaultMarginOnX*2));

            var controlWidth = 0;
            var colCount = 0;
            if (maxRowCount*maxColCount > discountPanelControls.Count)
            {
                colCount = discountPanelControls.Count/maxRowCount;
                controlWidth = DiscountPanel.Width/colCount - (DefaultMarginOnX*2);
            }
            else
            {
                controlWidth = ImageBtnWidth;
                colCount = maxColCount;
            }

            OrderControls(discountPanelControls, controlWidth, DiscountPanel,colCount, maxRowCount);

            #endregion

            #region Size BtnPanel Items

            controlWidth = 250;
            OrderControls(btnPanelControls, controlWidth, BtnPanel, 3 ,1 );

            #endregion

        }

        private void OrderControls(IEnumerable<Control> controls, int controlWidth, Control container, int colCount, int rowCount)
        {
            var ctrls = controls.ToList();

            if (colCount == 0 && rowCount == 0)
            {
                Control controlAfter = null;
                foreach (var control in ctrls)
                {
                    control.Width = controlWidth;
                    control.Height = ImageBtnHeight;
                    PushToLeft(control, container, controlAfter);
                    controlAfter = control;
                }
            }
            else
            {
                for (var r = 0; r < rowCount; r++)
                {
                    Control controlAfter = null;
                    var yPos = r*ImageBtnHeight + (DefaultMarginOnY*2);
                    for (var c = 0; c < colCount; c++)
                    {
                        var ctrl = ctrls.FirstOrDefault();
                        if (ctrl == null)
                            return;

                        ctrl.Width = controlWidth;
                        ctrl.Height = ImageBtnHeight;
                        PushToLeft(ctrl, container, controlAfter, false, yPos);
                        controlAfter = ctrl;

                        ctrls.Remove(ctrl);
                    }
                }
            }
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            SizePanels();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            FormState.NormalFullScreen(this);
            SizePanels();
        }

        public void ShowError(string text)
        {
            MessageBox.Show(text, @"Ошибка", MessageBoxButtons.OK);
        }

        private void SendCode( string code = null)
        {
            code = code ?? InputCode;
            try
            {
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    if (!CurrentCheque.IsReturn && ChequeService.Get(s => s.CheckCodeIsMaterial(code)) == 1)
                        ChequeService.Execute(
                            s => s.AddMaterialToChequeByCode(CurrentCheque.Id.GetValueOrDefault(), code));
                    RefreshCurrentCheque();
                }
            }
            catch (FaultException ex)
            {
                ShowError(ex.Message);
            }

        }

        public void RefreshCurrentCheque()
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                _blockPositionChange = true;

                CurrentCheque = CurrentCheque != null
                    ? ChequeService.Get(s => s.GetChequeById(CurrentCheque.Id.GetValueOrDefault()))
                    : ChequeService.Get(s => s.GetActiveCheque());

                if (CurrentCheque != null)
                {
                    chequeBindingSource.DataSource = CurrentCheque;

                    if (linesBindingSource.CurrencyManager.Count >= CurrentSelectedIndex)
                        linesBindingSource.CurrencyManager.Position = CurrentSelectedIndex;
                    else
                        CurrentSelectedIndex = 0;

                    ModeTextLb.Text = CurrentCheque.IsReturn
                        ? Properties.Resources.ReturnChequeNoPostfix
                        : string.Empty;
                }

                _blockPositionChange = false;
            }
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (ChequeLineGrid.IsCurrentCellInEditMode || NameTb.Focused)
                return;

            switch (e.KeyChar)
            {
                case (char) Keys.LineFeed:
                    FinishChequeBt_Click(FinishChequeBt, new EventArgs());
                    break;

                case (char) Keys.Enter:
                    SendCode();
                    InputCode = string.Empty;
                    e.Handled = true;
                    break;

                case (char) Keys.Back:
                    if (InputCode != string.Empty)
                        InputCode = InputCode.Remove(InputCode.Count() - 1);
                    break;
                
                case '-':
                    MinusQuantityBt_Click(this, null);
                    e.Handled = true;
                    break;

                case '+':
                     PlusQuantityBt_Click(this,null);
                     e.Handled = true;
                    break;

                default:
                    InputCode += e.KeyChar;
                    break;

            }

            CodeTb.SelectionStart = CodeTb.TextLength;
            CodeTb.SelectionLength = 0;
            e.Handled = true;
        }

        private void ChequeLineGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                ChequeService.Execute(s => s.UpdateChequeLine(linesBindingSource.Current as ChequeLineDto));
                RefreshCurrentCheque();
            }
        }

        private void ClearChequeBt_Click(object sender, EventArgs e)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                if (CurrentCheque == null)
                    return;

                UnityContext.Container.Resolve<IChequePrinter>().CancelCheque();

                ChequeService.Execute(s => s.ClearChequeById(CurrentCheque.Id.GetValueOrDefault()));
                RefreshCurrentCheque();

                CodeTb.Select();
            }
        }

        private void FinishChequeBt_Click(object sender, EventArgs e)
        {
            if (CurrentCheque == null || !CurrentCheque.Lines.Any())
            {
                ShowError("Отсутствуют позиции чека для завершения!");
                CodeTb.Select();
                return;
            }

            using (var fr = new FinishCheque(CurrentCheque))
            {
                if (!fr.IsDisposed /*Форма не открывалась*/&& fr.ShowDialog() != DialogResult.OK)
                {
                    CodeTb.Select();
                    return;
                }

                using (new WaitDialog(@"Закрытие чека"))
                {

                    ChequeService.Execute(s => s.CloseCheque(CurrentCheque));
                    CurrentCheque = null;
                    RefreshCurrentCheque();

                    CodeTb.Select();
                }
            }

        }

        private void PostponeBt_Click(object sender, EventArgs e)
        {
            if (!CurrentCheque.Lines.Any())
                return;

            try
            {
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    ChequeService.Execute(s => s.PostponeChequeById(CurrentCheque.Id.GetValueOrDefault()));
                    CurrentCheque = null;
                    RefreshCurrentCheque();

                    CodeTb.Select();
                }
            }
            catch (FaultException ex)
            {
                ShowError(ex.Message);
            }

        }

        /* private void ShowPostponedBt_Click(object sender, EventArgs e)
        {
            using (var pf = new PostponedForm())
            {
                if (pf.ShowDialog() != DialogResult.OK || pf.SelectedId <= 0)
                {
                    CodeTb.Select();
                    return;
                }
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    ChequeService.Execute(s => s.DeleteChequeById(CurrentCheque.Id.GetValueOrDefault()));
                    ChequeService.Execute(s=>s.ActivateCheque(pf.SelectedId));
                    CurrentCheque.Id = pf.SelectedId;
                    RefreshCurrentCheque();
                }
            }

            CodeTb.Select();
        }*/

        private void DeleteCurrentPosBt_Click(object sender, EventArgs e)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                var chequeLineDto = linesBindingSource.Current as ChequeLineDto;
                if (chequeLineDto != null)
                {
                    ChequeService.Execute(
                        s => s.DeleteChequeLineById(chequeLineDto.Id.GetValueOrDefault()));
                }
                RefreshCurrentCheque();            
            }

            CodeTb.Select();
        }

        private void linesBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            var chequeLineDto = linesBindingSource.Current as ChequeLineDto;

            CurrentPosLb.Text = chequeLineDto != null ? chequeLineDto.MaterialName : string.Empty;
        }

        /*private void CloseShiftBt_Click(object sender, EventArgs e)
        {
            using (var fr = new FinishShift())
            {
                fr.ShowDialog();
                SetActiveShift();
            }
            CodeTb.Select();
        }*/

        private void ReturnBt_Click(object sender, EventArgs e)
        {
            using (var fr = new ReturnForm())
            {
                if (fr.ShowDialog() != DialogResult.OK || fr.SelectedId <= 0)
                {
                    CodeTb.Select();
                    return;
                }
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    ChequeService.Execute(s => s.DeleteChequeById(CurrentCheque.Id.GetValueOrDefault()));
                    CurrentCheque = ChequeService.Get(s => s.CreateReturnCheque(fr.SelectedId));
                    if (fr.SelectedLineId > 0)
                        ChequeService.Execute(
                            s => s.CopyChequeLineById(fr.SelectedLineId, CurrentCheque.Id.GetValueOrDefault()));
                    else
                        ChequeService.Execute(
                            s => s.CopyChequeLines(fr.SelectedId, CurrentCheque.Id.GetValueOrDefault(), fr.SelectedLinesIds));

                    RefreshCurrentCheque();

                }
            }

            CodeTb.Select();
        }

        private void ChequeLineGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            e.Cancel = CurrentCheque.IsReturn;
        }

        private void ServiceBt_Click(object sender, EventArgs e)
        {
            using (var fr = new ServiceForm())
            {
                fr.ShowDialog();
                
            }

            CodeTb.Select();
        }

        private void KeyboardCtrl_KeyboardClick(char key)
        {
           /* if (GetNextControl(button1, false) NameTb)
                NameTb_KeyPress(this, new KeyPressEventArgs(key));
            else*/
                MainForm_KeyPress(this, new KeyPressEventArgs(key));
            CodeTb.Select();
        }

        private void NameTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Enter)
            {
                /*List<MaterialDto> res;
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    res = MaterialService.Get(s => s.SearchMaterialsByName(NameTb.Text));
                }
                if (res.Count > 1)
                {*/
                using (var fr = new MaterialForm(NameTb.Text))
                {
                    if (fr.ShowDialog() == DialogResult.OK)
                    {
                        if (!fr.SelectedOffer.IsHasOnCurrentWarehouse)
                        {
                            if (
                                MessageBox.Show(
                                    @"Попытка продажи товара, отсутствующего на текущем складе!" + Environment.NewLine +
                                    @"Продолжить?", @"Внимание",
                                    MessageBoxButtons.YesNo) == DialogResult.Yes)
                                SendCode(fr.SelectedOfferCode);
                        }
                        else
                            SendCode(fr.SelectedOfferCode);
                    }
                }
                //}
                NameTb.Clear();
                CodeTb.Select();
            }
        }

        private void FreeSellBt_Click(object sender, EventArgs e)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                ChequeService.Execute(s => s.AddFreeLineToCheque(CurrentCheque.Id.GetValueOrDefault()));
                RefreshCurrentCheque();
            }

        }

        private void DiscountSumBt_Click(object sender, EventArgs e)
        {
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    var chequeLineDto = linesBindingSource.Current as ChequeLineDto;
                    if (chequeLineDto != null)
                    {
                        if (fr.Sum > chequeLineDto.Price * chequeLineDto.Quantity)
                            ShowError(@"Нельзя задавать скидку более суммы текущей позиции чека");
                        else
                        {
                            chequeLineDto.Discount = fr.Sum;
                            ChequeService.Execute(
                                s => s.UpdateChequeLine(linesBindingSource.Current as ChequeLineDto));
                            RefreshCurrentCheque();
                        }
                    }
                }
            }
            CodeTb.Select();
        }

        private void DiscountPercentBt_Click(object sender, EventArgs e)
        {
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    if (fr.Sum > 100)
                        ShowError(@"Нельзя задавать скидку более 100%");
                    else
                    {
                        var chequeDto = (linesBindingSource.Current as ChequeLineDto);
                        if (chequeDto != null)
                        {
                            chequeDto.Discount = chequeDto.Sum/100*fr.Sum;
                            ChequeService.Execute(s => s.UpdateChequeLine(chequeDto));
                            RefreshCurrentCheque();
                        }
                    }
                }
            }
            CodeTb.Select();
        }

        private void SetSumBt_Click(object sender, EventArgs e)
        {
             var chequeLineDto = (linesBindingSource.Current as ChequeLineDto);
             if (chequeLineDto == null /*|| chequeLineDto.MaterialId != 0*/)
            {
                 ShowError("Не выбрана строка чека для задачи цены!");
               // ShowError(@"Сумму можно задавать только для свободной продажи!");
                return;
            }
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    chequeLineDto.Price = fr.Sum;
                    ChequeService.Execute(s => s.UpdateChequeLine(chequeLineDto));
                        RefreshCurrentCheque();
                }
            }
            CodeTb.Select();
        }

        private bool _blockPositionChange;

        private void linesBindingSource_PositionChanged(object sender, EventArgs e)
        {
            CurrentSelectedIndex = _blockPositionChange ? CurrentSelectedIndex : linesBindingSource.Position;
        }

        private void NameBt_Click(object sender, EventArgs e)
        {
            NameTb_KeyPress(null, new KeyPressEventArgs((char)Keys.Enter));
        }

        private void SetAmountBt_Click(object sender, EventArgs e)
        {
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    var chequeLineDto = linesBindingSource.Current as ChequeLineDto;
                    if (chequeLineDto != null)
                    {
                        chequeLineDto.Quantity = (int) fr.Sum;
                        ChequeService.Execute(s => s.UpdateChequeLine(linesBindingSource.Current as ChequeLineDto));
                        RefreshCurrentCheque();
                    }
                }
            }
            CodeTb.Select();
        }

        private void PlusQuantityBt_Click(object sender, EventArgs e)
        {
            var chequeLineDto = linesBindingSource.Current as ChequeLineDto;
            if (chequeLineDto != null)
            {
                chequeLineDto.Quantity++;
                ChequeService.Execute(s => s.UpdateChequeLine(linesBindingSource.Current as ChequeLineDto));
                RefreshCurrentCheque();
            }

            CodeTb.Select();
        }

        private void MinusQuantityBt_Click(object sender, EventArgs e)
        {
            var chequeLineDto = linesBindingSource.Current as ChequeLineDto;
            if (chequeLineDto != null && chequeLineDto.Quantity > 1)
            {
                chequeLineDto.Quantity--;
                ChequeService.Execute(s => s.UpdateChequeLine(linesBindingSource.Current as ChequeLineDto));
                RefreshCurrentCheque();
            }

            CodeTb.Select();
        }

        private void ClearDiscountBt_Click(object sender, EventArgs e)
        {
            var chequeLineDto = linesBindingSource.Current as ChequeLineDto;
            if (chequeLineDto != null)
            {
                chequeLineDto.Discount = 0;
                ChequeService.Execute(s => s.UpdateChequeLine(chequeLineDto));
                RefreshCurrentCheque();
            }
        }

        private void MainFormV2_1_Load(object sender, EventArgs e)
        {

        }

        private void TopPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FastBtn_Click(object sender, EventArgs e)
        {
          SendCode((sender as Control).Tag.ToString());
        }
    }
}
