﻿using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Services.Infrastructure.Client;

namespace TB.Cashier.Client.Shell
{
    public interface IMainForm
    {
        CabClientProxy<IChequeService> ChequeService { get; set; }
        CabClientProxy<IMaterialService> MaterialService { get; set; }

        ChequeDto CurrentCheque { get; set; }
        ShiftDto CurrentShift { get; set; }

        bool ActiveShiftIsAbsent { get; set; }

        void ShowError(string text);

        void SetActiveShift();
        void RefreshCurrentCheque();
    }
}
