﻿using System;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using Cashier.Equipment.Interface;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using TB.Cashier.Client.Shell.SelectForms;
using TB.Cashier.Client.Shell.Wait;
using TB.Cashier.Server.Services.Interfaces;
using TB.Cashier.Server.Services.Interfaces.Faults;
using TB.Services.Infrastructure.Client;
using Utils;

namespace TB.Cashier.Client.Shell
{

    public partial class ServiceForm : Form
    {

        private readonly IMainForm _mainForm = (Program.AppContext.MainForm as IMainForm);

        private readonly IChequePrinter _chequePrinter;

        public ServiceForm()
        {
            InitializeComponent();
            _chequePrinter = UnityContext.Container.Resolve<IChequePrinter>();
        }

        private void ServiceForm_Load(object sender, EventArgs e)
        {
            if (_mainForm.ActiveShiftIsAbsent)
            {
                Controls.Cast<Control>().ForEach(c=>c.Enabled = false);
                CloseShiftBt.Enabled = OpenShiftBt.Enabled = true;
            }
        }

        private void ServiceForm_Activated(object sender, EventArgs e)
        {
            if (_mainForm.ActiveShiftIsAbsent)
            {
                Controls.Cast<Control>().ForEach(c => c.Enabled = false);

                CloseShiftBt.Enabled = OpenShiftBt.Enabled = true;
                CloseWinSessionBt.Enabled = true;
                ClosePrBt.Enabled = true;
                TurnOffBt.Enabled = true;
            }
            else
                Controls.Cast<Control>().ForEach(c => c.Enabled = true);
        }


        private void CloseShiftBt_Click(object sender, EventArgs e)
        {
            using (var wD = new WaitDialog(Properties.Resources.LoadingText))
            {
                if (!_chequePrinter.PrintZReport())
                {
                    wD.Hide();
                    MessageBox.Show(_chequePrinter.LastError(), @"Ошибка", MessageBoxButtons.OK);
                }
                else
                {
                    new CabClientProxy<IChequeService>().Get(x => x.CloseCurrentShift());
                    _mainForm.SetActiveShift();
                }
            }
        }

        private void ClosePrBt_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CloseWinSessionBt_Click(object sender, EventArgs e)
        {
            ExitWindows.LogOff(true);
        }

        private void TurnOffBt_Click(object sender, EventArgs e)
        {
            ExitWindows.Shutdown(true);
        }

        private void XOtchetBt_Click(object sender, EventArgs e)
        {
            if (!_chequePrinter.PrintXReport())
                MessageBox.Show(_chequePrinter.LastError(), @"Ошибка", MessageBoxButtons.OK);
        }

        private void BalanceBt_Click(object sender, EventArgs e)
        {
            var sum = new CabClientProxy<IChequeService>().Get(x => x.CurrentShiftCashSum());
            using (var fr = new InfoForm(sum.ToString("C", CultureInfo.CurrentCulture), @"Наличность в кассе"))
            {
                fr.ShowDialog();
            }
        }

        private void IncomeBt_Click(object sender, EventArgs e)
        {
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    if (!_chequePrinter.CashIncome(fr.Sum))
                        MessageBox.Show(_chequePrinter.LastError(), @"Ошибка", MessageBoxButtons.OK);
                    else
                    {
                        var sum = fr.Sum;
                        new CabClientProxy<IChequeService>().Execute(s => s.MakeIncome(sum));
                    }
                }
            }
        }

        private void OutcomeBt_Click(object sender, EventArgs e)
        {
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    if (!_chequePrinter.CashOutcome(fr.Sum))
                        MessageBox.Show(_chequePrinter.LastError(), @"Ошибка", MessageBoxButtons.OK);
                    else
                    {
                        var sum = fr.Sum;
                        new CabClientProxy<IChequeService>().Execute(s => s.MakeOutcome(sum));
                    }
                }
            }
        }

        private void ShowPostponedBt_Click(object sender, EventArgs e)
        {
            using (var pf = new PostponedForm())
            {
                if (pf.ShowDialog() != DialogResult.OK || pf.SelectedId <= 0)
                {
                    return;
                }
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    _mainForm.ChequeService.Execute(
                        s => s.DeleteChequeById(_mainForm.CurrentCheque.Id.GetValueOrDefault()));
                    _mainForm.ChequeService.Execute(s => s.ActivateCheque(pf.SelectedId));
                    _mainForm.CurrentCheque.Id = pf.SelectedId;
                    _mainForm.RefreshCurrentCheque();
                    Close();
                }
            }
        }


        private void ShowShiftChequesBt_Click(object sender, EventArgs e)
        {
            using (var pf = new ShiftChequesForm())
            {
                pf.ShowDialog();
            }
        }

        private void OpenShiftBt_Click(object sender, EventArgs e)
        {
            using (var wD = new WaitDialog(Properties.Resources.LoadingText))
            {
                var currentShift = new CabClientProxy<IChequeService>().Get(s => s.OpenShift());

                if (currentShift != null)
                {
                    if (!_chequePrinter.OpenShift(currentShift))
                    {
                        wD.Hide();
                        _mainForm.ShowError(UnityContext.Container.Resolve<IChequePrinter>().LastError());
                    }
                    _mainForm.SetActiveShift();
                }
            }
        }
    }
}
