﻿namespace TB.Cashier.Client.Shell
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.DiscountPanel = new System.Windows.Forms.Panel();
            this.SetSumBt = new System.Windows.Forms.Button();
            this.DiscountPercentBt = new System.Windows.Forms.Button();
            this.DiscountSumBt = new System.Windows.Forms.Button();
            this.SummaryPanel = new System.Windows.Forms.Panel();
            this.SumLb = new System.Windows.Forms.Label();
            this.chequeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SummaryTextLb = new System.Windows.Forms.Label();
            this.CodePanel = new System.Windows.Forms.Panel();
            this.NameLb = new System.Windows.Forms.Label();
            this.NameTb = new System.Windows.Forms.TextBox();
            this.CodeLb = new System.Windows.Forms.Label();
            this.CodeTb = new System.Windows.Forms.TextBox();
            this.CurrentPanel = new System.Windows.Forms.Panel();
            this.CurrentPosLb = new System.Windows.Forms.Label();
            this.GridPanel = new System.Windows.Forms.Panel();
            this.ChequeLineGrid = new System.Windows.Forms.DataGridView();
            this.materialNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.InfoPanel = new System.Windows.Forms.Panel();
            this.InfoCenterPanel = new System.Windows.Forms.Panel();
            this.ModeTextLb = new System.Windows.Forms.Label();
            this.InfoRightPanel = new System.Windows.Forms.Panel();
            this.OpenShiftBt = new System.Windows.Forms.Button();
            this.ShiftTextLb = new System.Windows.Forms.Label();
            this.ShiftLb = new System.Windows.Forms.Label();
            this.shiftBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.InfoLeftPanel = new System.Windows.Forms.Panel();
            this.ChequeNoTextLb = new System.Windows.Forms.Label();
            this.ChequeNoLb = new System.Windows.Forms.Label();
            this.RightPanel = new System.Windows.Forms.Panel();
            this.FreeSellBt = new System.Windows.Forms.Button();
            this.ChequeWorkBt = new System.Windows.Forms.Button();
            this.BtDelimeter4 = new System.Windows.Forms.Panel();
            this.KeyboardCtrl = new TB.Cashier.Client.Shell.Controls.KeyboardControl();
            this.ServiceBt = new System.Windows.Forms.Button();
            this.DeleteCurrentPosBt = new System.Windows.Forms.Button();
            this.BtDelimiter3 = new System.Windows.Forms.Panel();
            this.ReturnBt = new System.Windows.Forms.Button();
            this.BtDelimiter2 = new System.Windows.Forms.Panel();
            this.BtDelimiter1 = new System.Windows.Forms.Panel();
            this.PostponeBt = new System.Windows.Forms.Button();
            this.ClearChequeBt = new System.Windows.Forms.Button();
            this.FinishChequeBt = new System.Windows.Forms.Button();
            this.LeftPanel.SuspendLayout();
            this.DiscountPanel.SuspendLayout();
            this.SummaryPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chequeBindingSource)).BeginInit();
            this.CodePanel.SuspendLayout();
            this.CurrentPanel.SuspendLayout();
            this.GridPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChequeLineGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource)).BeginInit();
            this.InfoPanel.SuspendLayout();
            this.InfoCenterPanel.SuspendLayout();
            this.InfoRightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shiftBindingSource)).BeginInit();
            this.InfoLeftPanel.SuspendLayout();
            this.RightPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LeftPanel
            // 
            this.LeftPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LeftPanel.Controls.Add(this.DiscountPanel);
            this.LeftPanel.Controls.Add(this.SummaryPanel);
            this.LeftPanel.Controls.Add(this.CodePanel);
            this.LeftPanel.Controls.Add(this.CurrentPanel);
            this.LeftPanel.Controls.Add(this.GridPanel);
            this.LeftPanel.Controls.Add(this.InfoPanel);
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(1070, 1045);
            this.LeftPanel.TabIndex = 0;
            // 
            // DiscountPanel
            // 
            this.DiscountPanel.Controls.Add(this.SetSumBt);
            this.DiscountPanel.Controls.Add(this.DiscountPercentBt);
            this.DiscountPanel.Controls.Add(this.DiscountSumBt);
            this.DiscountPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DiscountPanel.Location = new System.Drawing.Point(0, 904);
            this.DiscountPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.DiscountPanel.Name = "DiscountPanel";
            this.DiscountPanel.Size = new System.Drawing.Size(1066, 97);
            this.DiscountPanel.TabIndex = 5;
            // 
            // SetSumBt
            // 
            this.SetSumBt.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.SetSumBt.Location = new System.Drawing.Point(414, 33);
            this.SetSumBt.Name = "SetSumBt";
            this.SetSumBt.Size = new System.Drawing.Size(196, 39);
            this.SetSumBt.TabIndex = 243;
            this.SetSumBt.Text = "Задать сумму";
            this.SetSumBt.UseVisualStyleBackColor = true;
            this.SetSumBt.Click += new System.EventHandler(this.SetSumBt_Click);
            // 
            // DiscountPercentBt
            // 
            this.DiscountPercentBt.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.DiscountPercentBt.Location = new System.Drawing.Point(629, 33);
            this.DiscountPercentBt.Name = "DiscountPercentBt";
            this.DiscountPercentBt.Size = new System.Drawing.Size(196, 39);
            this.DiscountPercentBt.TabIndex = 242;
            this.DiscountPercentBt.Text = "Скидка в %";
            this.DiscountPercentBt.UseVisualStyleBackColor = true;
            this.DiscountPercentBt.Click += new System.EventHandler(this.DiscountPercentBt_Click);
            // 
            // DiscountSumBt
            // 
            this.DiscountSumBt.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.DiscountSumBt.Location = new System.Drawing.Point(863, 32);
            this.DiscountSumBt.Name = "DiscountSumBt";
            this.DiscountSumBt.Size = new System.Drawing.Size(196, 40);
            this.DiscountSumBt.TabIndex = 241;
            this.DiscountSumBt.Text = "Скидка суммой";
            this.DiscountSumBt.UseVisualStyleBackColor = true;
            this.DiscountSumBt.Click += new System.EventHandler(this.DiscountSumBt_Click);
            // 
            // SummaryPanel
            // 
            this.SummaryPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SummaryPanel.Controls.Add(this.SumLb);
            this.SummaryPanel.Controls.Add(this.SummaryTextLb);
            this.SummaryPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.SummaryPanel.Font = new System.Drawing.Font("Segoe UI", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SummaryPanel.ForeColor = System.Drawing.Color.Blue;
            this.SummaryPanel.Location = new System.Drawing.Point(0, 779);
            this.SummaryPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.SummaryPanel.Name = "SummaryPanel";
            this.SummaryPanel.Size = new System.Drawing.Size(1066, 125);
            this.SummaryPanel.TabIndex = 4;
            // 
            // SumLb
            // 
            this.SumLb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.chequeBindingSource, "Sum", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "C2"));
            this.SumLb.Dock = System.Windows.Forms.DockStyle.Right;
            this.SumLb.Location = new System.Drawing.Point(618, 0);
            this.SumLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SumLb.Name = "SumLb";
            this.SumLb.Size = new System.Drawing.Size(444, 121);
            this.SumLb.TabIndex = 1;
            this.SumLb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chequeBindingSource
            // 
            this.chequeBindingSource.DataSource = typeof(TB.Cashier.Server.Services.Dto.ChequeDto);
            // 
            // SummaryTextLb
            // 
            this.SummaryTextLb.Dock = System.Windows.Forms.DockStyle.Left;
            this.SummaryTextLb.Location = new System.Drawing.Point(0, 0);
            this.SummaryTextLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SummaryTextLb.Name = "SummaryTextLb";
            this.SummaryTextLb.Size = new System.Drawing.Size(323, 121);
            this.SummaryTextLb.TabIndex = 0;
            this.SummaryTextLb.Text = "Итог чека:";
            this.SummaryTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CodePanel
            // 
            this.CodePanel.Controls.Add(this.NameLb);
            this.CodePanel.Controls.Add(this.NameTb);
            this.CodePanel.Controls.Add(this.CodeLb);
            this.CodePanel.Controls.Add(this.CodeTb);
            this.CodePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.CodePanel.Location = new System.Drawing.Point(0, 657);
            this.CodePanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.CodePanel.Name = "CodePanel";
            this.CodePanel.Size = new System.Drawing.Size(1066, 122);
            this.CodePanel.TabIndex = 3;
            // 
            // NameLb
            // 
            this.NameLb.Location = new System.Drawing.Point(586, 7);
            this.NameLb.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.NameLb.Name = "NameLb";
            this.NameLb.Size = new System.Drawing.Size(147, 111);
            this.NameLb.TabIndex = 3;
            this.NameLb.Text = "Наименование:";
            this.NameLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NameTb
            // 
            this.NameTb.Location = new System.Drawing.Point(743, 47);
            this.NameTb.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.NameTb.Name = "NameTb";
            this.NameTb.Size = new System.Drawing.Size(316, 32);
            this.NameTb.TabIndex = 2;
            this.NameTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NameTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NameTb_KeyPress);
            // 
            // CodeLb
            // 
            this.CodeLb.Location = new System.Drawing.Point(0, 7);
            this.CodeLb.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.CodeLb.Name = "CodeLb";
            this.CodeLb.Size = new System.Drawing.Size(49, 111);
            this.CodeLb.TabIndex = 1;
            this.CodeLb.Text = "Код:";
            this.CodeLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CodeTb
            // 
            this.CodeTb.Location = new System.Drawing.Point(70, 47);
            this.CodeTb.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.CodeTb.Name = "CodeTb";
            this.CodeTb.Size = new System.Drawing.Size(279, 32);
            this.CodeTb.TabIndex = 0;
            this.CodeTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CurrentPanel
            // 
            this.CurrentPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CurrentPanel.Controls.Add(this.CurrentPosLb);
            this.CurrentPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.CurrentPanel.Location = new System.Drawing.Point(0, 565);
            this.CurrentPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.CurrentPanel.Name = "CurrentPanel";
            this.CurrentPanel.Size = new System.Drawing.Size(1066, 92);
            this.CurrentPanel.TabIndex = 2;
            // 
            // CurrentPosLb
            // 
            this.CurrentPosLb.Dock = System.Windows.Forms.DockStyle.Right;
            this.CurrentPosLb.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.CurrentPosLb.Location = new System.Drawing.Point(767, 0);
            this.CurrentPosLb.Name = "CurrentPosLb";
            this.CurrentPosLb.Size = new System.Drawing.Size(295, 88);
            this.CurrentPosLb.TabIndex = 0;
            this.CurrentPosLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GridPanel
            // 
            this.GridPanel.Controls.Add(this.ChequeLineGrid);
            this.GridPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.GridPanel.Location = new System.Drawing.Point(0, 192);
            this.GridPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.GridPanel.Name = "GridPanel";
            this.GridPanel.Size = new System.Drawing.Size(1066, 373);
            this.GridPanel.TabIndex = 1;
            // 
            // ChequeLineGrid
            // 
            this.ChequeLineGrid.AllowUserToAddRows = false;
            this.ChequeLineGrid.AllowUserToResizeRows = false;
            this.ChequeLineGrid.AutoGenerateColumns = false;
            this.ChequeLineGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ChequeLineGrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.ChequeLineGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChequeLineGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ChequeLineGrid.ColumnHeadersHeight = 35;
            this.ChequeLineGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ChequeLineGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialNameDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.discountDataGridViewTextBoxColumn,
            this.sumDataGridViewTextBoxColumn});
            this.ChequeLineGrid.DataSource = this.linesBindingSource;
            this.ChequeLineGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChequeLineGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.ChequeLineGrid.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.ChequeLineGrid.Location = new System.Drawing.Point(0, 0);
            this.ChequeLineGrid.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.ChequeLineGrid.Name = "ChequeLineGrid";
            this.ChequeLineGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.ChequeLineGrid.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChequeLineGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.ChequeLineGrid.RowTemplate.DividerHeight = 1;
            this.ChequeLineGrid.RowTemplate.Height = 40;
            this.ChequeLineGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ChequeLineGrid.ShowEditingIcon = false;
            this.ChequeLineGrid.Size = new System.Drawing.Size(1066, 373);
            this.ChequeLineGrid.TabIndex = 1;
            this.ChequeLineGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.ChequeLineGrid_CellBeginEdit);
            this.ChequeLineGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ChequeLineGrid_CellEndEdit);
            // 
            // materialNameDataGridViewTextBoxColumn
            // 
            this.materialNameDataGridViewTextBoxColumn.DataPropertyName = "MaterialName";
            this.materialNameDataGridViewTextBoxColumn.FillWeight = 228.4264F;
            this.materialNameDataGridViewTextBoxColumn.HeaderText = "Наименование";
            this.materialNameDataGridViewTextBoxColumn.Name = "materialNameDataGridViewTextBoxColumn";
            this.materialNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            this.quantityDataGridViewTextBoxColumn.FillWeight = 67.89333F;
            this.quantityDataGridViewTextBoxColumn.HeaderText = "Количество";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.priceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.priceDataGridViewTextBoxColumn.FillWeight = 67.89333F;
            this.priceDataGridViewTextBoxColumn.HeaderText = "Цена";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // discountDataGridViewTextBoxColumn
            // 
            this.discountDataGridViewTextBoxColumn.DataPropertyName = "Discount";
            this.discountDataGridViewTextBoxColumn.FillWeight = 67.89333F;
            this.discountDataGridViewTextBoxColumn.HeaderText = "Скидка";
            this.discountDataGridViewTextBoxColumn.Name = "discountDataGridViewTextBoxColumn";
            // 
            // sumDataGridViewTextBoxColumn
            // 
            this.sumDataGridViewTextBoxColumn.DataPropertyName = "Sum";
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.sumDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.sumDataGridViewTextBoxColumn.FillWeight = 67.89333F;
            this.sumDataGridViewTextBoxColumn.HeaderText = "Сумма";
            this.sumDataGridViewTextBoxColumn.Name = "sumDataGridViewTextBoxColumn";
            this.sumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // linesBindingSource
            // 
            this.linesBindingSource.DataMember = "Lines";
            this.linesBindingSource.DataSource = this.chequeBindingSource;
            this.linesBindingSource.CurrentChanged += new System.EventHandler(this.linesBindingSource_CurrentChanged);
            // 
            // InfoPanel
            // 
            this.InfoPanel.Controls.Add(this.InfoCenterPanel);
            this.InfoPanel.Controls.Add(this.InfoRightPanel);
            this.InfoPanel.Controls.Add(this.InfoLeftPanel);
            this.InfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.InfoPanel.ForeColor = System.Drawing.Color.MidnightBlue;
            this.InfoPanel.Location = new System.Drawing.Point(0, 0);
            this.InfoPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.InfoPanel.Name = "InfoPanel";
            this.InfoPanel.Size = new System.Drawing.Size(1066, 192);
            this.InfoPanel.TabIndex = 0;
            // 
            // InfoCenterPanel
            // 
            this.InfoCenterPanel.Controls.Add(this.ModeTextLb);
            this.InfoCenterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoCenterPanel.Location = new System.Drawing.Point(143, 0);
            this.InfoCenterPanel.Name = "InfoCenterPanel";
            this.InfoCenterPanel.Size = new System.Drawing.Size(449, 192);
            this.InfoCenterPanel.TabIndex = 6;
            // 
            // ModeTextLb
            // 
            this.ModeTextLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.ModeTextLb.ForeColor = System.Drawing.Color.Red;
            this.ModeTextLb.Location = new System.Drawing.Point(0, 0);
            this.ModeTextLb.Name = "ModeTextLb";
            this.ModeTextLb.Size = new System.Drawing.Size(449, 84);
            this.ModeTextLb.TabIndex = 0;
            this.ModeTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // InfoRightPanel
            // 
            this.InfoRightPanel.Controls.Add(this.OpenShiftBt);
            this.InfoRightPanel.Controls.Add(this.ShiftTextLb);
            this.InfoRightPanel.Controls.Add(this.ShiftLb);
            this.InfoRightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.InfoRightPanel.Location = new System.Drawing.Point(592, 0);
            this.InfoRightPanel.Name = "InfoRightPanel";
            this.InfoRightPanel.Size = new System.Drawing.Size(474, 192);
            this.InfoRightPanel.TabIndex = 5;
            // 
            // OpenShiftBt
            // 
            this.OpenShiftBt.Location = new System.Drawing.Point(279, 132);
            this.OpenShiftBt.Name = "OpenShiftBt";
            this.OpenShiftBt.Size = new System.Drawing.Size(188, 50);
            this.OpenShiftBt.TabIndex = 4;
            this.OpenShiftBt.Text = "Открыть смену";
            this.OpenShiftBt.UseVisualStyleBackColor = true;
            this.OpenShiftBt.Visible = false;
            this.OpenShiftBt.Click += new System.EventHandler(this.OpenShiftBt_Click);
            // 
            // ShiftTextLb
            // 
            this.ShiftTextLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.ShiftTextLb.Location = new System.Drawing.Point(0, 0);
            this.ShiftTextLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ShiftTextLb.Name = "ShiftTextLb";
            this.ShiftTextLb.Size = new System.Drawing.Size(474, 108);
            this.ShiftTextLb.TabIndex = 3;
            this.ShiftTextLb.Text = "Смена:";
            this.ShiftTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ShiftLb
            // 
            this.ShiftLb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shiftBindingSource, "ShiftDateShort", true));
            this.ShiftLb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ShiftLb.Location = new System.Drawing.Point(0, 108);
            this.ShiftLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ShiftLb.Name = "ShiftLb";
            this.ShiftLb.Size = new System.Drawing.Size(474, 84);
            this.ShiftLb.TabIndex = 2;
            this.ShiftLb.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // shiftBindingSource
            // 
            this.shiftBindingSource.DataSource = typeof(TB.Cashier.Server.Services.Dto.ShiftDto);
            // 
            // InfoLeftPanel
            // 
            this.InfoLeftPanel.Controls.Add(this.ChequeNoTextLb);
            this.InfoLeftPanel.Controls.Add(this.ChequeNoLb);
            this.InfoLeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.InfoLeftPanel.Location = new System.Drawing.Point(0, 0);
            this.InfoLeftPanel.Name = "InfoLeftPanel";
            this.InfoLeftPanel.Size = new System.Drawing.Size(143, 192);
            this.InfoLeftPanel.TabIndex = 4;
            // 
            // ChequeNoTextLb
            // 
            this.ChequeNoTextLb.Dock = System.Windows.Forms.DockStyle.Top;
            this.ChequeNoTextLb.Location = new System.Drawing.Point(0, 0);
            this.ChequeNoTextLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ChequeNoTextLb.Name = "ChequeNoTextLb";
            this.ChequeNoTextLb.Size = new System.Drawing.Size(143, 84);
            this.ChequeNoTextLb.TabIndex = 0;
            this.ChequeNoTextLb.Text = "Номер Чека:";
            this.ChequeNoTextLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ChequeNoLb
            // 
            this.ChequeNoLb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.chequeBindingSource, "No", true));
            this.ChequeNoLb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChequeNoLb.Location = new System.Drawing.Point(0, 90);
            this.ChequeNoLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ChequeNoLb.Name = "ChequeNoLb";
            this.ChequeNoLb.Size = new System.Drawing.Size(143, 102);
            this.ChequeNoLb.TabIndex = 1;
            // 
            // RightPanel
            // 
            this.RightPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.RightPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.RightPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.RightPanel.Controls.Add(this.FreeSellBt);
            this.RightPanel.Controls.Add(this.ChequeWorkBt);
            this.RightPanel.Controls.Add(this.BtDelimeter4);
            this.RightPanel.Controls.Add(this.KeyboardCtrl);
            this.RightPanel.Controls.Add(this.ServiceBt);
            this.RightPanel.Controls.Add(this.DeleteCurrentPosBt);
            this.RightPanel.Controls.Add(this.BtDelimiter3);
            this.RightPanel.Controls.Add(this.ReturnBt);
            this.RightPanel.Controls.Add(this.BtDelimiter2);
            this.RightPanel.Controls.Add(this.BtDelimiter1);
            this.RightPanel.Controls.Add(this.PostponeBt);
            this.RightPanel.Controls.Add(this.ClearChequeBt);
            this.RightPanel.Controls.Add(this.FinishChequeBt);
            this.RightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightPanel.Location = new System.Drawing.Point(1071, 0);
            this.RightPanel.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.RightPanel.Name = "RightPanel";
            this.RightPanel.Size = new System.Drawing.Size(372, 1045);
            this.RightPanel.TabIndex = 1;
            // 
            // FreeSellBt
            // 
            this.FreeSellBt.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.FreeSellBt.Location = new System.Drawing.Point(6, 663);
            this.FreeSellBt.Name = "FreeSellBt";
            this.FreeSellBt.Size = new System.Drawing.Size(196, 40);
            this.FreeSellBt.TabIndex = 235;
            this.FreeSellBt.Text = "Свободная продажа";
            this.FreeSellBt.UseVisualStyleBackColor = true;
            this.FreeSellBt.Click += new System.EventHandler(this.FreeSellBt_Click);
            // 
            // ChequeWorkBt
            // 
            this.ChequeWorkBt.Location = new System.Drawing.Point(6, 463);
            this.ChequeWorkBt.Name = "ChequeWorkBt";
            this.ChequeWorkBt.Size = new System.Drawing.Size(196, 40);
            this.ChequeWorkBt.TabIndex = 275;
            this.ChequeWorkBt.Text = "Чеки";
            this.ChequeWorkBt.UseVisualStyleBackColor = true;
            this.ChequeWorkBt.Click += new System.EventHandler(this.ChequeWorkBt_Click);
            // 
            // BtDelimeter4
            // 
            this.BtDelimeter4.BackColor = System.Drawing.Color.DarkGray;
            this.BtDelimeter4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BtDelimeter4.Location = new System.Drawing.Point(6, 397);
            this.BtDelimeter4.Name = "BtDelimeter4";
            this.BtDelimeter4.Size = new System.Drawing.Size(200, 10);
            this.BtDelimeter4.TabIndex = 281;
            // 
            // KeyboardCtrl
            // 
            this.KeyboardCtrl.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyboardCtrl.Location = new System.Drawing.Point(4, 13);
            this.KeyboardCtrl.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.KeyboardCtrl.Name = "KeyboardCtrl";
            this.KeyboardCtrl.Padding = new System.Windows.Forms.Padding(9, 10, 9, 10);
            this.KeyboardCtrl.Size = new System.Drawing.Size(351, 366);
            this.KeyboardCtrl.TabIndex = 290;
            this.KeyboardCtrl.KeyboardClick += new TB.Cashier.Client.Shell.Controls.KeyboardClickDelegate(this.KeyboardCtrl_KeyboardClick);
            // 
            // ServiceBt
            // 
            this.ServiceBt.Location = new System.Drawing.Point(5, 412);
            this.ServiceBt.Name = "ServiceBt";
            this.ServiceBt.Size = new System.Drawing.Size(196, 40);
            this.ServiceBt.TabIndex = 280;
            this.ServiceBt.Text = "Сервис";
            this.ServiceBt.UseVisualStyleBackColor = true;
            this.ServiceBt.Click += new System.EventHandler(this.ServiceBt_Click);
            // 
            // DeleteCurrentPosBt
            // 
            this.DeleteCurrentPosBt.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.DeleteCurrentPosBt.Location = new System.Drawing.Point(6, 617);
            this.DeleteCurrentPosBt.Name = "DeleteCurrentPosBt";
            this.DeleteCurrentPosBt.Size = new System.Drawing.Size(196, 40);
            this.DeleteCurrentPosBt.TabIndex = 240;
            this.DeleteCurrentPosBt.Text = "Удалить текущую позицию";
            this.DeleteCurrentPosBt.UseVisualStyleBackColor = true;
            this.DeleteCurrentPosBt.Click += new System.EventHandler(this.DeleteCurrentPosBt_Click);
            // 
            // BtDelimiter3
            // 
            this.BtDelimiter3.BackColor = System.Drawing.Color.DarkGray;
            this.BtDelimiter3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BtDelimiter3.Location = new System.Drawing.Point(6, 711);
            this.BtDelimiter3.Name = "BtDelimiter3";
            this.BtDelimiter3.Size = new System.Drawing.Size(200, 10);
            this.BtDelimiter3.TabIndex = 230;
            // 
            // ReturnBt
            // 
            this.ReturnBt.ForeColor = System.Drawing.SystemColors.Highlight;
            this.ReturnBt.Location = new System.Drawing.Point(6, 727);
            this.ReturnBt.Name = "ReturnBt";
            this.ReturnBt.Size = new System.Drawing.Size(196, 40);
            this.ReturnBt.TabIndex = 220;
            this.ReturnBt.Text = "Возврат";
            this.ReturnBt.UseVisualStyleBackColor = true;
            this.ReturnBt.Click += new System.EventHandler(this.ReturnBt_Click);
            // 
            // BtDelimiter2
            // 
            this.BtDelimiter2.BackColor = System.Drawing.Color.DarkGray;
            this.BtDelimiter2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BtDelimiter2.Location = new System.Drawing.Point(6, 509);
            this.BtDelimiter2.Name = "BtDelimiter2";
            this.BtDelimiter2.Size = new System.Drawing.Size(200, 10);
            this.BtDelimiter2.TabIndex = 270;
            // 
            // BtDelimiter1
            // 
            this.BtDelimiter1.BackColor = System.Drawing.Color.DarkGray;
            this.BtDelimiter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BtDelimiter1.Location = new System.Drawing.Point(6, 776);
            this.BtDelimiter1.Name = "BtDelimiter1";
            this.BtDelimiter1.Size = new System.Drawing.Size(200, 10);
            this.BtDelimiter1.TabIndex = 210;
            // 
            // PostponeBt
            // 
            this.PostponeBt.ForeColor = System.Drawing.Color.OliveDrab;
            this.PostponeBt.Location = new System.Drawing.Point(6, 525);
            this.PostponeBt.Name = "PostponeBt";
            this.PostponeBt.Size = new System.Drawing.Size(196, 40);
            this.PostponeBt.TabIndex = 260;
            this.PostponeBt.Text = "Отложить";
            this.PostponeBt.UseVisualStyleBackColor = true;
            this.PostponeBt.Click += new System.EventHandler(this.PostponeBt_Click);
            // 
            // ClearChequeBt
            // 
            this.ClearChequeBt.Location = new System.Drawing.Point(6, 571);
            this.ClearChequeBt.Name = "ClearChequeBt";
            this.ClearChequeBt.Size = new System.Drawing.Size(196, 40);
            this.ClearChequeBt.TabIndex = 250;
            this.ClearChequeBt.Text = "Сторно";
            this.ClearChequeBt.UseVisualStyleBackColor = true;
            this.ClearChequeBt.Click += new System.EventHandler(this.ClearChequeBt_Click);
            // 
            // FinishChequeBt
            // 
            this.FinishChequeBt.ForeColor = System.Drawing.Color.Red;
            this.FinishChequeBt.Location = new System.Drawing.Point(4, 792);
            this.FinishChequeBt.Name = "FinishChequeBt";
            this.FinishChequeBt.Size = new System.Drawing.Size(196, 40);
            this.FinishChequeBt.TabIndex = 200;
            this.FinishChequeBt.Text = "Закрытие чека";
            this.FinishChequeBt.UseVisualStyleBackColor = true;
            this.FinishChequeBt.Click += new System.EventHandler(this.FinishChequeBt_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1443, 1045);
            this.ControlBox = false;
            this.Controls.Add(this.RightPanel);
            this.Controls.Add(this.LeftPanel);
            this.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Фронт кассира";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.LeftPanel.ResumeLayout(false);
            this.DiscountPanel.ResumeLayout(false);
            this.SummaryPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chequeBindingSource)).EndInit();
            this.CodePanel.ResumeLayout(false);
            this.CodePanel.PerformLayout();
            this.CurrentPanel.ResumeLayout(false);
            this.GridPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChequeLineGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource)).EndInit();
            this.InfoPanel.ResumeLayout(false);
            this.InfoCenterPanel.ResumeLayout(false);
            this.InfoRightPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.shiftBindingSource)).EndInit();
            this.InfoLeftPanel.ResumeLayout(false);
            this.RightPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.Panel DiscountPanel;
        private System.Windows.Forms.Panel SummaryPanel;
        private System.Windows.Forms.Panel CodePanel;
        private System.Windows.Forms.Panel CurrentPanel;
        private System.Windows.Forms.Panel GridPanel;
        private System.Windows.Forms.Panel InfoPanel;
        private System.Windows.Forms.Panel RightPanel;
        private System.Windows.Forms.DataGridView ChequeLineGrid;
        private System.Windows.Forms.Label CodeLb;
        private System.Windows.Forms.TextBox CodeTb;
        private System.Windows.Forms.Label ChequeNoLb;
        private System.Windows.Forms.Label ChequeNoTextLb;
        private System.Windows.Forms.BindingSource chequeBindingSource;
        private System.Windows.Forms.Label SumLb;
        private System.Windows.Forms.Label SummaryTextLb;
        private System.Windows.Forms.BindingSource linesBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn discountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button ClearChequeBt;
        private System.Windows.Forms.Button FinishChequeBt;
        private System.Windows.Forms.Label ShiftLb;
        private System.Windows.Forms.Label ShiftTextLb;
        private System.Windows.Forms.BindingSource shiftBindingSource;
        private System.Windows.Forms.Button PostponeBt;
        private System.Windows.Forms.Panel BtDelimiter1;
        private System.Windows.Forms.Panel BtDelimiter2;
        private System.Windows.Forms.Panel BtDelimiter3;
        private System.Windows.Forms.Button ReturnBt;
        private System.Windows.Forms.Button DeleteCurrentPosBt;
        private System.Windows.Forms.Label CurrentPosLb;
        private System.Windows.Forms.Panel InfoRightPanel;
        private System.Windows.Forms.Panel InfoLeftPanel;
        private System.Windows.Forms.Panel InfoCenterPanel;
        private System.Windows.Forms.Label ModeTextLb;
        private System.Windows.Forms.Button ServiceBt;
        private Controls.KeyboardControl KeyboardCtrl;
        private System.Windows.Forms.Panel BtDelimeter4;
        private System.Windows.Forms.Label NameLb;
        private System.Windows.Forms.TextBox NameTb;
        private System.Windows.Forms.Button ChequeWorkBt;
        private System.Windows.Forms.Button OpenShiftBt;
        private System.Windows.Forms.Button DiscountPercentBt;
        private System.Windows.Forms.Button DiscountSumBt;
        private System.Windows.Forms.Button FreeSellBt;
        private System.Windows.Forms.Button SetSumBt;
    }
}