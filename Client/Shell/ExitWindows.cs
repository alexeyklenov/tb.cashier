﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace TB.Cashier.Client.Shell
{
    internal static class ExitWindows
    {

        private struct Luid
        {
            public int LowPart;
            public int HighPart;
        }

        private struct LuidAndAttributes
        {
            public Luid PLuid;
            public int Attributes;
        }

        private struct TokenPrivileges
        {
            public int PrivilegeCount;
            public LuidAndAttributes Privileges;
        }

        [DllImport("advapi32.dll")]
        private static extern int OpenProcessToken(IntPtr processHandle,
            int desiredAccess, out IntPtr tokenHandle);

        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AdjustTokenPrivileges(IntPtr tokenHandle,
            [MarshalAs(UnmanagedType.Bool)] bool disableAllPrivileges,
            ref TokenPrivileges newState,
            UInt32 bufferLength,
            IntPtr previousState,
            IntPtr returnLength);

        [DllImport("advapi32.dll")]
        private static extern int LookupPrivilegeValue(string lpSystemName,
            string lpName, out Luid lpLuid);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int ExitWindowsEx(uint uFlags, uint dwReason);

        private const string SeShutdownName = "SeShutdownPrivilege";
        private const short SePrivilegeEnabled = 2;
        private const short TokenAdjustPrivileges = 32;
        private const short TokenQuery = 8;

        private const ushort EwxLogoff = 0;
        private const ushort EwxPoweroff = 0x00000008;
        private const ushort EwxReboot = 0x00000002;
        private const ushort EwxRestartapps = 0x00000040;
        private const ushort EwxShutdown = 0x00000001;
        private const ushort EwxForce = 0x00000004;

        private static void GetPrivileges()
        {
            IntPtr hToken;
            TokenPrivileges tkp;

            OpenProcessToken(Process.GetCurrentProcess().Handle,
                TokenAdjustPrivileges | TokenQuery, out hToken);
            tkp.PrivilegeCount = 1;
            tkp.Privileges.Attributes = SePrivilegeEnabled;
            LookupPrivilegeValue("", SeShutdownName,
                out tkp.Privileges.PLuid);
            AdjustTokenPrivileges(hToken, false, ref tkp,
                0U, IntPtr.Zero, IntPtr.Zero);
        }

        public static void Shutdown()
        {
            Shutdown(false);
        }

        public static void Shutdown(bool force)
        {
            GetPrivileges();
            ExitWindowsEx(EwxShutdown |
                          (uint) (force ? EwxForce : 0) | EwxPoweroff, 0);
        }

        public static void Reboot()
        {
            Reboot(false);
        }

        public static void Reboot(bool force)
        {
            GetPrivileges();
            ExitWindowsEx(EwxReboot |
                          (uint) (force ? EwxForce : 0), 0);
        }

        public static void LogOff()
        {
            LogOff(false);
        }

        public static void LogOff(bool force)
        {
            GetPrivileges();
            ExitWindowsEx(EwxLogoff |
                          (uint) (force ? EwxForce : 0), 0);
        }
    }
}
