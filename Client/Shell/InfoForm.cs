﻿using TB.Cashier.Client.Shell.SelectForms;

namespace TB.Cashier.Client.Shell
{
    public partial class InfoForm : DefaultSelectionForm
    {
        public InfoForm(string text, string caption)
        {
            InitializeComponent();
            TextLb.Text = text;
            Text = caption;
        }
    }
}
