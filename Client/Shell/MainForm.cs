﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using Cashier.Equipment.Interface;
using Microsoft.Practices.Unity;
using TB.Cashier.Client.Shell.FinishForms;
using TB.Cashier.Client.Shell.SelectForms;
using TB.Cashier.Client.Shell.Wait;
using TB.Cashier.Server.Services.Dto;
using TB.Cashier.Server.Services.Interfaces;
using TB.Cashier.Server.Services.Interfaces.Faults;
using TB.Services.Infrastructure.Client;
using Utils;

namespace TB.Cashier.Client.Shell
{
    public partial class MainForm : Form, IMainForm
    {
        #region static resize methods

        private const int DefaultMarginOnX = 15;
        private const int DefaultMarginOnY = 5;

        private static int HeightByPercent(int percent, Control owner)
        {
            return (int) (owner.ClientSize.Height/100f*percent);
        }

        private static int WidthByPercent(int percent, Control owner)
        {
            return (int) (owner.ClientSize.Width/100f*percent);
        }

        private static int MiddleTopY(Control inner, Control owner, bool noRelation = false)
        {
            return (int)(owner.ClientSize.Height / 2f - inner.ClientSize.Height / 2f + (noRelation ? owner.Location.Y : 0));
        }

        private static int MiddleLeftX(Control inner, Control owner, bool noRelation = false)
        {
            return (int) (owner.ClientSize.Width/2f - inner.ClientSize.Width/2f + (noRelation ? owner.Location.X : 0));
        }

        private static void PushToLeft(Control pushable, Control owner, Control before = null, bool noRelation = false)
        {

            pushable.Location =
                new Point(
                    DefaultMarginOnX + (before == null ? 0 : before.Location.X + before.Width) +
                    (noRelation ? owner.Location.X : 0),
                    MiddleTopY(pushable, owner, noRelation));
        }

        private static void PushToRight(Control pushable, Control owner, Control before = null, bool noRelation = false)
        {

            pushable.Location =
                new Point(
                    (before == null ? 
                    owner.ClientSize.Width - pushable.ClientSize.Width - DefaultMarginOnX : before.Location.X - pushable.Width - DefaultMarginOnX)  + (noRelation ? owner.Location.X : 0),
                    MiddleTopY(pushable, owner, noRelation));
        }

        #endregion

        public ChequeDto CurrentCheque { get; set; }
        public ShiftDto CurrentShift { get; set; }
        public bool ActiveShiftIsAbsent { get; set; }

        public CabClientProxy<IChequeService> ChequeService { get; set; }
        public CabClientProxy<IMaterialService> MaterialService { get; set; }

        private string _inputCode = string.Empty;

        public string InputCode
        {
            get { return _inputCode; }
            set
            {
                _inputCode = value;
                CodeTb.Text = value;
            }
        }


        public MainForm()
        {
            InitializeComponent();
            ChequeService = new CabClientProxy<IChequeService>();
            MaterialService = new CabClientProxy<IMaterialService>();


            #region Getting Data

            SetActiveShift();

            #endregion

            #region Set Scanner

            UnityContext.Container.Resolve<IBarCodeScanner>().BarCodeReaded += MainForm_BarCodeReaded;

            #endregion
        }

        void MainForm_BarCodeReaded(string barCode)
        {
            InputCode = barCode;
            SendCode();
            InputCode = string.Empty;
        }

        public void SetActiveShift()
        {

            try
            {
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    CurrentShift = null;
                    shiftBindingSource.DataSource = typeof (ShiftDto);
                    CurrentCheque = null;
                    chequeBindingSource.DataSource = typeof (ChequeDto);

                    CurrentShift = ChequeService.Get(s => s.GetCurrentShift());

                    shiftBindingSource.DataSource = CurrentShift;
                    RefreshCurrentCheque();

                    EnableControl(this, true);
                    OpenShiftBt.Enabled = OpenShiftBt.Visible = false;
                }

            }
            catch (FaultException ex)
            {
                ShowError(ex.Message);
                EnableControl(this, false);

                ServiceBt.Enabled = true;
                OpenShiftBt.Enabled = OpenShiftBt.Visible = true;

            }
        }

        public void OpenShift()
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                try
                {
                    CurrentShift = ChequeService.Get(s => s.OpenShift());
                }
                catch (FaultException<ShiftFault> ex)
                {
                    ShowError(ex.Detail.Message);
                    return;
                }
                catch (FaultException ex)
                {
                    ShowError(ex.Message);
                    return;
                }
            }

            if (!UnityContext.Container.Resolve<IChequePrinter>().OpenShift(CurrentShift))
                ShowError(UnityContext.Container.Resolve<IChequePrinter>().LastError());
            SetActiveShift();
        }

        static void EnableControl(Control control, bool state)
        {
            foreach (Control cntrl in control.Controls)
            {
                if (cntrl.Controls.Count > 0)
                    EnableControl(cntrl, state);
                else
                    cntrl.Enabled = state;
            }
        }

        public void SizePanels()
        {
            #region Size Panels

            LeftPanel.Width = WidthByPercent(80, this);
            RightPanel.Width = ClientSize.Width - LeftPanel.Width;

            InfoPanel.Height = HeightByPercent(12, LeftPanel);
            GridPanel.Height = HeightByPercent(45, LeftPanel);
            CurrentPanel.Height = HeightByPercent(18, LeftPanel);
            CodePanel.Height = HeightByPercent(9, LeftPanel);
            SummaryPanel.Height = HeightByPercent(8, LeftPanel);
            DiscountPanel.Height = HeightByPercent(8, LeftPanel) - DefaultMarginOnY;

            #endregion

            #region Size InfoPanel items

            InfoLeftPanel.Width = WidthByPercent(20, InfoPanel);
            InfoRightPanel.Width = WidthByPercent(30, InfoPanel);

            ChequeNoTextLb.Height = ChequeNoLb.Height = HeightByPercent(50, InfoLeftPanel);
            ModeTextLb.Height = HeightByPercent(50, InfoCenterPanel);
            ShiftTextLb.Height = ShiftLb.Height = HeightByPercent(50, InfoRightPanel);

            OpenShiftBt.Height = InfoPanel.Height/2;
            OpenShiftBt.Width = ShiftLb.Width/2;
            PushToRight(OpenShiftBt, ShiftLb, null, true);
            #endregion

            #region Size Current Panel

            CurrentPosLb.Width = WidthByPercent(80, CurrentPanel);

            #endregion

            #region Size CodePanel items

            //CodeLb.Width = WidthByPercent(30, CodePanel);
            CodeLb.Height = NameLb.Height = HeightByPercent(99, CodePanel);

            CodeTb.Width = NameTb.Width = WidthByPercent(30, CodePanel);

            PushToLeft(CodeLb, CodePanel);
            PushToLeft(CodeTb, CodePanel, CodeLb);

            PushToRight(NameTb, CodePanel);
            PushToRight(NameLb, CodePanel, NameTb);


            #endregion

            #region Size RightPanel Items

            var controls = RightPanel.Controls.Cast<Control>().OrderBy(c => c.TabIndex).ToList();
            var topY = RightPanel.Height - DefaultMarginOnY;
            var buttonsBlockHeight = 0;

            foreach (var control in controls)
            {
                control.Width = RightPanel.Width - DefaultMarginOnX*2;
                if (control.GetType() == typeof (Button))
                    control.Height = HeightByPercent(6, RightPanel);

                if (control != KeyboardCtrl)
                    buttonsBlockHeight += control.Height + DefaultMarginOnY;

                control.Location = new Point(MiddleLeftX(control, RightPanel), topY - control.Height);
                topY -= control.Height + DefaultMarginOnY;
            }

            KeyboardCtrl.Width = RightPanel.Width - DefaultMarginOnX*2;
            KeyboardCtrl.Height = RightPanel.Height - buttonsBlockHeight - DefaultMarginOnY;
            KeyboardCtrl.Location = new Point(MiddleLeftX(KeyboardCtrl, RightPanel), 0);

            #endregion

            #region Size DiscountPanel Items

            DiscountSumBt.Height = HeightByPercent(70, DiscountPanel);
            PushToRight(DiscountSumBt,DiscountPanel);
            DiscountPercentBt.Height = HeightByPercent(70, DiscountPanel);
            PushToRight(DiscountPercentBt,DiscountPanel,DiscountSumBt);
            SetSumBt.Height = HeightByPercent(70, DiscountPanel);
            PushToRight(SetSumBt,DiscountPanel,DiscountPercentBt);


            #endregion
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            SizePanels();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            FormState.NormalFullScreen(this);
            SizePanels();
        }

        public void ShowError(string text)
        {
            MessageBox.Show(text, @"Ошибка", MessageBoxButtons.OK);
        }

        private void SendCode( string code = null)
        {
            code = code ?? InputCode;
            try
            {
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    if (!CurrentCheque.IsReturn && ChequeService.Get(s => s.CheckCodeIsMaterial(code)) == 1)
                        ChequeService.Execute(
                            s => s.AddMaterialToChequeByCode(CurrentCheque.Id.GetValueOrDefault(), code));

                    RefreshCurrentCheque();
                }
            }
            catch (FaultException ex)
            {
                ShowError(ex.Message);
            }

        }

        public void RefreshCurrentCheque()
        {

            try
            {
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    CurrentCheque = CurrentCheque != null
                        ? ChequeService.Get(s => s.GetChequeById(CurrentCheque.Id.GetValueOrDefault()))
                        : ChequeService.Get(s => s.GetActiveCheque());

                    chequeBindingSource.DataSource = CurrentCheque;

                    ModeTextLb.Text = CurrentCheque.IsReturn
                        ? Properties.Resources.ReturnChequeNoPostfix
                        : string.Empty;
                }
            }
            catch (FaultException ex)
            {
                ShowError(ex.Message);
            }

        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (ChequeLineGrid.IsCurrentCellInEditMode || NameTb.Focused)
                return;

            switch (e.KeyChar)
            {
                case (char) Keys.LineFeed:
                    FinishChequeBt_Click(FinishChequeBt, new EventArgs());
                    break;

                case (char) Keys.Enter:
                    SendCode();
                    InputCode = string.Empty;
                    e.Handled = true;
                    break;

                case (char) Keys.Back:
                    if (InputCode != string.Empty)
                        InputCode = InputCode.Remove(InputCode.Count() - 1);
                    break;

                default:
                    InputCode += e.KeyChar;
                    break;

            }

            CodeTb.SelectionStart = CodeTb.TextLength;
            CodeTb.SelectionLength = 0;
            e.Handled = true;
        }

        private void ChequeLineGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                ChequeService.Execute(s => s.UpdateChequeLine(linesBindingSource.Current as ChequeLineDto));
                RefreshCurrentCheque();
            }
        }

        private void ClearChequeBt_Click(object sender, EventArgs e)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                if (CurrentCheque == null)
                    return;
                ChequeService.Execute(s => s.ClearChequeById(CurrentCheque.Id.GetValueOrDefault()));
                RefreshCurrentCheque();

                CodeTb.Select();
            }
        }

        private void FinishChequeBt_Click(object sender, EventArgs e)
        {

                try
                {
                    if (CurrentCheque == null || !CurrentCheque.Lines.Any())
                    {
                        CodeTb.Select();
                        return;
                    }

                    using (var fr = new FinishCheque(CurrentCheque))
                    {
                        if (!fr.IsDisposed/*Форма не открывалась*/ && fr.ShowDialog() != DialogResult.OK)
                        {
                            CodeTb.Select();
                            return;
                        }

                        using (new WaitDialog(@"Закрытие чека"))
                        {

                            ChequeService.Execute(s => s.CloseCheque(CurrentCheque));
                            CurrentCheque = null;
                            RefreshCurrentCheque();

                            CodeTb.Select();
                        }
                    }
                }
                catch (FaultException ex)
                {
                    ShowError(ex.Message);
                }
        }

        private void PostponeBt_Click(object sender, EventArgs e)
        {
            if (!CurrentCheque.Lines.Any())
                return;

            try
            {
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    ChequeService.Execute(s => s.PostponeChequeById(CurrentCheque.Id.GetValueOrDefault()));
                    CurrentCheque = null;
                    RefreshCurrentCheque();

                    CodeTb.Select();
                }
            }
            catch (FaultException ex)
            {
                ShowError(ex.Message);
            }

        }

        /* private void ShowPostponedBt_Click(object sender, EventArgs e)
        {
            using (var pf = new PostponedForm())
            {
                if (pf.ShowDialog() != DialogResult.OK || pf.SelectedId <= 0)
                {
                    CodeTb.Select();
                    return;
                }
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    ChequeService.Execute(s => s.DeleteChequeById(CurrentCheque.Id.GetValueOrDefault()));
                    ChequeService.Execute(s=>s.ActivateCheque(pf.SelectedId));
                    CurrentCheque.Id = pf.SelectedId;
                    RefreshCurrentCheque();
                }
            }

            CodeTb.Select();
        }*/

        private void DeleteCurrentPosBt_Click(object sender, EventArgs e)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                var chequeLineDto = linesBindingSource.Current as ChequeLineDto;
                if (chequeLineDto != null)
                {
                    ChequeService.Execute(
                        s => s.DeleteChequeLineById(chequeLineDto.Id.GetValueOrDefault()));
                }
                RefreshCurrentCheque();            
            }

            CodeTb.Select();
        }

        private void linesBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            var chequeLineDto = linesBindingSource.Current as ChequeLineDto;

            CurrentPosLb.Text = chequeLineDto != null ? chequeLineDto.MaterialName : string.Empty;
        }

        /*private void CloseShiftBt_Click(object sender, EventArgs e)
        {
            using (var fr = new FinishShift())
            {
                fr.ShowDialog();
                SetActiveShift();
            }
            CodeTb.Select();
        }*/

        private void ReturnBt_Click(object sender, EventArgs e)
        {
            using (var fr = new ReturnForm())
            {
                if (fr.ShowDialog() != DialogResult.OK || fr.SelectedId <= 0)
                {
                    CodeTb.Select();
                    return;
                }
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    ChequeService.Execute(s => s.DeleteChequeById(CurrentCheque.Id.GetValueOrDefault()));
                    CurrentCheque = ChequeService.Get(s => s.CreateReturnCheque(fr.SelectedId));
                    if (fr.SelectedLineId > 0)
                        ChequeService.Execute(
                            s => s.CopyChequeLineById(fr.SelectedLineId, CurrentCheque.Id.GetValueOrDefault()));
                    else
                        ChequeService.Execute(
                            s => s.CopyChequeLines(fr.SelectedId, CurrentCheque.Id.GetValueOrDefault(), fr.SelectedLinesIds));

                    RefreshCurrentCheque();

                }
            }

            CodeTb.Select();
        }

        private void ChequeLineGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            e.Cancel = CurrentCheque.IsReturn;
        }

        private void ServiceBt_Click(object sender, EventArgs e)
        {
            using (var fr = new ServiceForm())
            {
                fr.ShowDialog();
                
            }

            CodeTb.Select();
        }

        private void KeyboardCtrl_KeyboardClick(char key)
        {
           /* if (GetNextControl(button1, false) NameTb)
                NameTb_KeyPress(this, new KeyPressEventArgs(key));
            else*/
                MainForm_KeyPress(this, new KeyPressEventArgs(key));
            CodeTb.Select();
        }

        private void ChequeWorkBt_Click(object sender, EventArgs e)
        {
            using (var fr = new ChequeWorkForm())
            {
                fr.ShowDialog();
            }

            CodeTb.Select();
        }

        private void OpenShiftBt_Click(object sender, EventArgs e)
        {
            OpenShift();
        }

        private void NameTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Enter)
            {
                /*  var res = new List<MaterialDto>();
                using (new WaitDialog(Properties.Resources.LoadingText))
                {
                    res = MaterialService.Get(s => s.SearchMaterialsByName(NameTb.Text));
                }
                if (res.Count > 1)
                {*/
                using (var fr = new MaterialForm(NameTb.Text))
                {
                    if (fr.ShowDialog() == DialogResult.OK)
                    {
                        SendCode(fr.SelectedOfferCode);
                    }
                }
                // }
                NameTb.Clear();
                CodeTb.Select();
            }
        }

        private void FreeSellBt_Click(object sender, EventArgs e)
        {
            using (new WaitDialog(Properties.Resources.LoadingText))
            {
                ChequeService.Execute(s => s.AddFreeLineToCheque(CurrentCheque.Id.GetValueOrDefault()));
                RefreshCurrentCheque();
            }

        }

        private void DiscountSumBt_Click(object sender, EventArgs e)
        {
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    (linesBindingSource.Current as ChequeLineDto).Discount = fr.Sum;
                    ChequeService.Execute(s => s.UpdateChequeLine(linesBindingSource.Current as ChequeLineDto));
                    RefreshCurrentCheque();
                    
                }
            }
            CodeTb.Select();
        }

        private void DiscountPercentBt_Click(object sender, EventArgs e)
        {
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    var chequeDto = (linesBindingSource.Current as ChequeLineDto);
                    if (chequeDto != null)
                    {
                        chequeDto.Discount = chequeDto.Sum / 100 * fr.Sum;
                        ChequeService.Execute(s => s.UpdateChequeLine(chequeDto));
                        RefreshCurrentCheque();
                    }

                }
            }
            CodeTb.Select();
        }

        private void SetSumBt_Click(object sender, EventArgs e)
        {
             var chequeDto = (linesBindingSource.Current as ChequeLineDto);
            if (chequeDto == null || chequeDto.MaterialId != 0)
            {
                ShowError(@"Сумму можно задавать только для свободной продажи!");
                return;
            }
            using (var fr = new SumForm())
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                        chequeDto.Price = fr.Sum;
                        ChequeService.Execute(s => s.UpdateChequeLine(chequeDto));
                        RefreshCurrentCheque();
                }
            }
            CodeTb.Select();
        }

    }
}
