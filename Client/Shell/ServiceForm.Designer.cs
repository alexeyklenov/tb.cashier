﻿using TB.Cashier.Client.Shell.Controls;

namespace TB.Cashier.Client.Shell
{
    partial class ServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServiceForm));
            this.btDelimiter3 = new System.Windows.Forms.Panel();
            this.btDelimiter1 = new System.Windows.Forms.Panel();
            this.btDelimiter2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ShowShiftChequesBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.ShowPostponedBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.OutcomeBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.IncomeBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.BalanceBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.XOtchetBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.TurnOffBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.CloseWinSessionBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.ClosePrBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.CloseShiftBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.OpenShiftBt = new TB.Cashier.Client.Shell.Controls.ButtonControl();
            this.SuspendLayout();
            // 
            // btDelimiter3
            // 
            this.btDelimiter3.BackColor = System.Drawing.Color.DimGray;
            this.btDelimiter3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btDelimiter3.Location = new System.Drawing.Point(15, 417);
            this.btDelimiter3.Name = "btDelimiter3";
            this.btDelimiter3.Size = new System.Drawing.Size(364, 10);
            this.btDelimiter3.TabIndex = 0;
            // 
            // btDelimiter1
            // 
            this.btDelimiter1.BackColor = System.Drawing.Color.DimGray;
            this.btDelimiter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btDelimiter1.Location = new System.Drawing.Point(12, 168);
            this.btDelimiter1.Name = "btDelimiter1";
            this.btDelimiter1.Size = new System.Drawing.Size(364, 10);
            this.btDelimiter1.TabIndex = 1;
            // 
            // btDelimiter2
            // 
            this.btDelimiter2.BackColor = System.Drawing.Color.DimGray;
            this.btDelimiter2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btDelimiter2.Location = new System.Drawing.Point(12, 255);
            this.btDelimiter2.Name = "btDelimiter2";
            this.btDelimiter2.Size = new System.Drawing.Size(364, 10);
            this.btDelimiter2.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(12, 79);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(364, 10);
            this.panel1.TabIndex = 2;
            // 
            // ShowShiftChequesBt
            // 
            this.ShowShiftChequesBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ShowShiftChequesBt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ShowShiftChequesBt.Location = new System.Drawing.Point(12, 95);
            this.ShowShiftChequesBt.Name = "ShowShiftChequesBt";
            this.ShowShiftChequesBt.Size = new System.Drawing.Size(179, 65);
            this.ShowShiftChequesBt.TabIndex = 278;
            this.ShowShiftChequesBt.Text = "Чеки за смену";
            this.ShowShiftChequesBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.ShowShiftChequesBt.UseVisualStyleBackColor = true;
            this.ShowShiftChequesBt.Click += new System.EventHandler(this.ShowShiftChequesBt_Click);
            // 
            // ShowPostponedBt
            // 
            this.ShowPostponedBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ShowPostponedBt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ShowPostponedBt.Location = new System.Drawing.Point(197, 95);
            this.ShowPostponedBt.Name = "ShowPostponedBt";
            this.ShowPostponedBt.Size = new System.Drawing.Size(179, 65);
            this.ShowPostponedBt.TabIndex = 277;
            this.ShowPostponedBt.Text = "Отложенные за смену";
            this.ShowPostponedBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.ShowPostponedBt.UseVisualStyleBackColor = true;
            this.ShowPostponedBt.Click += new System.EventHandler(this.ShowPostponedBt_Click);
            // 
            // OutcomeBt
            // 
            this.OutcomeBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.OutcomeBt.ForeColor = System.Drawing.Color.Black;
            this.OutcomeBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.OutComeSmall;
            this.OutcomeBt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.OutcomeBt.Location = new System.Drawing.Point(197, 184);
            this.OutcomeBt.Name = "OutcomeBt";
            this.OutcomeBt.Size = new System.Drawing.Size(179, 65);
            this.OutcomeBt.TabIndex = 8;
            this.OutcomeBt.Text = "Выдача наличности";
            this.OutcomeBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.OutcomeBt.UseVisualStyleBackColor = true;
            this.OutcomeBt.Click += new System.EventHandler(this.OutcomeBt_Click);
            // 
            // IncomeBt
            // 
            this.IncomeBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.IncomeBt.ForeColor = System.Drawing.Color.Black;
            this.IncomeBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.IncomeSmall;
            this.IncomeBt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.IncomeBt.Location = new System.Drawing.Point(12, 184);
            this.IncomeBt.Name = "IncomeBt";
            this.IncomeBt.Size = new System.Drawing.Size(179, 65);
            this.IncomeBt.TabIndex = 7;
            this.IncomeBt.Text = "Внесение наличности";
            this.IncomeBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.IncomeBt.UseVisualStyleBackColor = true;
            this.IncomeBt.Click += new System.EventHandler(this.IncomeBt_Click);
            // 
            // BalanceBt
            // 
            this.BalanceBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.BalanceBt.ForeColor = System.Drawing.Color.Black;
            this.BalanceBt.Location = new System.Drawing.Point(12, 8);
            this.BalanceBt.Name = "BalanceBt";
            this.BalanceBt.Size = new System.Drawing.Size(364, 65);
            this.BalanceBt.TabIndex = 6;
            this.BalanceBt.Text = "Остаток в кассе";
            this.BalanceBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.BalanceBt.UseVisualStyleBackColor = true;
            this.BalanceBt.Click += new System.EventHandler(this.BalanceBt_Click);
            // 
            // XOtchetBt
            // 
            this.XOtchetBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.XOtchetBt.ForeColor = System.Drawing.Color.Black;
            this.XOtchetBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.XReportSmall;
            this.XOtchetBt.Location = new System.Drawing.Point(15, 342);
            this.XOtchetBt.Name = "XOtchetBt";
            this.XOtchetBt.Size = new System.Drawing.Size(361, 65);
            this.XOtchetBt.TabIndex = 5;
            this.XOtchetBt.Text = "X отчет";
            this.XOtchetBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.XOtchetBt.UseVisualStyleBackColor = true;
            this.XOtchetBt.Click += new System.EventHandler(this.XOtchetBt_Click);
            // 
            // TurnOffBt
            // 
            this.TurnOffBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.TurnOffBt.ForeColor = System.Drawing.Color.DarkRed;
            this.TurnOffBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.ShutDownSmall;
            this.TurnOffBt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TurnOffBt.Location = new System.Drawing.Point(200, 504);
            this.TurnOffBt.Name = "TurnOffBt";
            this.TurnOffBt.Size = new System.Drawing.Size(179, 65);
            this.TurnOffBt.TabIndex = 4;
            this.TurnOffBt.Text = "Завершение работы";
            this.TurnOffBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.TurnOffBt.UseVisualStyleBackColor = true;
            this.TurnOffBt.Click += new System.EventHandler(this.TurnOffBt_Click);
            // 
            // CloseWinSessionBt
            // 
            this.CloseWinSessionBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.CloseWinSessionBt.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.CloseWinSessionBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.ExitSessionSmall;
            this.CloseWinSessionBt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CloseWinSessionBt.Location = new System.Drawing.Point(15, 504);
            this.CloseWinSessionBt.Name = "CloseWinSessionBt";
            this.CloseWinSessionBt.Size = new System.Drawing.Size(179, 65);
            this.CloseWinSessionBt.TabIndex = 3;
            this.CloseWinSessionBt.Text = "Завершение сеанса";
            this.CloseWinSessionBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.CloseWinSessionBt.UseVisualStyleBackColor = true;
            this.CloseWinSessionBt.Click += new System.EventHandler(this.CloseWinSessionBt_Click);
            // 
            // ClosePrBt
            // 
            this.ClosePrBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ClosePrBt.ForeColor = System.Drawing.Color.Navy;
            this.ClosePrBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.Exit_60;
            this.ClosePrBt.Location = new System.Drawing.Point(15, 433);
            this.ClosePrBt.Name = "ClosePrBt";
            this.ClosePrBt.Size = new System.Drawing.Size(364, 65);
            this.ClosePrBt.TabIndex = 2;
            this.ClosePrBt.Text = "Выход из программы";
            this.ClosePrBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.ClosePrBt.UseVisualStyleBackColor = true;
            this.ClosePrBt.Click += new System.EventHandler(this.ClosePrBt_Click);
            // 
            // CloseShiftBt
            // 
            this.CloseShiftBt.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.CloseShiftBt.ForeColor = System.Drawing.Color.Black;
            this.CloseShiftBt.Image = global::TB.Cashier.Client.Shell.Properties.Resources.CloseShiftSmall;
            this.CloseShiftBt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CloseShiftBt.Location = new System.Drawing.Point(197, 271);
            this.CloseShiftBt.Name = "CloseShiftBt";
            this.CloseShiftBt.Size = new System.Drawing.Size(179, 65);
            this.CloseShiftBt.TabIndex = 1;
            this.CloseShiftBt.Text = "Закрытие смены";
            this.CloseShiftBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.CloseShiftBt.UseVisualStyleBackColor = true;
            this.CloseShiftBt.Click += new System.EventHandler(this.CloseShiftBt_Click);
            // 
            // OpenShiftBt
            // 
            this.OpenShiftBt.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.OpenShiftBt.Image = ((System.Drawing.Image)(resources.GetObject("OpenShiftBt.Image")));
            this.OpenShiftBt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.OpenShiftBt.Location = new System.Drawing.Point(15, 271);
            this.OpenShiftBt.Name = "OpenShiftBt";
            this.OpenShiftBt.Size = new System.Drawing.Size(176, 65);
            this.OpenShiftBt.TabIndex = 279;
            this.OpenShiftBt.Text = "Открытие смены";
            this.OpenShiftBt.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.OpenShiftBt.UseVisualStyleBackColor = true;
            this.OpenShiftBt.Click += new System.EventHandler(this.OpenShiftBt_Click);
            // 
            // ServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 590);
            this.Controls.Add(this.OpenShiftBt);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ShowShiftChequesBt);
            this.Controls.Add(this.ShowPostponedBt);
            this.Controls.Add(this.btDelimiter2);
            this.Controls.Add(this.OutcomeBt);
            this.Controls.Add(this.IncomeBt);
            this.Controls.Add(this.btDelimiter1);
            this.Controls.Add(this.BalanceBt);
            this.Controls.Add(this.XOtchetBt);
            this.Controls.Add(this.TurnOffBt);
            this.Controls.Add(this.CloseWinSessionBt);
            this.Controls.Add(this.ClosePrBt);
            this.Controls.Add(this.btDelimiter3);
            this.Controls.Add(this.CloseShiftBt);
            this.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServiceForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сервис";
            this.Activated += new System.EventHandler(this.ServiceForm_Activated);
            this.Load += new System.EventHandler(this.ServiceForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ButtonControl CloseShiftBt;
        private System.Windows.Forms.Panel btDelimiter3;
        private ButtonControl ClosePrBt;
        private ButtonControl CloseWinSessionBt;
        private ButtonControl TurnOffBt;
        private ButtonControl XOtchetBt;
        private ButtonControl BalanceBt;
        private System.Windows.Forms.Panel btDelimiter1;
        private ButtonControl IncomeBt;
        private ButtonControl OutcomeBt;
        private System.Windows.Forms.Panel btDelimiter2;
        private ButtonControl ShowShiftChequesBt;
        private ButtonControl ShowPostponedBt;
        private System.Windows.Forms.Panel panel1;
        private ButtonControl OpenShiftBt;
    }
}