﻿using System;
using System.Threading;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;
using ThreadTimer = System.Threading.Timer;

namespace TB.Cashier.Client.Shell
{
    public partial class SplashScreen : Form
    {
        private delegate void CloseSplash();
        private DialogResult _authResult;
        private Timer _timer;

        public SplashScreen()
        {
            InitializeComponent();
        }

        private void SplashScreen_Shown(object sender, EventArgs e)
        {
           /* new ThreadTimer(delegate
             {
                 using (var authForm = new AuthenticationForm())
                 {
                     _authResult = authForm.ShowDialog();
                 }
                 Invoke(new CloseSplash(ShowNextForm));
             },null, 100, Timeout.Infinite);
            */

            _timer = new Timer(){Interval = 2000};
            _timer.Tick += _timer_Tick;
            _timer.Start();
             
            /*using (var authForm = new AuthenticationForm())
            {
                _authResult = authForm.ShowDialog();
            }
            if (_authResult == DialogResult.OK)
                Program.SetMainForm(new MainFormV2());
            else
                Close();*/
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            _timer.Stop();

            using (var authForm = new AuthenticationForm())
            {
                _authResult = authForm.ShowDialog();
            }
            if (_authResult == DialogResult.OK)
                Program.SetMainForm(new MainFormV2_1());
            else
                Close();
        }

        private void ShowNextForm()
        {
            if (_authResult == DialogResult.OK)
                Program.SetMainForm(new MainFormV2_1());
            else
                Close();
        }

    }
}
