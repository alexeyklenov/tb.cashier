﻿namespace TB.Cashier.Client.Shell
{
    partial class ChequeWorkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ShowShiftChequesBt = new System.Windows.Forms.Button();
            this.ShowPostponedBt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ShowShiftChequesBt
            // 
            this.ShowShiftChequesBt.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.ShowShiftChequesBt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ShowShiftChequesBt.Location = new System.Drawing.Point(12, 12);
            this.ShowShiftChequesBt.Name = "ShowShiftChequesBt";
            this.ShowShiftChequesBt.Size = new System.Drawing.Size(360, 40);
            this.ShowShiftChequesBt.TabIndex = 4;
            this.ShowShiftChequesBt.Text = "Чеки за смену";
            this.ShowShiftChequesBt.UseVisualStyleBackColor = true;
            this.ShowShiftChequesBt.Click += new System.EventHandler(this.ShowShiftChequesBt_Click);
            // 
            // ShowPostponedBt
            // 
            this.ShowPostponedBt.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.ShowPostponedBt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ShowPostponedBt.Location = new System.Drawing.Point(12, 58);
            this.ShowPostponedBt.Name = "ShowPostponedBt";
            this.ShowPostponedBt.Size = new System.Drawing.Size(360, 40);
            this.ShowPostponedBt.TabIndex = 3;
            this.ShowPostponedBt.Text = "Отложенные за смену";
            this.ShowPostponedBt.UseVisualStyleBackColor = true;
            this.ShowPostponedBt.Click += new System.EventHandler(this.ShowPostponedBt_Click);
            // 
            // ChequeWorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 110);
            this.Controls.Add(this.ShowShiftChequesBt);
            this.Controls.Add(this.ShowPostponedBt);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChequeWorkForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Чеки";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ShowShiftChequesBt;
        private System.Windows.Forms.Button ShowPostponedBt;
    }
}